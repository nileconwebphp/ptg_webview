const env = 'prod'
var apiCollectiion
// User Api

if(env === 'prod') {
    apiCollectiion = {
        defaultLang: 'th',
        api:`https://asv-mobileapp-prod.azurewebsites.net/api`,
        dopa: 'https://crmpartner.pt.co.th/dopo',
    }
}else{
    apiCollectiion = {
        defaultLang: 'th',
        api:`https://asv-mobileapp-dev.azurewebsites.net/api`,
        dopa: 'https://crmpartner.pt.co.th/dopo',
    }
}

export const API_LIST = apiCollectiion