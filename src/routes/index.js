import React from 'react'
import { Switch, Route } from 'react-router-dom'
// import { PrivateRoute } from '../_components'

import Agreements from '../pages/agreements'
import Formprofile from '../pages/formprofile'
import Reset_pass from '../pages/reset_pass'
import History_earn from '../pages/history_earn'
import History_burn from '../pages/history_burn'
import History_totalpoint from '../pages/history_totalpoint'
import Formprofile_1 from '../pages/formprofile_1'
import Formprofile_2 from '../pages/formprofile_2'
import Formprofile_3 from '../pages/formprofile_3'
import Formprofile_4 from '../pages/formprofile_4'
import Formprofile_5 from '../pages/formprofile_5'
import Updateprofile from '../pages/formprofile1_update'
import updateAddress from '../pages/formprofile2_update'
import updateType1 from '../pages/formprofile_update4'
import updateType2 from '../pages/formprofile_update5'
import Inform_updateprofile from '../pages/inform_updateprofile'
import NotFoundPages from '../pages/404'

export default () => (
  <Switch>
    <Route exact path="/webview/agreements/:lang/:phoneid/:emailid/:firstname/:lastname/:birthdate/:udid/:deviceos" component={Formprofile_1} />

    <Route exact path="/webview/formprofile" component={Formprofile} />

    <Route exact path="/webview/history_earn/:lang/:tokenid/:customerid/:cardmemberid/:udid/:deviceos" component={History_earn} />
    <Route exact path="/webview/history_burn/:lang/:tokenid/:customerid/:cardmemberid/:udid/:deviceos" component={History_burn} />
    <Route exact path="/webview/history_totalpoint/:lang/:tokenid/:customerid/:cardmemberid/:udid/:deviceos" component={History_totalpoint} />

    <Route exact path="/webview/reset_pass/:lang" component={Reset_pass} />

    <Route exact path="/webview/formprofile_1/:lang" component={Formprofile_1} />
    <Route exact path="/webview/formprofile_2/:lang" component={Formprofile_2} />
    <Route exact path="/webview/formprofile_3/:lang" component={Formprofile_3} />
    <Route exact path="/webview/formprofile_4/:lang" component={Formprofile_4} />
    <Route exact path="/webview/formprofile_5/:lang" component={Formprofile_5} />

    <Route exact path="/webview/history_earn" component={History_earn} /> 
    <Route exact path="/webview/history_burn" component={History_burn} />
    <Route exact path="/webview/reset_pass" component={Reset_pass} />

    <Route exact path="/webview/update_formprofile/:lang/:tokenid/:customerid/:cardmemberid/:udid/:deviceos" component={Updateprofile} />
    <Route exact path="/webview/update_formaddress/:lang/:tokenid/:customerid/:cardmemberid/:udid/:deviceos" component={updateAddress} />
    <Route exact path="/webview/update_formtype1/:lang/:tokenid/:customerid/:cardmemberid/:udid/:deviceos" component={updateType1} />
    <Route exact path="/webview/update_formtype2/:lang/:tokenid/:customerid/:cardmemberid" component={updateType2} />

    <Route exact path="/webview/formprofile_5" component={Formprofile_5} />

    <Route exact path="/webview/inform_updateprofile/:lang/:phoneid/:emailid" component={Inform_updateprofile} />
    <Route component={NotFoundPages} />
  </Switch>
)