import axios from "axios";
import { API_LIST } from '../../src/_constants/matcher';

export const getTransection =  {
    getBurnHistoryList
};

function getBurnHistoryList(start,end,user,token,customer,member,type){
    let formData = new FormData();
    formData.append('userId' , user)
    formData.append('TOKEN_ID' , token)
    formData.append('CUSTOMER_ID' , customer)
    formData.append('CARD_MEMBER_ID' , member)
    formData.append('DATE_START' , start)
    formData.append('DATE_END' , end)
    formData.append('TRANS_TYPE', type)
    formData.append('PAGE_NO' , "1")
    formData.append('PAGE_SIZE' , "1000")
    formData.append('udId' , sessionStorage.getItem("_udid"))
    formData.append('deviceOs' , sessionStorage.getItem("_deviceos"))
    console.log("userId",user)
    console.log("TOKEN_ID",token)
    console.log("CUSTOMER_ID",customer)
    console.log("DATE_START",start)
    console.log("DATE_END",end)
    console.log("TRANS_TYPE",type)
    console.log("PAGE_NO", "1")
    console.log("PAGE_SIZE", "1000")
    return axios.post(`${API_LIST.api}/PTMaxCard/GetTransaction` , formData).then( res => {
        return res;
    });
}
