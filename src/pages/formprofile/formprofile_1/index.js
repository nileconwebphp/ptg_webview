import React from "react";
// import { NavLink } from "react-router-dom";

// import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import "./formprofile_1.css";
import {
  IntlProvider,
  FormattedMessage,
  FormattedHTMLMessage
} from "react-intl";
import { addLocaleData } from "react-intl";
import locale_en from "react-intl/locale-data/en";
import locale_th from "react-intl/locale-data/th";
import intlMessageEN from "./../../_translations/en.json";
import intlMessageTH from "./../../_translations/th.json";

addLocaleData([...locale_en, ...locale_th]);

  var messages = {
    th: intlMessageTH,
    en: intlMessageEN
  };


class Formprofile_1 extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      startDate: new Date(),
      language: "th"
      
    };
    this.handleChange = this.handleChange.bind(this);
  }
 
  handleChange(date) {
    this.setState({
      startDate: date
    });
  }

  componentDidMount() {
    const params = this.props.match.params;
    this.setState({ language: params.lang });
  }
 

  render() {

    var link = `/formprofile_2/${this.state.language}`


    return (

      <IntlProvider
        locale={this.state.language}
        messages={messages[this.state.language].formprofile1}
      >
      <div className="bg_full">
        <div className="container-fluid">
        <div className="content_page">
            <div className="head_title_agree"><FormattedMessage id="header_profile" /></div>
            <div className="form-group">
              <label><FormattedMessage id="title_idnumber" /></label>
              <input type="text" className="form-control input_form" id="exampleFormControlInput1" placeholder={messages[this.state.language].formprofile1.idcard_input} />
            </div>
            {/* <label className="check_conditions_form">
                    <FormattedMessage id="title_foreigners" />
              <input type="checkbox" name="aggreement" />
              <span className="checkmark_condition"></span>
             </label>
             <div className="form-group">
              <input type="text" className="form-control input_form" id="exampleFormControlInput1" placeholder={messages[this.state.language].formprofile1.idcard_inputforeigners} />
            </div> */}
            <div className="head_title_agree"><FormattedMessage id="title_user" /></div>
            <div className="form-group">
              <label><FormattedMessage id="title_nametitle" /></label>
              <FormattedHTMLMessage id="title_nametitle_list" />
            </div>
            <div className="form-group">
              <label><FormattedMessage id="title_name" /></label>
              <input type="text" className="form-control input_form" id="exampleFormControlInput1" placeholder={messages[this.state.language].formprofile1.name_input}/>
            </div>
            <div className="form-group">
              <label><FormattedMessage id="title_lastname" /></label>
              <input type="text" className="form-control input_form" id="exampleFormControlInput1" placeholder={messages[this.state.language].formprofile1.lastname_input} />
            </div>
            <div className="form-group">
              <label><FormattedMessage id="title_sex" /></label>
              <FormattedHTMLMessage id="title_sexlist" />
            </div>
            <div className="form-group">
              <label><FormattedMessage id="title_dateofbirth" /></label>
              {/* <div><DatePicker selected={this.state.startDate} onChange={this.handleChange}/></div> */}
              <input type="date" className="form-control input_form" id="exampleFormControlInput1"  />
              
              
            </div>
            <div className="form-group">
              <label><FormattedMessage id="title_status" /></label>
              <FormattedHTMLMessage id="title_statuslist" />
            </div>
            <div className="form-group">
              <label><FormattedMessage id="title_email" /></label>
              <input type="email" className="form-control input_form" id="exampleFormControlInput1" placeholder={messages[this.state.language].formprofile1.email_input} />
            </div>
            <div className="form-group">
              <label><FormattedMessage id="title_phonenumber" /></label>
              <input type="number" className="form-control input_form" id="exampleFormControlInput1" placeholder={messages[this.state.language].formprofile1.phone_input} />
            </div>
            <a href={link}>
              <div className="btn_nextstep">
                  <button type="button" className="btn btn-secondary btn-block btn_agreement"><FormattedMessage id="button_nextstep" /></button>
              </div>
             </a>
          </div>
        </div>
      </div>
      </IntlProvider>
    )
  }
}

export default Formprofile_1;
