import React from "react";
// import { NavLink } from "react-router-dom";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import "./formprofile.css";

class Formprofile extends React.Component {
 
  constructor(props) {
    super(props);
    this.state = {
      startDate: new Date()
    };
    this.handleChange = this.handleChange.bind(this);
  }
 
  handleChange(date) {
    this.setState({
      startDate: date
    });
  }

  render() {
    return (
      <div className="bg_full">
        <div className="container-fluid">
        <div className="content_page">

{/* form1 */}
        <div className="head_title_agree">ข้อมูลส่วนตัว</div>
            <div className="form-group">
              <label for="exampleFormControlInput1">เลขที่บัตรประชาชน</label>
              <input type="email" className="form-control input_form" id="exampleFormControlInput1" placeholder="กรอกเลขที่บัตรประชาชน" />
            </div>
            {/* <label className="check_conditions_form">
                 กรณีเป็นชาวต่างชาติ (เลขที่บัตรประชาชน/เลขที่หนังสือเดินทาง)
              <input type="checkbox" name="aggreement" />
              <span className="checkmark_condition"></span>
             </label>
             <div className="form-group">
              <input type="email" className="form-control input_form" id="exampleFormControlInput1" placeholder="กรอกข้อมูล" />
            </div> */}
            <div className="head_title_agree">ข้อมูลผู้ใข้งาน</div>
            <div className="form-group">
              <label for="exampleFormControlInput1">คำนำหน้าชื่อ</label>
              <select class="form-control" id="exampleFormControlSelect1">
                <option>นาย</option>
                <option>นาง</option>
                <option>นางสาว</option>
              </select>
            </div>
            <div className="form-group">
              <label for="exampleFormControlInput1">ชื่อ</label>
              <input type="email" className="form-control input_form" id="exampleFormControlInput1" placeholder="กรอกชื่อ" />
            </div>
            <div className="form-group">
              <label for="exampleFormControlInput1">นามสกุล</label>
              <input type="email" className="form-control input_form" id="exampleFormControlInput1" placeholder="กรอกนามสกุล" />
            </div>
            <div className="form-group">
              <label for="exampleFormControlInput1">เพศ</label>
              <select class="form-control" id="exampleFormControlSelect1">
                <option>ชาย</option>
                <option>หญิง</option>
              </select>
            </div>
            <div className="form-group">
              <label for="exampleFormControlInput1">วัน เดือน ปี เกิด*</label>
              <div><DatePicker selected={this.state.startDate} onChange={this.handleChange}/></div>
              
            </div>
            <div className="form-group">
              <label for="exampleFormControlInput1">สถานะภาพ</label>
              <select class="form-control" id="exampleFormControlSelect1">
                <option>โสด</option>
                <option>แต่งงานแล้ว</option>
                <option>ไม่ระบุ</option>
              </select>
            </div>
            <div className="form-group">
              <label for="exampleFormControlInput1">อีเมล</label>
              <input type="email" className="form-control input_form" id="exampleFormControlInput1" placeholder="กรอกอีเมล" />
            </div>
            <div className="form-group">
              <label for="exampleFormControlInput1">หมายเลขโทรศัพท์มือถือ</label>
              <input type="email" className="form-control input_form" id="exampleFormControlInput1" placeholder="กรอกหมายเลขโทรศัพท์มือถือ" />
            </div>

{/* form2 */}
            <div className="head_title_agree">ข้อมูลที่อยู่</div>
            <div className="form-group">
              <label for="exampleFormControlInput1">บ้านเลขที่</label>
              <input type="email" className="form-control input_form" id="exampleFormControlInput1" placeholder="กรอกบ้านเลขที่" />
            </div>
             <div className="form-group">
              <label for="exampleFormControlInput1">หมู่บ้าน / อาคาร</label>
              <input type="email" className="form-control input_form" id="exampleFormControlInput1" placeholder="กรอกหมู่บ้าน / อาคาร" />
            </div>
            <div className="form-group">
              <label for="exampleFormControlInput1">ซอย</label>
              <input type="email" className="form-control input_form" id="exampleFormControlInput1" placeholder="กรอกซอย" />
            </div>
            <div className="form-group">
              <label for="exampleFormControlInput1">ถนน</label>
              <input type="email" className="form-control input_form" id="exampleFormControlInput1" placeholder="กรอกถนน" />
            </div>
            <div className="form-group">
              <label for="exampleFormControlInput1">จังหวัด</label>
              <select class="form-control" id="exampleFormControlSelect1">
                <option>เลือกจังหวัด</option>
                <option>กระบี่</option>
                <option>กรุงเทพฯ</option>
              </select>
            </div>
            <div className="form-group">
              <label for="exampleFormControlInput1">เขต / อำเภอ</label>
              <select class="form-control" id="exampleFormControlSelect1">
                <option>เลือกเขต / อำเภอ</option>
                <option>คลองเตย </option>
                <option>คลองสามวา</option>
              </select>
            </div>
            <div className="form-group">
              <label for="exampleFormControlInput1">แขวง / ตำบล</label>
              <select class="form-control" id="exampleFormControlSelect1">
                <option>เลือกแขวง / ตำบล</option>
                <option>คลองเตย</option>
                <option>คลองตัน	</option>
                <option>พระโขนง	</option>
              </select>
            </div>
            <div className="form-group">
              <label for="exampleFormControlInput1">รหัสไปรษณีย์</label>
              <input type="email" className="form-control input_form" id="exampleFormControlInput1" placeholder="รหัสไปรษณีย์" />
            </div>
            <div className="form-group">
              <label for="exampleFormControlInput1">Address</label>
              <textarea class="form-control input_form" id="exampleFormControlTextarea1" rows="3" placeholder="Please input your address"></textarea> 
            </div>


 {/* form3 */} 
            <div className="head_title_agree">รูปภาพ</div>
            <div className="mockup"><img src="images/upload_mockup.png" alt="" /></div>
            <label className="check_conditions">
                 ต้องการให้ข้อมูลเพิ่มเติม เพื่อสิทธิประโยชน์ที่ตรงกับความต้องการของท่าน
              <input type="checkbox" name="aggreement" />
              <span className="checkmark_condition"></span>
             </label>
             <div className="remindall">
               *เอกสารเพิ่มเติมระบบมีข้อจำกัด คือ
               <div>1. อัพโหลดรูปไม่ได้ไม่เกิน 3 รูป</div>
               <div>2. ในแต่ละรูปขนาดไม่เกิน 3 MB</div>
             </div>


 {/* form4 */}       
            <div className="form-group">
              <label for="exampleFormControlInput1">ประเภทรถ</label>
              <select class="form-control" id="exampleFormControlSelect1">
                <option>เลือกประเภทรถ</option>
                <option>รถเก๋ง</option>
                <option>รถกระบะบ้าน</option>
                <option>รถกระบะพาณิชย์ / อุตสาหกรรม</option>
                <option>รถบรรทุกพาณิชย์ / อุตสาหกรรม</option>
              </select>
            </div>
             <div className="form-group">
              <label for="exampleFormControlInput1">ยี่ห้อรถ</label>
              <select class="form-control" id="exampleFormControlSelect1">
                <option>เลือกยี่ห้อรถ</option>
                <option>โตโยต้า (Toyota)</option>
                <option>นิสสัน (Nissan)</option>
                <option>มาสด้า (Mazda)</option>
                <option>ฟอร์ด (Ford)</option>
              </select>
            </div>
            <div className="form-group">
              <label for="exampleFormControlInput1">ประเภทของอาชีพ</label>
              <select class="form-control" id="exampleFormControlSelect1">
                <option>เลือกอาชีพ</option>
                <option>เกษตรกร</option>
                <option>รับราชการ / รัฐวิสาหกิจ</option>
                <option>พนักงานบริษัทเอกชน</option>
                <option>เจ้าของธุรกิจ</option>
              </select>
            </div>
            <div className="form-group">
              <label for="exampleFormControlInput1">ระดับการศึกษา</label>
              <select class="form-control" id="exampleFormControlSelect1">
                <option>เลือกระดับการศึกษา</option>
                <option>ประถมศึกษา</option>
                <option>มัธยมศึกษาตอนต้น</option>
                <option>มัธยมศึกษาตอนปลายและอาชีวศึกษา</option>
                <option>อุดมศึกษาและเทียบเท่า</option>
                <option>ปริญญาตรี</option>
                <option>ปริญญาโท</option>
                <option>ปริญญาเอก</option>
              </select>
            </div>
            <div className="form-group">
              <label for="exampleFormControlInput1">รายได้ต่อเดือน</label>
              <select class="form-control" id="exampleFormControlSelect1">
                <option>เลือกรายได้ของคุณ</option>
                <option>ต่ำกว่า 10,000 บาท</option>
                <option>10,001 - 20,000 บาท</option>
                <option>20,001 - 30,000 บาท</option>
                <option>30,001 - 50,000 บาท</option>
                <option>มากกว่า 50,000 บาท ขึ้นไป</option>
              </select>
            </div>
            <label for="exampleFormControlInput1">ท่านยินดีรับข่าวสาร/โปรโมชั่นจากทางใด(เลือกได้มากกว่า 1 ข้อ)</label>
            <div className="row">
                <div className="col">
                <label className="check_conditions_select">จดหมาย
                  <input type="checkbox" name="aggreement" />
                  <span className="checkmark_condition_select"></span>
                </label>
                </div>
                <div className="col">
                <label className="check_conditions_select">อีเมล
                  <input type="checkbox" name="aggreement" />
                  <span className="checkmark_condition_select"></span>
                </label>
                </div>
                <div class="w-100"></div>
                <div className="col">
                <label className="check_conditions_select">เลือกรับข่าวสารจาก SMS
                  <input type="checkbox" name="aggreement" />
                  <span className="checkmark_condition_select"></span>
                </label>
                </div>
                <div className="col">
                <label className="check_conditions_select">ไม่รับข่าวสาร
                  <input type="checkbox" name="aggreement" />
                  <span className="checkmark_condition_select"></span>
                </label>
                </div>
            </div>
            <label for="exampleFormControlInput1">โปรโมชั่นที่น่าสนใจ</label>
            <div className="row">
                <div className="col">
                <label class="selectoneonly">อาหาร
                  <input type="radio" name="radio" />
                  <span class="checkmark_selectoneonly"></span>
                </label>
                </div>
                <div className="col">
                <label class="selectoneonly">ท่องเที่ยว
                  <input type="radio" name="radio" />
                  <span class="checkmark_selectoneonly"></span>
                </label>
                </div>
                <div class="w-100"></div>
                <div className="col">
                <label class="selectoneonly">ช้อปปิ้ง
                  <input type="radio" name="radio" />
                  <span class="checkmark_selectoneonly"></span>
                </label>
                </div>
                <div className="col">
                <label class="selectoneonly">ไลฟ์ สไตล์ และความงาม
                  <input type="radio" name="radio" />
                  <span class="checkmark_selectoneonly"></span>
                </label>
                </div>
                <div class="w-100"></div>
                <div className="col">
                <label class="selectoneonly">ที่พัก และรถยนต์
                  <input type="radio" name="radio" />
                  <span class="checkmark_selectoneonly"></span>
                </label>
                </div>
                <div class="w-100"></div>
                <div className="col">
                <label class="selectoneonly">อื่นๆ โปรดระบุ
                  <input type="radio" name="radio" />
                  <span class="checkmark_selectoneonly"></span>
                </label>
                <div class="form-group">
                  <textarea class="form-control input_form" id="exampleFormControlTextarea1" rows="1" placeholder="อื่นๆ โปรดระบุข้อมูล"></textarea>
                </div>
                </div>
            </div>

 {/* form5 */} 
            <label for="exampleFormControlInput1">กิจกรรมที่ท่านสนใจ ด้านเอนเตอร์เทนเมนต์</label>
            <div className="row">
                <div className="col">
                <label class="selectoneonly">ฟังเพลง
                  <input type="radio" name="radio" />
                  <span class="checkmark_selectoneonly"></span>
                </label>
                </div>
                <div className="col">
                <label class="selectoneonly">คอนเสิร์ต
                  <input type="radio" name="radio" />
                  <span class="checkmark_selectoneonly"></span>
                </label>
                </div>
                <div class="w-100"></div>
                <div className="col">
                <label class="selectoneonly">คอนเสิร์ตลูกทุ่ง
                  <input type="radio" name="radio" />
                  <span class="checkmark_selectoneonly"></span>
                </label>
                </div>
                <div className="col">
                <label class="selectoneonly">ภาพยนตร์
                  <input type="radio" name="radio" />
                  <span class="checkmark_selectoneonly"></span>
                </label>
                </div>
                <div class="w-100"></div>
                <div className="col">
                <label class="selectoneonly">ละครเวที
                  <input type="radio" name="radio" />
                  <span class="checkmark_selectoneonly"></span>
                </label>
                </div>
                <div class="w-100"></div>
                <div className="col">
                <label class="selectoneonly">อื่นๆ โปรดระบุ
                  <input type="radio" name="radio" />
                  <span class="checkmark_selectoneonly"></span>
                </label>
                <div class="form-group">
                  <textarea class="form-control input_form" id="exampleFormControlTextarea1" rows="1" placeholder="อื่นๆ โปรดระบุข้อมูล"></textarea>
                </div>
                </div>
            </div>

            <label for="exampleFormControlInput1">ด้านกิจกรรมนอกบ้าน</label>
            <div className="row">
                <div className="col">
                <label class="selectoneonly">ช้อปปิ้ง
                  <input type="radio" name="radio" />
                  <span class="checkmark_selectoneonly"></span>
                </label>
                </div>
                <div className="col">
                <label class="selectoneonly">ความงาม
                  <input type="radio" name="radio" />
                  <span class="checkmark_selectoneonly"></span>
                </label>
                </div>
                <div class="w-100"></div>
                <div className="col">
                <label class="selectoneonly">ร้านอาหาร
                  <input type="radio" name="radio" />
                  <span class="checkmark_selectoneonly"></span>
                </label>
                </div>
                <div className="col">
                <label class="selectoneonly">สถานบันเทิง
                  <input type="radio" name="radio" />
                  <span class="checkmark_selectoneonly"></span>
                </label>
                </div>
                <div class="w-100"></div>
                <div className="col">
                <label class="selectoneonly">กิจกรรมแม่และเด็ก
                  <input type="radio" name="radio" />
                  <span class="checkmark_selectoneonly"></span>
                </label>
                </div>
                <div className="col">
                <label class="selectoneonly">แรลลี่
                  <input type="radio" name="radio" />
                  <span class="checkmark_selectoneonly"></span>
                </label>
                </div>
                <div class="w-100"></div>
                <div className="col">
                <label class="selectoneonly">การผจญภัย
                  <input type="radio" name="radio" />
                  <span class="checkmark_selectoneonly"></span>
                </label>
                </div>
                <div className="col">
                <label class="selectoneonly">ถ่ายภาพ
                  <input type="radio" name="radio" />
                  <span class="checkmark_selectoneonly"></span>
                </label>
                </div>
                <div class="w-100"></div>
                <div className="col">
                <label class="selectoneonly">ไหว้พระและการทำบุญ
                  <input type="radio" name="radio" />
                  <span class="checkmark_selectoneonly"></span>
                </label>
                </div>
                <div className="col">
                <label class="selectoneonly">ดูดวงและฮวงจุ้ย
                  <input type="radio" name="radio" />
                  <span class="checkmark_selectoneonly"></span>
                </label>
                </div>
                <div class="w-100"></div>
                <div className="col">
                <label class="selectoneonly">ท่องเที่ยวต่างประเทศ
                  <input type="radio" name="radio" />
                  <span class="checkmark_selectoneonly"></span>
                </label>
                </div>
                <div className="col">
                <label class="selectoneonly">ท่องเที่ยวในประเทศ
                  <input type="radio" name="radio" />
                  <span class="checkmark_selectoneonly"></span>
                </label>
                </div>
                <div class="w-100"></div>
                <div className="col">
                <label class="selectoneonly">อื่นๆ โปรดระบุ
                  <input type="radio" name="radio" />
                  <span class="checkmark_selectoneonly"></span>
                </label>
                <div class="form-group">
                  <textarea class="form-control input_form" id="exampleFormControlTextarea1" rows="1" placeholder="อื่นๆ โปรดระบุข้อมูล"></textarea>
                </div>
                </div>
            </div>
            <label for="exampleFormControlInput1">ด้านกิจกรรมในบ้าน</label>
            <div className="row">
                <div className="col">
                <label class="selectoneonly">ทำอาหาร
                  <input type="radio" name="radio" />
                  <span class="checkmark_selectoneonly"></span>
                </label>
                </div>
                <div className="col">
                <label class="selectoneonly">ทำขนมอบ
                  <input type="radio" name="radio" />
                  <span class="checkmark_selectoneonly"></span>
                </label>
                </div>
                <div class="w-100"></div>
                <div className="col">
                <label class="selectoneonly">ตกแต่งบ้าน
                  <input type="radio" name="radio" />
                  <span class="checkmark_selectoneonly"></span>
                </label>
                </div>
                <div className="col">
                <label class="selectoneonly">ทำสวน
                  <input type="radio" name="radio" />
                  <span class="checkmark_selectoneonly"></span>
                </label>
                </div>
                <div class="w-100"></div>
                <div className="col">
                <label class="selectoneonly">อ่านหนังสือ
                  <input type="radio" name="radio" />
                  <span class="checkmark_selectoneonly"></span>
                </label>
                </div>
                <div className="col">
                <label class="selectoneonly">เล่นเกมส์
                  <input type="radio" name="radio" />
                  <span class="checkmark_selectoneonly"></span>
                </label>
                </div>
                <div class="w-100"></div>
                <div className="col">
                <label class="selectoneonly">อินเตอร์เน็ต
                  <input type="radio" name="radio" />
                  <span class="checkmark_selectoneonly"></span>
                </label>
                </div>
                <div class="w-100"></div>
                <div className="col">
                <label class="selectoneonly">อื่นๆ โปรดระบุ
                  <input type="radio" name="radio" />
                  <span class="checkmark_selectoneonly"></span>
                </label>
                <div class="form-group">
                  <textarea class="form-control input_form" id="exampleFormControlTextarea1" rows="1" placeholder="อื่นๆ โปรดระบุข้อมูล"></textarea>
                </div>
                </div>
            </div>
            <label for="exampleFormControlInput1">ด้านกิจกรรมที่ท่านสนใจ</label>
            <div className="row">
                <div className="col">
                <label class="selectoneonly">กิจกรรมสำหรับเพื่อนฝูง
                  <input type="radio" name="radio" />
                  <span class="checkmark_selectoneonly"></span>
                </label>
                </div>
                <div className="col">
                <label class="selectoneonly">กิจกรรมสำหรับคู่รัก
                  <input type="radio" name="radio" />
                  <span class="checkmark_selectoneonly"></span>
                </label>
                </div>
                <div class="w-100"></div>
                <div className="col">
                <label class="selectoneonly">กิจกรรมสำหรับครอบครัว
                  <input type="radio" name="radio" />
                  <span class="checkmark_selectoneonly"></span>
                </label>
                </div>
                <div className="col">
                <label class="selectoneonly">ไม่สนใจทำกิจกรรม
                  <input type="radio" name="radio" />
                  <span class="checkmark_selectoneonly"></span>
                </label>
                </div>
            </div>
            
           
             {/* <a href="/formprofile_3">
              <div className="btn_nextstep">
                  <button type="button" className="btn btn-secondary btn-block btn_agreement">ดำเนินการต่อ</button>
              </div>
             </a> */}

             <a href="/formprofile_1">
              <div className="btn_nextstep">
                  <button type="button" className="btn btn-secondary btn-block btn_agreement">ส่งข้อมูลเพื่อสมัครสมาชิก</button>
              </div>
             </a>
      
          </div>
        </div>
      </div>
       
    )
  }
}

export default Formprofile;
