import React from "react";
// import { NavLink } from "react-router-dom";
import {
  IntlProvider,
  FormattedMessage,
  FormattedHTMLMessage
} from "react-intl";
import { addLocaleData } from "react-intl";
import locale_en from "react-intl/locale-data/en";
import locale_th from "react-intl/locale-data/th";
import intlMessageEN from "./../../_translations/en.json";
import intlMessageTH from "./../../_translations/th.json";
import { API_LIST } from "../../_constants/matcher";
import "./agreements.css";


addLocaleData([...locale_en, ...locale_th]);

var messages = {
  th: intlMessageTH,
  en: intlMessageEN
};

class Agreements extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      language: "th",
      isActive: {},
      checked: false
    };
    this.handleCheck = this.handleCheck.bind(this);
  }

  handleCheck(event){
    this.setState({ 
      checked: event.target.checked
    });
  }

  clickNextPage(){
    if(this.state.checked == true){
      window.location.href = `/webview/formprofile_1/${this.state.language}`
    }else{
     this.state.checked = false
    }
  }

  componentDidMount() {
    localStorage.clear();
    sessionStorage.clear();
    const params = this.props.match.params;
    var tokenData = this.props.location;
    var sstr = tokenData.search.search("&");
    var sub_memberid = tokenData.search.substr(sstr);
    var memberid = sub_memberid.substr(5);
    this.setState({
      language: params.lang
        ? params.lang.toString().toLowerCase()
        : API_LIST.defaultLang
    });
    localStorage.setItem('_usid', memberid)
    localStorage.setItem('_phid', params.phoneid)
    localStorage.setItem('_emid', params.emailid)
    localStorage.setItem('_firstname' , params.firstname)
    localStorage.setItem('_lastname' , params.lastname)
    localStorage.setItem('_birthdate' , params.birthdate)
    localStorage.setItem('_udid' , params.udid)
    localStorage.setItem('_deviceos' , params.deviceos)
  }


  render() {
    var link = `/formprofile_1/${this.state.language}`
    return (
      <IntlProvider
        locale={this.state.language}
        messages={messages[this.state.language].agreement}
      >
      <div className="bg_full">
        <div className="container-fluid">
        <div className="content_page">
            <div className="head_title_agree"><FormattedMessage id="header" /></div>
            <div className="detail_content">
              <FormattedHTMLMessage id="detail" />
              <label className="check_conditions">
             <FormattedMessage id="condition_title" />
             {/* <input type="checkbox" name="aggreement"/> */}
              <input type="checkbox" name="aggreement" value={this.state.checked} onChange={(event) => this.handleCheck(event)}/>
              <span className="checkmark_condition"></span>
             </label>
            </div>
             <a onClick={() => this.clickNextPage()}>
             <div className="content_btn_agreements">
                 <button type="button" className="btn btn-secondary btn-block btn_agreement" disabled={!this.state.checked}><FormattedMessage id="accept_button" /></button>
                 {/* <button type="button" className="btn btn-secondary btn-block btn_agreement"><FormattedMessage id="accept_button" /></button> */}
             </div>
             </a>
          </div>
        </div>
      </div>
      </IntlProvider>
    )
  }
}

export default Agreements;
