import React from "react";
import {
  IntlProvider,
  FormattedMessage,
  // FormattedHTMLMessage
} from "react-intl";
import { addLocaleData } from "react-intl";
import locale_en from "react-intl/locale-data/en";
import locale_th from "react-intl/locale-data/th";
import intlMessageEN from "./../../_translations/en.json";
import intlMessageTH from "./../../_translations/th.json";
import { API_LIST } from "../../_constants/matcher"
import { regisMaxCard } from "../../_actions/regiser_ptmaxcard";
import './formprofile_update5.css'
import SweetAlert from "react-bootstrap-sweetalert";

var alert;
var disabled_block = 'form-control input_form disabled'
var sport_remark_display = 'form-control input_form disabled'
var outside_remark_display = 'form-control input_form disabled'
var inside_remark_display = 'form-control input_form disabled'
var resultApi = ''

addLocaleData([...locale_en, ...locale_th]);

  var messages = {
    th: intlMessageTH,
    en: intlMessageEN
  };


class updateType2 extends React.Component {
  
  constructor(props) {
    super(props);
    this.state = {
      language: "th",
      disableAgreement:true,
      disabled: true,
      disabled_sport: true,
      disabled_outside: true,
      disabled_inside: true,
      disabledBtn: '',
      resultApi: '',
      modalIsOpen: false,
      fields: { 
        isChangeCartype: false,
      },  
      show: false,
      modal: null,
    };
    this.openModal = this.openModal.bind(this);
    this.afterOpenModal = this.afterOpenModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
  }
  

  openModal() {
    this.setState({modalIsOpen: true});
  }

  afterOpenModal() {
    // references are now sync'd and can be accessed.
    this.subtitle.style.color = '#f00';
  }

  closeModal() {
    this.setState({modalIsOpen: false});
  }

  componentDidMount() {
    if(this.props.location.state.data) {
      let newObject = Object.assign(this.props.location.state.data , this.state.fields)
      this.setState({ fields: newObject })
    }
    const params = this.props.match.params;
    var tokenData = this.props.location;
    this.setState({ language: (params.lang ? params.lang.toString().toLowerCase() : API_LIST.defaultLang) });

    sessionStorage.setItem('_usid' , tokenData.state.userid)
    sessionStorage.setItem('_tokenid' , params.tokenid)
    sessionStorage.setItem('_customerid' , params.customerid)
    sessionStorage.setItem('_cardmemberid' , params.cardmemberid)

  }

  modalCheckSubmit(res,img){
    alert = (
      <SweetAlert
        custom
        confirmBtnBsStyle="success"
        closeOnClickOutside={true}
        cancelBtnBsStyle="default"
        focusConfirmBtn={false}
        title=""
        customIcon={img}
        showConfirm={false}
        showCancelButton
        onCancel={() => this.handleChoice(false)}
        onConfirm={() => this.handleChoice(true)}
        onOutsideClick={() => {
          this.wasOutsideClick = true 
          this.setState({ showConfirm : false })
        }}
      >
        <div className="iconClose" onClick={() => this.handleChoice(false)}><i className="fas fa-times sizeCloseBtn"></i></div>
        <div className="fontSizeCase">{res}</div>
      </SweetAlert>
    );
    this.setState({ show: true , modal: alert })
  }

  handleChoice(choice) {
    if (choice === false) {
      if(this.state.disableAgreement === false){
        this.setState({ show: false , modal: null })
      }else{
        this.setState({ show: false , modal: null })
        window.location.href=`PTG:||updateinformation||success`
      }
    }
  }

  submitFormFinal(){
    regisMaxCard.updateAdditionalProfile(this.state.fields,this.props.location.state.tokenGet).then(e => {
      if(e.data.isSuccess === false) {
        this.setState({ disableAgreement: false })
        this.setState({ disabledBtn: '' })
        this.setState({ resultApi: e.data.errMsg })
        var imgPopup = `${process.env.PUBLIC_URL}/images/cancel.png`;
        this.modalCheckSubmit(this.state.resultApi,imgPopup)
      } else {
        if(this.props.location.state.changeCartype == true){
          regisMaxCard.updateCar(this.state.fields,this.props.location.state.tokenGet).then(data => {
            if(data.data.isSuccess === false){
              this.setState({ disableAgreement:false })
              this.setState({ resultApi1: data.data.errMsg })
              var imgPopup = `${process.env.PUBLIC_URL}/images/cancel.png`;
              this.modalCheckSubmit(this.state.resultApi1,imgPopup)
            }else{
              var imgPopup = `${process.env.PUBLIC_URL}/images/checked.png`;
              if(e.data.errMsg === "Success"){
                if(this.state.language === "th"){
                  resultApi = 'แก้ไขข้อมูลสำเร็จ'
                }else{
                  resultApi = e.data.errMsg 
                }
              }
              this.modalCheckSubmit(resultApi,imgPopup)
              // window.location.href=`PTG:||updateinformation||success`
            }
          })
        }else{
          var imgPopup = `${process.env.PUBLIC_URL}/images/checked.png`;
          if(e.data.errMsg === "Success"){
            if(this.state.language === "th"){
              resultApi = 'แก้ไขข้อมูลสำเร็จ'
            }else{
              resultApi = e.data.errMsg 
            }
          }
          this.modalCheckSubmit(resultApi,imgPopup)
          // window.location.href=`PTG:||updateinformation||success`
        }
      }
    })
  }

  handleGameClik(ev) {
    let { fields } = this.state;
    if(ev.target.name === "entertain") {
      if (ev.target.id === "ACT_EN_OTHERS") {
        if(ev.target.checked === true) {
          disabled_block = 'form-control input_form'
          fields[ev.target.id] = ev.target.checked
        } else {
          disabled_block = 'form-control input_form disabled'
          fields['ACT_EN_OTHERS_REMARK'] = ''
          fields[ev.target.id] = ev.target.checked
        }
        disabled_block = 'form-control input_form'
      } else {
        if(fields['ACT_EN_OTHERS'] === true) {
          disabled_block = 'form-control input_form'
        } else {
          disabled_block = 'form-control input_form disabled'
          fields['ACT_EN_OTHERS_REMARK'] = ''
        }
        fields[ev.target.id] = ev.target.checked
      }
      this.setState( {disabled: (ev.target.checked === "" ? false : true)} )
    } 
      else if(ev.target.name === "sport") {
        if (ev.target.id === "ACT_SPT_OTHERS") {
          if(ev.target.checked === true) {
            sport_remark_display = 'form-control input_form'
            fields[ev.target.id] = ev.target.checked
          } else {
            sport_remark_display = 'form-control input_form disabled'
            fields['ACT_SPT_OTHERS_REMARK'] = ''
            fields[ev.target.id] = ev.target.checked
          }
        } else {
          if (fields['ACT_SPT_OTHERS'] === true) {
            sport_remark_display = 'form-control input_form'
          } else {
            sport_remark_display = 'form-control input_form disabled'
            fields['ACT_SPT_OTHERS_REMARK'] = ''
          }
          fields[ev.target.id] = ev.target.checked
        }
      this.setState( {disabled_sport: (ev.target.checked === "" ? false : true)} )
    } else if(ev.target.name === "outside") {
      if (ev.target.id === "ACT_OD_OTHERS") {
        if(ev.target.checked === true) {
          outside_remark_display = 'form-control input_form'
          fields[ev.target.id] = ev.target.checked
        } else {
          outside_remark_display = 'form-control input_form disabled'
          fields['ACT_OD_OTHERS_REMARK'] = ''
          fields[ev.target.id] = ev.target.checked
        }
      } else {
        if (fields['ACT_OD_OTHERS'] === true){
          outside_remark_display = 'form-control input_form'
        } else {
          outside_remark_display = 'form-control input_form disabled'
          fields['ACT_OD_OTHERS_REMARK'] = ''
        }
        fields[ev.target.id] = ev.target.checked
      }  
      this.setState( {disabled_outside: (ev.target.checked === "" ? false : true)} )
    } else if(ev.target.name === "inside") {
      if (ev.target.id === "ACT_ID_OTHERS") {
        if(ev.target.checked === true) {
          inside_remark_display = 'form-control input_form'
          fields[ev.target.id] = ev.target.checked
        } else {
          inside_remark_display = 'form-control input_form disabled'
          fields['ACT_ID_OTHERS_REMARK'] = ''
          fields[ev.target.id] = ev.target.checked
        }
      } else {
        if ( fields['ACT_ID_OTHERS'] === true) {
          inside_remark_display = 'form-control input_form'
        } else {
          inside_remark_display = 'form-control input_form disabled'
          fields['ACT_ID_OTHERS_REMARK'] = ''
        }
       fields[ev.target.id] = ev.target.checked
      }  
      this.setState( {disabled_inside: (ev.target.checked === "" ? false : true)} )
    } else if(ev.target.name === "inside") { 
      fields['ACT_HNGOUT_FRIEND'] = false
      fields['ACT_HNGOUT_COUPLE'] = false
      fields['ACT_HNGOUT_FAMILY'] = false
      fields['ACT_HNGOUT_NO'] = false
      fields[ev.target.id] = ev.target.checked
    } else {
      fields[ev.target.id] = ev.target.value
    }
  
      this.setState({ fields })
  } 

  checboxClick(ev) {
    let { fields } = this.state;
      if(ev.target.name === "activities") {
        fields[ev.target.id] = ev.target.checked
        if (ev.target.id === "ACT_HNGOUT_NO") {
          if(ev.target.checked === true) {
            fields['ACT_HNGOUT_FRIEND'] = false
            fields['ACT_HNGOUT_COUPLE'] = false
            fields['ACT_HNGOUT_FAMILY'] = false
          }
        } else {
          fields['ACT_HNGOUT_NO'] = false
        }
      } else {
        fields[ev.target.id] = ev.target.value
      }
    this.setState({ fields })
}

  render() {
    return (
      <IntlProvider
      locale={this.state.language}
      defaultLocale={this.state.language}
      messages={messages[this.state.language].formprofile5}
      >
      <div className="bg_full">
      <SweetAlert
          show={this.state.show}
          title=""
          text={this.state.resultApi}
          onOutsideClick={() => this.setState({ show: false })}
        />
        <div className="container-fluid">
        <div className="content_page">
            <label ><FormattedMessage id="title_typecars" /></label>
            <div className="label1">{messages[this.state.language].formprofile5.title_entertainment}</div>
            <div className="row">
                <div className="col">
                <label className="selectoneonly"><FormattedMessage id="title_listmusic" />
                  <input type="checkbox" id="ACT_EN_MUSIC" checked={this.state.fields['ACT_EN_MUSIC']} name="entertain" onClick = {(e) => this.handleGameClik(e)} />
                  <span className="checkmark_selectoneonly"></span>
                </label>
                </div>
                <div className="col">
                <label className="selectoneonly"><FormattedMessage id="title_listconcert" />
                  <input type="checkbox" id="ACT_EN_CONCERT" checked={this.state.fields['ACT_EN_CONCERT']} name="entertain" onClick = {(e) => this.handleGameClik(e)} />
                  <span className="checkmark_selectoneonly"></span>
                </label>
                </div>
                <div className="w-100"></div>
                <div className="col">
                <label className="selectoneonly"><FormattedMessage id="title_listcontry_concert" />
                  <input type="checkbox" id="ACT_EN_COUNTRY_CONCERT" checked={this.state.fields['ACT_EN_COUNTRY_CONCERT']} name="entertain" onClick = {(e) => this.handleGameClik(e)} />
                  <span className="checkmark_selectoneonly"></span>
                </label>
                </div>
                <div className="col">
                <label className="selectoneonly"><FormattedMessage id="title_listmovie" />
                  <input type="checkbox" id="ACT_EN_MOVIES" checked={this.state.fields['ACT_EN_MOVIES']} name="entertain" onClick = {(e) => this.handleGameClik(e)} />
                  <span className="checkmark_selectoneonly"></span>
                </label>
                </div>
                <div className="w-100"></div>
                <div className="col">
                <label className="selectoneonly"><FormattedMessage id="title_liststage_play" />
                  <input type="checkbox" id="ACT_EN_THEATER" checked={this.state.fields['ACT_EN_THEATER']} name="entertain" onClick = {(e) => this.handleGameClik(e)} />
                  <span className="checkmark_selectoneonly"></span>
                </label>
                </div>
                <div className="w-100"></div>
                <div className="col">
                <label className="selectoneonly"><FormattedMessage id="title_listother" />
                  <input type="checkbox" id="ACT_EN_OTHERS" checked={this.state.fields['ACT_EN_OTHERS']} name="entertain" onClick = {(e) => this.handleGameClik(e)} />
                  <span className="checkmark_selectoneonly"></span>
                </label>
                <div className="form-group">
                <input type="text" id="ACT_EN_OTHERS_REMARK" className={disabled_block} 
                  placeholder={messages[this.state.language].formprofile5.etc_input} value={this.state.fields['ACT_EN_OTHERS_REMARK']}  onChange={(e) => this.handleGameClik(e)} />
                </div>
                </div>
            </div>

            <label ><FormattedMessage id="title_typesport" /></label>
            <div className="row">
                <div className="col">
                <label className="selectoneonly"><FormattedMessage id="title_listfitness" />
                  <input type="checkbox" id="ACT_SPT_FITNESS" checked={this.state.fields['ACT_SPT_FITNESS']} name="sport" onClick = {(e) => this.handleGameClik(e)} />
                  <span className="checkmark_selectoneonly"></span>
                </label>
                </div>
                <div className="col">
                <label className="selectoneonly"><FormattedMessage id="title_listrun" />
                  <input type="checkbox" id="ACT_SPT_RUN" checked={this.state.fields['ACT_SPT_RUN']} name="sport" onClick = {(e) => this.handleGameClik(e)} />
                  <span className="checkmark_selectoneonly"></span>
                </label>
                </div>
                <div className="w-100"></div>
                <div className="col">
                <label className="selectoneonly"><FormattedMessage id="title_bicycle" />
                  <input type="checkbox" id="ACT_SPT_CYCLING" checked={this.state.fields['ACT_SPT_CYCLING']} name="sport" onClick = {(e) => this.handleGameClik(e)} />
                  <span className="checkmark_selectoneonly"></span>
                </label>
                </div>
                <div className="col">
                <label className="selectoneonly"><FormattedMessage id="title_football" />
                  <input type="checkbox" id="ACT_SPT_FOOTBALL" checked={this.state.fields['ACT_SPT_FOOTBALL']} name="sport" onClick = {(e) => this.handleGameClik(e)} />
                  <span className="checkmark_selectoneonly"></span>
                </label>
                </div>
                <div className="w-100"></div>
                <div className="col">
                <label className="selectoneonly"><FormattedMessage id="title_listother" />
                  <input type="checkbox" id="ACT_SPT_OTHERS" checked={this.state.fields['ACT_SPT_OTHERS']} name="sport"  onClick = {(e) => this.handleGameClik(e)} />
                  <span className="checkmark_selectoneonly"></span>
                </label>
                <div className="form-group">
                <input type="text" id="ACT_SPT_OTHERS_REMARK" className={sport_remark_display} 
                  placeholder={messages[this.state.language].formprofile5.etc_input} value={this.state.fields['ACT_SPT_OTHERS_REMARK']}  onChange={(e) => this.handleGameClik(e)} />
                </div>
                </div>
  
            </div>


            <label ><FormattedMessage id="title_listoutside"/></label>
            <div className="row">
                <div className="col">
                <label className="selectoneonly"><FormattedMessage id="title_listshopping" />
                  <input type="checkbox" id="ACT_OD_SHOPPING" checked={this.state.fields['ACT_OD_SHOPPING']} name="outside" onClick = {(e) => this.handleGameClik(e)} />
                  <span className="checkmark_selectoneonly"></span>
                </label>
                </div>
                <div className="col">
                <label className="selectoneonly"><FormattedMessage id="title_listbeauty" />
                  <input type="checkbox" id="ACT_OD_BEAUTY" checked={this.state.fields['ACT_OD_BEAUTY']} name="outside" onClick = {(e) => this.handleGameClik(e)} />
                  <span className="checkmark_selectoneonly"></span>
                </label>
                </div>
                <div className="w-100"></div>
                <div className="col">
                <label className="selectoneonly"><FormattedMessage id="title_listrestaurant" />
                  <input type="checkbox" id="ACT_OD_RESTAURANT" checked={this.state.fields['ACT_OD_RESTAURANT']} name="outside" onClick = {(e) => this.handleGameClik(e)} />
                  <span className="checkmark_selectoneonly"></span>
                </label>
                </div>
                <div className="col">
                <label className="selectoneonly"><FormattedMessage id="title_listpub" />
                  <input type="checkbox" id="ACT_OD_CLUB" checked={this.state.fields['ACT_OD_CLUB']} name="outside" onClick = {(e) => this.handleGameClik(e)} />
                  <span className="checkmark_selectoneonly"></span>
                </label>
                </div>
                <div className="w-100"></div>
                <div className="col">
                <label className="selectoneonly"><FormattedMessage id="title_listmom" />
                  <input type="checkbox" id="ACT_OD_MOMCHILD" checked={this.state.fields['ACT_OD_MOMCHILD']} name="outside" onClick = {(e) => this.handleGameClik(e)} />
                  <span className="checkmark_selectoneonly"></span>
                </label>
                </div>
                <div className="col">
                <label className="selectoneonly"><FormattedMessage id="title_listrally" />
                  <input type="checkbox" id="ACT_OD_RALLY" checked={this.state.fields['ACT_OD_RALLY']} name="outside" onClick = {(e) => this.handleGameClik(e)} />
                  <span className="checkmark_selectoneonly"></span>
                </label>
                </div>
                <div className="w-100"></div>
                <div className="col">
                <label className="selectoneonly"><FormattedMessage id="title_listadventure" />
                  <input type="checkbox" id="ACT_OD_ADVANTURE" checked={this.state.fields['ACT_OD_ADVANTURE']} name="outside" onClick = {(e) => this.handleGameClik(e)} />
                  <span className="checkmark_selectoneonly"></span>
                </label>
                </div>
                <div className="col">
                <label className="selectoneonly"><FormattedMessage id="title_listtakephoto" />
                  <input type="checkbox" id="ACT_OD_PHOTO" checked={this.state.fields['ACT_OD_PHOTO']} name="outside" onClick = {(e) => this.handleGameClik(e)} />
                  <span className="checkmark_selectoneonly"></span>
                </label>
                </div>
                <div className="w-100"></div>
                <div className="col">
                <label className="selectoneonly"><FormattedMessage id="title_listmakemerit" />
                  <input type="checkbox" id="ACT_OD_PHILANTHROPY" checked={this.state.fields['ACT_OD_PHILANTHROPY']} name="outside" onClick = {(e) => this.handleGameClik(e)} />
                  <span className="checkmark_selectoneonly"></span>
                </label>
                </div>
                <div className="col">
                <label className="selectoneonly"><FormattedMessage id="title_listhoroscope" />
                  <input type="checkbox" id="ACT_OD_HOROSCOPE" checked={this.state.fields['ACT_OD_HOROSCOPE']} name="outside" onClick = {(e) => this.handleGameClik(e)} />
                  <span className="checkmark_selectoneonly"></span>
                </label>
                </div>
                <div className="w-100"></div>
                <div className="col">
                <label className="selectoneonly"><FormattedMessage id="title_listtravelout" />
                  <input type="checkbox" id="ACT_OD_ABROAD" checked={this.state.fields['ACT_OD_ABROAD']} name="outside" onClick = {(e) => this.handleGameClik(e)} />
                  <span className="checkmark_selectoneonly"></span>
                </label>
                </div>
                <div className="col">
                <label className="selectoneonly"><FormattedMessage id="title_listtravelin" />
                  <input type="checkbox" id="ACT_OD_UPCOUNTRY" checked={this.state.fields['ACT_OD_UPCOUNTRY']} name="outside" onClick = {(e) => this.handleGameClik(e)} />
                  <span className="checkmark_selectoneonly"></span>
                </label>
                </div>
                <div className="w-100"></div>
                <div className="col">
                <label className="selectoneonly"><FormattedMessage id="title_listother" />
                  <input type="checkbox" id="ACT_OD_OTHERS" checked={this.state.fields['ACT_OD_OTHERS']} name="outside" onClick = {(e) => this.handleGameClik(e)} />
                  <span className="checkmark_selectoneonly"></span>
                </label>
                <div className="form-group">
                  <input type="text" id="ACT_OD_OTHERS_REMARK" className={outside_remark_display} 
                    placeholder={messages[this.state.language].formprofile5.etc_input} value={this.state.fields['ACT_OD_OTHERS_REMARK']}  onChange={(e) => this.handleGameClik(e)} />
                </div>
                </div>
            </div>
            <label><FormattedMessage id="title_listin_house" /></label>
            <div className="row">
                <div className="col">
                <label className="selectoneonly"><FormattedMessage id="title_listcook" />
                  <input type="checkbox"  id="ACT_ID_COOKING" checked={this.state.fields['ACT_ID_COOKING']} name="inside" onClick = {(e) => this.handleGameClik(e)} />
                  <span className="checkmark_selectoneonly"></span>
                </label>
                </div>
                <div className="col">
                <label className="selectoneonly"><FormattedMessage id="title_listpastries" />
                  <input type="checkbox"  id="ACT_ID_BAKING" checked={this.state.fields['ACT_ID_BAKING']} name="inside" onClick = {(e) => this.handleGameClik(e)} />
                  <span className="checkmark_selectoneonly"></span>
                </label>
                </div>
                <div className="w-100"></div>
                <div className="col">
                <label className="selectoneonly"><FormattedMessage id="title_listdecorate" />
                  <input type="checkbox"  id="ACT_ID_DECORATE" checked={this.state.fields['ACT_ID_DECORATE']} name="inside" onClick = {(e) => this.handleGameClik(e)} />
                  <span className="checkmark_selectoneonly"></span>
                </label>
                </div>
                <div className="col">
                <label className="selectoneonly"><FormattedMessage id="title_listgardening" />
                  <input type="checkbox"  id="ACT_ID_GARDENING" checked={this.state.fields['ACT_ID_GARDENING']} name="inside" onClick = {(e) => this.handleGameClik(e)} />
                  <span className="checkmark_selectoneonly"></span>
                </label>
                </div>
                <div className="w-100"></div>
                <div className="col">
                <label className="selectoneonly"><FormattedMessage id="title_listread" />
                  <input type="checkbox"  id="ACT_ID_READING" checked={this.state.fields['ACT_ID_READING']} name="inside" onClick = {(e) => this.handleGameClik(e)} />
                  <span className="checkmark_selectoneonly"></span>
                </label>
                </div>
                <div className="col">
                <label className="selectoneonly"><FormattedMessage id="title_listplaygame" />
                  <input type="checkbox"  id="ACT_ID_GAMING" checked={this.state.fields['ACT_ID_GAMING']} name="inside" onClick = {(e) => this.handleGameClik(e)} />
                  <span className="checkmark_selectoneonly"></span>
                </label>
                </div>
                <div className="w-100"></div>
                <div className="col">
                <label className="selectoneonly"><FormattedMessage id="title_listinternet" />
                  <input type="checkbox"  id="ACT_ID_INTERNET" checked={this.state.fields['ACT_ID_INTERNET']} name="inside" onClick = {(e) => this.handleGameClik(e)} />
                  <span className="checkmark_selectoneonly"></span>
                </label>
                </div>
                <div className="w-100"></div>
                <div className="col">
                <label className="selectoneonly"><FormattedMessage id="title_listother" />
                  <input type="checkbox"  id="ACT_ID_OTHERS" checked={this.state.fields['ACT_ID_OTHERS']} name="inside" value="inside_other" onClick = {(e) => this.handleGameClik(e)} />
                  <span className="checkmark_selectoneonly"></span>
                </label>
                <div className="form-group">
                <input type="text" id="ACT_ID_OTHERS_REMARK" className={inside_remark_display} 
                    placeholder={messages[this.state.language].formprofile5.etc_input} value={this.state.fields['ACT_ID_OTHERS_REMARK']}  onChange={(e) => this.handleGameClik(e)} />
                </div>
                </div>
            </div>
            <label ><FormattedMessage id="title_listyourinterested" /></label>
            <div className="row">
                <div className="col">
                <label className="check_conditions_select"><FormattedMessage id="title_listactivitieswithfriend" />
                  <input type="checkbox"  id="ACT_HNGOUT_FRIEND" name="activities"  onChange = {(e) => this.checboxClick(e)} checked={(this.state.fields['ACT_HNGOUT_FRIEND']) ? (this.state.fields['ACT_HNGOUT_FRIEND'] === true || this.state.fields['ACT_HNGOUT_NO'] != true ? true : false) : false}  />
                  <span className="checkmark_condition_select"></span>
                </label>
                </div>
                <div className="col">
                <label className="check_conditions_select"><FormattedMessage id="title_listactivitieswithcouple" />
                  <input type="checkbox"  id="ACT_HNGOUT_COUPLE" name="activities"  onChange = {(e) => this.checboxClick(e)} checked={(this.state.fields['ACT_HNGOUT_COUPLE']) ? (this.state.fields['ACT_HNGOUT_COUPLE'] === true || this.state.fields['ACT_HNGOUT_NO'] != true ? true : false) : false} />
                  <span className="checkmark_condition_select"></span>
                </label>
                </div>
                <div className="w-100"></div>
                <div className="col">
                <label className="check_conditions_select"><FormattedMessage id="title_listactivitieswithfamily" />
                  <input type="checkbox"  id="ACT_HNGOUT_FAMILY" name="activities"  onChange = {(e) => this.checboxClick(e)} checked={(this.state.fields['ACT_HNGOUT_FAMILY']) ? (this.state.fields['ACT_HNGOUT_FAMILY'] === true || this.state.fields['ACT_HNGOUT_NO'] != true ? true : false) : false} />
                  <span className="checkmark_condition_select"></span>
                </label>
                </div>
                <div className="col">
                <label className="check_conditions_select"><FormattedMessage id="title_listactivitiesnone" />
                  <input type="checkbox"  id="ACT_HNGOUT_NO" name="activities"   onChange = {(e) => this.checboxClick(e)} checked={(this.state.fields['ACT_HNGOUT_NO'] === true ? true : false)} />
                  <span className="checkmark_condition_select"></span>
                </label>
                </div>
            </div>
          
              <div className="">
              <button type="button" className={`btn btn-secondary btn-block btn_agreement ${this.state.disabledBtn}`} onClick={() => this.submitFormFinal()}>{(this.state.disabledBtn === 'disabled' ? <i className="fa fa-circle-o-notch fa-spin" style={{ marginRight: "10px",marginTop: "5px" }}/> : '')}<FormattedMessage id="button_edit" /></button>
              </div>
            {this.state.modal}
          </div>
        </div>
      </div>
      </IntlProvider>
    )
  }
}

export default updateType2;
