import React from "react";
import { regisMaxCard } from "../../_actions/regiser_ptmaxcard";
import "react-datepicker/dist/react-datepicker.css";
import "./formprofile_1.css";
import {
  IntlProvider,
  FormattedMessage
  // FormattedHTMLMessage
} from "react-intl";
import { addLocaleData } from "react-intl";
import locale_en from "react-intl/locale-data/en";
import locale_th from "react-intl/locale-data/th";
import intlMessageEN from "./../../_translations/en.json";
import intlMessageTH from "./../../_translations/th.json";
import { API_LIST } from "../../_constants/matcher";
import moment from "moment-timezone";
import 'moment/locale/th';
import DatePicker from 'react-mobile-datepicker';
import SweetAlert from "react-bootstrap-sweetalert";

var genderClass = "form-control disabled";
var crypto = require('crypto');

addLocaleData([...locale_en, ...locale_th]);

var messages = {
  th: intlMessageTH,
  en: intlMessageEN
};

var ERROR_USER_EMAIL = false
var ERROR_USER_PHONE = false


const monthMap = {
  '1': '01',
  '2': '02',
  '3': '03',
  '4': '04',
  '5': '05',
  '6': '06',
  '7': '07',
  '8': '08',
  '9': '09',
  '10': '10',
  '11': '11',
  '12': '12',
};

const dateConfig = {
  'date':{
    format: 'DD',
  },
  'month':{
    format: value => monthMap[value.getMonth() + 1]
  },
  'year':{
    format: 'YYYY'
  },
}

const dateConfigTH = {
  'date':{
    format: 'DD',
  },
  'month':{
    format: value => monthMap[value.getMonth() + 1]
  },
  'year':{
    format: value => moment(value).add(543,'y').format('YYYY')
  },
}

const Dopa_dateConfig = {'date':{format: 'DD',},'month':{format: value => monthMap[value.getMonth() + 1]}, 'year':{ format: 'YYYY' },}
const Dopa_dateConfigTH = {'date':{  format: 'DD', }, 'month':{  format: value => monthMap[value.getMonth() + 1] },'year':{  format: value => moment(value).add(543,'y').format('YYYY') },}
const Dopa_dateConfigOnlyDayYear = {'date':{  format: 'DD',},'year':{  format: 'YYYY' },}
const Dopa_dateConfigOnlyDayYearTH = { 'date':{   format: 'DD', },'year':{   format: value => moment(value).add(543,'y').format('YYYY') },}
const Dopa_dateConfigOnlyMonthYear = { 'month':{   format: value => monthMap[value.getMonth() + 1]}, 'year':{   format: 'YYYY' },}
const Dopa_dateConfigOnlyMonthYearTH = { 'month':{   format: value => monthMap[value.getMonth() + 1] }, 'year':{   format: value => moment(value).add(543,'y').format('YYYY')},}
const Dopa_dateConfigOnlyYear = {'year':{format: 'YYYY'},}
const Dopa_dateConfigOnlyYearTH = {'year':{  format: value => moment(value).add(543,'y').format('YYYY') },}


class Formprofile_1 extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      time: new Date(),
      isOpenDatepickerMobile: false,
      startDate: new Date(),
      language: "th",
      isForeigner: (localStorage.getItem('inputBody') !== null ? (JSON.parse(localStorage.getItem('inputBody')).CARD_TYPE_ID != null && JSON.parse(localStorage.getItem('inputBody')).CARD_TYPE_ID === 2 ? "block" : "none") : 'none'),
      inputcard: (localStorage.getItem('inputBody') !== null ? (JSON.parse(localStorage.getItem('inputBody')).CARD_TYPE_ID != null && JSON.parse(localStorage.getItem('inputBody')).CARD_TYPE_ID === 2 ? false : true ) : true),
      agreement: false,
      title_name: "",
      gender: "",
      errors: {},
      errorsFocus: {
        CARD_ID: "",
        CARD_TYPE_ID: "",
        TITLE_NAME_ID: "",
        GENDER_ID: "",
        PHONE_NO: "",
        MARITAL_STATUS: "",
        FNAME_TH:"",
        LNAME_TH:""
      },
      marital_status: "",
      GENDER_ID: 1,
      fields: {
        CARD_ID_ENG: (localStorage.getItem('inputBody') !== null ? (JSON.parse(localStorage.getItem('inputBody')).CARD_ID_ENG != null ? JSON.parse(localStorage.getItem('inputBody')).CARD_ID_ENG : "" ): ''),
        TITLE_NAME_REMARK: (localStorage.getItem('inputBody') !== null ? (JSON.parse(localStorage.getItem('inputBody')).TITLE_NAME_REMARK != null ? JSON.parse(localStorage.getItem('inputBody')).TITLE_NAME_REMARK : "" ) : ''),
        FNAME_TH: ( localStorage.getItem('inputBody') !== null ? (JSON.parse(localStorage.getItem('inputBody')).FNAME_TH != null ? JSON.parse(localStorage.getItem('inputBody')).FNAME_TH : localStorage.getItem("_firstname")) : localStorage.getItem("_firstname")),
        LNAME_TH: (localStorage.getItem('inputBody') !== null ? (JSON.parse(localStorage.getItem('inputBody')).LNAME_TH != null ? JSON.parse(localStorage.getItem('inputBody')).LNAME_TH : localStorage.getItem("_lastname")) : localStorage.getItem("_lastname")),
        BIRTH_DATE: (localStorage.getItem('inputBody') !== null ?(JSON.parse(localStorage.getItem('inputBody')).BIRTH_DATE != null ? JSON.parse(localStorage.getItem('inputBody')).BIRTH_DATE : localStorage.getItem("_birthdate")) : localStorage.getItem("_birthdate")),
        CARD_ID: (localStorage.getItem('inputBody') !== null ? (JSON.parse(localStorage.getItem('inputBody')).CARD_ID != null ? JSON.parse(localStorage.getItem('inputBody')).CARD_ID : "" ) : ''),
        Laser: (localStorage.getItem('inputBody') !== null ? (JSON.parse(localStorage.getItem('inputBody')).Laser != null ? JSON.parse(localStorage.getItem('inputBody')).Laser : "" ) : ''),
        CARD_TYPE_ID: (localStorage.getItem('inputBody') !== null ? (JSON.parse(localStorage.getItem('inputBody')).CARD_TYPE_ID != null ? JSON.parse(localStorage.getItem('inputBody')).CARD_TYPE_ID : 1 ) : 1),
        TITLE_NAME_ID: (localStorage.getItem('inputBody') !== null ? (JSON.parse(localStorage.getItem('inputBody')).TITLE_NAME_ID != null ? JSON.parse(localStorage.getItem('inputBody')).TITLE_NAME_ID : 0 ): ''),
        GENDER_ID: (localStorage.getItem('inputBody') !== null ? (JSON.parse(localStorage.getItem('inputBody')).GENDER_ID != null ? JSON.parse(localStorage.getItem('inputBody')).GENDER_ID : 0 ) : ''),
        PHONE_NO: (localStorage.getItem('inputBody') !== null ? (JSON.parse(localStorage.getItem('inputBody')).PHONE_NO != null ? JSON.parse(localStorage.getItem('inputBody')).PHONE_NO : localStorage.getItem("_phid")) : localStorage.getItem("_phid")),
        USER_EMAIL: (localStorage.getItem('inputBody') !== null ? (JSON.parse(localStorage.getItem('inputBody')).USER_EMAIL != null ? JSON.parse(localStorage.getItem('inputBody')).USER_EMAIL : localStorage.getItem("_emid")) : localStorage.getItem("_emid")),
        MARITAL_STATUS: (localStorage.getItem('inputBody') !== null ? (JSON.parse(localStorage.getItem('inputBody')).MARITAL_STATUS != null ? JSON.parse(localStorage.getItem('inputBody')).MARITAL_STATUS : 0 ): ''),
        ADDR_NO: (localStorage.getItem('inputBody') !== null ? (JSON.parse(localStorage.getItem('inputBody')).ADDR_NO != null ? JSON.parse(localStorage.getItem('inputBody')).ADDR_NO : (JSON.parse(localStorage.getItem('inputBody')).CARD_TYPE_ID === 2 ? "90" : "")) : ""),
        ADDR_STREET: (localStorage.getItem('inputBody') !== null ? (JSON.parse(localStorage.getItem('inputBody')).ADDR_STREET != null ? JSON.parse(localStorage.getItem('inputBody')).ADDR_STREET : (JSON.parse(localStorage.getItem('inputBody')).CARD_TYPE_ID === 2 ? 'ถนนรัชดาภิเษก' : "")) : ""),
        ADDR_MOO: (localStorage.getItem('inputBody') !== null ? (JSON.parse(localStorage.getItem('inputBody')).ADDR_MOO != null ? JSON.parse(localStorage.getItem('inputBody')).ADDR_MOO : "" ) : ''),
        ADDR_VILLAGE: (localStorage.getItem('inputBody') !== null ? (JSON.parse(localStorage.getItem('inputBody')).ADDR_VILLAGE != null ? JSON.parse(localStorage.getItem('inputBody')).ADDR_VILLAGE : (JSON.parse(localStorage.getItem('inputBody')).CARD_TYPE_ID === 2 ? 'CW TOWER A (33rd Floor)' : "")): ''),
        ADDR_SOI: (localStorage.getItem('inputBody') !== null ? (JSON.parse(localStorage.getItem('inputBody')).ADDR_SOI != null ? JSON.parse(localStorage.getItem('inputBody')).ADDR_SOI : "" ) : ''),
        ADDR_ORG_PROVINCE_ID: (localStorage.getItem('inputBody') !== null ? (JSON.parse(localStorage.getItem('inputBody')).ADDR_ORG_PROVINCE_ID != null ? JSON.parse(localStorage.getItem('inputBody')).ADDR_ORG_PROVINCE_ID : (JSON.parse(localStorage.getItem('inputBody')).CARD_TYPE_ID === 2 ? '5957AE60-4992-E311-9402-0050568975FF' : "")): ''),
        ADDR_ORG_AMPHURE_ID: (localStorage.getItem('inputBody') !== null ?(JSON.parse(localStorage.getItem('inputBody')).ADDR_ORG_AMPHURE_ID != null ? JSON.parse(localStorage.getItem('inputBody')).ADDR_ORG_AMPHURE_ID : (JSON.parse(localStorage.getItem('inputBody')).CARD_TYPE_ID === 2 ? '9D16266E-C692-E311-9402-0050568975FF' : "")): ''),
        ADDR_ORG_SUB_DISTRICT_ID: (localStorage.getItem('inputBody') !== null ?(JSON.parse(localStorage.getItem('inputBody')).ADDR_ORG_SUB_DISTRICT_ID != null ? JSON.parse(localStorage.getItem('inputBody')).ADDR_ORG_SUB_DISTRICT_ID : (JSON.parse(localStorage.getItem('inputBody')).CARD_TYPE_ID === 2 ? 'B2B68FBA-D492-E311-9402-0050568975FF' : "")) : ''),
        ADDR_ORG_POSTALCODE_ID: (localStorage.getItem('inputBody') !== null ? (JSON.parse(localStorage.getItem('inputBody')).ADDR_ORG_POSTALCODE_ID != null ? JSON.parse(localStorage.getItem('inputBody')).ADDR_ORG_POSTALCODE_ID : (JSON.parse(localStorage.getItem('inputBody')).CARD_TYPE_ID=== 2 ? '84ED344B-D592-E311-9402-0050568975FF' : "")) : ''),
        ADDR_OTHER: (localStorage.getItem('inputBody') !== null ? (JSON.parse(localStorage.getItem('inputBody')).ADDR_OTHER != null ? JSON.parse(localStorage.getItem('inputBody')).ADDR_OTHER : (JSON.parse(localStorage.getItem('inputBody')).CARD_TYPE_ID === 2 ? '' : "")): ""),
      },
      TITLE_NAME_ID: 1,
      TITLE_NAME_REMARK: "none",
      onOpenAlert: null,
      content_loading: "none",

      DopaBirthDay: "DMY",
      DopaBirthDayValue: "",
      DopaBirthDayShow: "",
      DopaBirthDayText: "วัน/เดือน/ปีเกิด",
      isOpenDOPASelectDate: false,
    };
    this.handleChange = this.handleChange.bind(this);
  }

  renderDatepickerMobile = () => {
    this.setState({ isOpenDatepickerMobile: true });
  }

  handleCancelDatepickerMobile = () => {
      this.setState({ isOpenDatepickerMobile: false });
  }

  handleSelectDatepickerMobile = (time) => {
    let { fields } = this.state;
    fields['BIRTH_DATE'] = moment(time).format('YYYY-MM-DD');
    this.setState({ fields, isOpenDatepickerMobile: false });
  }

  componentDidMount() {
 
    // localStorage.setItem('inputBody','{}');
    if(sessionStorage.getItem('test') ===  null){
      localStorage.clear();
      sessionStorage.clear();
    }

    let SessionData = JSON.parse(localStorage.getItem('inputBody'))
    let { fields ,TITLE_NAME_REMARK , checkType } = this.state;

    const params = this.props.match.params;
    this.setState({
      language: params.lang
        ? params.lang.toString().toLowerCase()
        : API_LIST.defaultLang
    });
    var tokenData = this.props.location;
    var sstr = tokenData.search.search("&");
    var sub_memberid = tokenData.search.substr(sstr);
    var memberid = sub_memberid.substr(5); 

    localStorage.setItem('_usid', memberid)
    localStorage.setItem('_phid', params.phoneid)
    localStorage.setItem('_emid', params.emailid)
    localStorage.setItem('_firstname' , params.firstname)
    localStorage.setItem('_lastname' , params.lastname)
    localStorage.setItem('_birthdate' , params.birthdate)
    localStorage.setItem('_udid' , params.udid)
    localStorage.setItem('_deviceos' , params.deviceos)

    if(params.lang === "th"){
      this.setState({
        DopaBirthDayValue:  (localStorage.getItem('inputBody') !== null ?(JSON.parse(localStorage.getItem('inputBody')).BIRTH_DATE != null ? JSON.parse(localStorage.getItem('inputBody')).BIRTH_DATE : moment(localStorage.getItem("_birthdate")).add(543,'y').format('DD-MM-YYYY')) : moment(localStorage.getItem("_birthdate")).add(543,'y').format('DD-MM-YYYY')),
        DopaBirthDayShow: (localStorage.getItem('inputBody') !== null ?(JSON.parse(localStorage.getItem('inputBody')).BIRTH_DATE != null ? JSON.parse(localStorage.getItem('inputBody')).BIRTH_DATE : moment(localStorage.getItem("_birthdate")).add(543,'y').format('DD/MM/YYYY')) : moment(localStorage.getItem("_birthdate")).add(543,'y').format('DD/MM/YYYY')),
      })
    } else {
      this.setState({
        DopaBirthDayValue:  (localStorage.getItem('inputBody') !== null ?(JSON.parse(localStorage.getItem('inputBody')).BIRTH_DATE != null ? JSON.parse(localStorage.getItem('inputBody')).BIRTH_DATE : moment(localStorage.getItem("_birthdate")).format('DD-MM-YYYY')) : moment(localStorage.getItem("_birthdate")).format('DD-MM-YYYY')),
        DopaBirthDayShow: (localStorage.getItem('inputBody') !== null ?(JSON.parse(localStorage.getItem('inputBody')).BIRTH_DATE != null ? JSON.parse(localStorage.getItem('inputBody')).BIRTH_DATE : moment(localStorage.getItem("_birthdate")).format('DD/MM/YYYY')) : moment(localStorage.getItem("_birthdate")).format('DD/MM/YYYY')),
      })
    }
   
    this.getDropdownlist_type("TITLE").then(result => {
      this.setState({ title_name: result.dropdowN_INFO });
    });
    this.getDropdownlist_type("GENDER").then(result => {
      this.setState({ gender: result.dropdowN_INFO });
    });
    this.getDropdownlist_type("MARITAL_STATUS").then(result => {
      this.setState({ marital_status: result.dropdowN_INFO });
    });

    if(SessionData != null) {
      let newObject = Object.assign(SessionData , this.state.fields);
      fields = newObject
      if (fields.TITLE_NAME_ID === 4) {
        TITLE_NAME_REMARK = "block";
      }
      this.setState({ fields, TITLE_NAME_REMARK , checkType })
    }
  }

  changeHandle(e) {
    var { errorsFocus,errors, fields } = this.state;
    var checkedForeign = (e.target.checked === true) ? 2 : 1
    fields["CARD_TYPE_ID"] = checkedForeign
    if(checkedForeign === 1) {
      errorsFocus['CARD_ID_ENG'] = '';
      errors['CARD_ID_ENG'] = '';
      fields['CARD_ID_ENG'] = '';
      fields['CARD_ID'] = '';
      fields['Laser'] = '';
      fields['ADDR_NO'] = '';
      fields['ADDR_STREET'] = '';
      fields['ADDR_VILLAGE'] = '';
      fields['ADDR_ORG_PROVINCE_ID'] = '';
      fields['ADDR_ORG_AMPHURE_ID'] = '';
      fields['ADDR_ORG_SUB_DISTRICT_ID'] = '';
      fields['ADDR_ORG_POSTALCODE_ID'] = '';
      fields['ADDR_OTHER'] = '';
    } else {
      errorsFocus['CARD_ID'] = '';
      errors['CARD_ID'] = '';
      fields['CARD_ID'] = '';
      fields['Laser'] = '';
      fields['ADDR_NO'] = '90';
      fields['ADDR_STREET'] = 'ถนนรัชดาภิเษก';
      fields['ADDR_VILLAGE'] = 'CW TOWER A (33rd Floor)';
      fields['ADDR_ORG_PROVINCE_ID'] = '5957AE60-4992-E311-9402-0050568975FF';
      fields['ADDR_ORG_AMPHURE_ID'] = '9D16266E-C692-E311-9402-0050568975F';
      fields['ADDR_ORG_SUB_DISTRICT_ID'] = 'B2B68FBA-D492-E311-9402-0050568975FF';
      fields['ADDR_ORG_POSTALCODE_ID'] = '84ED344B-D592-E311-9402-0050568975FF';
    }
      
    this.setState({ errorsFocus, errors, fields });
    this.setState({
      isForeigner: e.target.checked === true ? "block" : "none"
    });
    this.setState({
      inputcard: e.target.checked === true ? false : true
    })
  }

  getDropdownlist_type(title) {
    return regisMaxCard.getDropdownlist(title);
  }

  renderContent(type) {
    const { title_name, gender, marital_status } = this.state;
    var option = [];
    var name_title;
    if (type === "type_title_name") {
      name_title = title_name;
    } else if (type === "type_gender") {
      name_title = gender;
    } else {
      name_title = marital_status;
    }
    for (var i in name_title) {
      option.push(
        <option key={i} value={name_title[i].code}>
          {name_title[i].values}
        </option>
      );
    }
    return option;
  }

    async handleSubmit(event) {
        var { fields } = this.state;
        event.preventDefault();
        if (await this.validateForm()) {
            this.setState({ content_loading: "block" })
            this.onCheckDopa()
        } else {
        console.log('formsubmit ' + false);
        }
    }

    onCheckDopa(){
        const { fields, DopaBirthDayValue, language } = this.state
        const checkDopaBirthDayValue = (fields['BIRTH_DATE']).split('-');
        var day = checkDopaBirthDayValue[2]
        var month = checkDopaBirthDayValue[1]
        var setyear = checkDopaBirthDayValue[0]
        if(language === "th"){
          var year  = parseInt(setyear) + 543
        } else {
          var year  = parseInt(setyear) + 543
        }
        var dataForEncrypt = `{
            "PID": "${fields["CARD_ID"]}",
            "FirstName": "${fields["FNAME_TH"]}",
            "LastName": "${fields["LNAME_TH"]}",
            "DateOfBirth": "${day}",
            "MonthOfBirth": "${month}",
            "YearOfBirth": "${year}",
            "Laser": "${fields["Laser"]}"
        }`

        const ENC_KEY = "S6ZKJTl3yqzsNmckMhE4Pi0p1Br7GnbX";
        const IV = "k4Iaf1LKsRUZ2Evi";
        let cipher = crypto.createCipheriv('aes-256-cbc', Buffer.from(ENC_KEY), IV);
        let encrypted = cipher.update(dataForEncrypt);
        encrypted = Buffer.concat([encrypted, cipher.final()]);
        var dataEncrypt = encrypted.toString('base64');
        var iat = new Date().getTime();
        var exp = new Date(iat * 1000).getTime();
        var tokenForEncrypt = `{
            "iat": ${iat},
            "exp": ${exp},
            "data": "${dataEncrypt}"
        }`
        var tokenEncryptBase64 = Buffer.from(tokenForEncrypt).toString('base64');
        var dataToken = {
            "token": `${tokenEncryptBase64}`
        }
        // var auth = "MAXREWARDS:5Dj0ASMW2EB8as9hQX67JmLekuOKYTCx"
        // var authBase64 = Buffer.from(auth).toString('base64');
        // console.log("authBase64",authBase64)
        regisMaxCard.CheckCardByLaser(dataToken).then(e => {
            var response = e.response
            if(response.code === 0){
                this.onNextToScreen()
            } else {
                var msg = response.message;
                var img = `${process.env.PUBLIC_URL}/images/cancel.png`;
                this.onOpenAlert(msg,img);
            }
        })
    }

    onOpenAlert(msg,img){
        alert = (
            <SweetAlert
                custom
                confirmBtnBsStyle="success"
                closeOnClickOutside={true}
                cancelBtnBsStyle="default"
                focusConfirmBtn={false}
                title=""
                customIcon={img}
                showConfirm={false}
                showCancelButton
                onCancel={() => this.onCloseAlert(false)}
                onOutsideClick={() => {
                    this.setState({ onOpenAlert: null })
                }}
            >
                <div className="">
                    {msg}
                </div>
            </SweetAlert>
        );
        this.setState({ 
            onOpenAlert: alert,
            content_loading: "none"
        })
    }

    onCloseAlert(){
        this.setState({ 
            onOpenAlert: null,
            content_loading: "none"
        })
    }

    onNextToScreen(){
        this.setState({ content_loading: "none" })
        var { fields } = this.state;
        if(fields['CARD_TYPE_ID'] === 2) {
            fields['CARD_ID'] = fields['CARD_ID_ENG']
            this.setState({ fields })
        }
        localStorage.setItem('inputBody', JSON.stringify(this.state.fields));
        this.props.history.push({
            pathname: `${process.env.PUBLIC_URL}/formprofile_2/${this.state.language}`,
                state: {
                data: this.state.fields
            }
        });
    }

  async validateForm() {
    let fields = this.state.fields;
    let errors = {};
    let errorsFocus = {};
    let formIsValid = true;
    
    if(fields['CARD_TYPE_ID'] === 1) {
      if (!fields["CARD_ID"]) {
          formIsValid = false;
          errorsFocus["CARD_ID"] = 'errorFocus'
          errors["CARD_ID"] = <FormattedMessage id="ALERT_CARD_ID" />;
      } else {
          var chkValidCard = await this.checkCard(this.state.fields["CARD_TYPE_ID"],this.state.fields["CARD_ID"]);
          if (chkValidCard.status === true) {
            formIsValid = false;
            errorsFocus["CARD_ID"] = 'errorFocus'
            errors["CARD_ID"] = (chkValidCard.msg === "" ? <FormattedMessage id="ALERT_CARD_NOT_USE" /> : chkValidCard.msg)
          }
      }
    } else {
      if (!fields["CARD_ID_ENG"]) {
          formIsValid = false;
          errorsFocus["CARD_ID_ENG"] = 'errorFocus'
          errors["CARD_ID_ENG"] = <FormattedMessage id="ALERT_CARD_ID" />;
      }else{
        var chkValidCard = await this.checkCard(this.state.fields["CARD_TYPE_ID"],this.state.fields["CARD_ID_ENG"]);
        if(chkValidCard.status === true){
          formIsValid = false;
          errorsFocus["CARD_ID_ENG"] = 'errorFocus'
          errors["CARD_ID_ENG"] = (chkValidCard.msg === "" ? <FormattedMessage id="ALERT_PASSSPORT_USE" /> : chkValidCard.msg)
        }
      }
    }

    if (!fields["TITLE_NAME_ID"]) {
      formIsValid = false;
      errorsFocus["TITLE_NAME_ID"] = 'errorFocus'
      errors["TITLE_NAME_ID"] = <FormattedMessage id="ALERT_TITLE_NAME_ID" />;
    }
    if(fields["TITLE_NAME_ID"] === 4) {
      if (!fields["TITLE_NAME_REMARK"]) {
        formIsValid = false;
        errorsFocus["TITLE_NAME_REMARK"] = 'errorFocus'
        errors["TITLE_NAME_REMARK"] = (
          <FormattedMessage id="ALERT_TITLE_NAME_ID" />
        );
      }
    }
    if (!fields["FNAME_TH"]) {
      formIsValid = false;
      errorsFocus["FNAME_TH"] = 'errorFocus'
      errors["FNAME_TH"] = <FormattedMessage id="ALERT_FNAME_TH" />;
    }
    if (!fields["LNAME_TH"]) {
      formIsValid = false;
      errorsFocus["LNAME_TH"] = 'errorFocus'
      errors["LNAME_TH"] = <FormattedMessage id="ALERT_LNAME_TH" />;
    }
    if (!fields["GENDER_ID"]) {
      formIsValid = false;
      errorsFocus["GENDER_ID"] = 'errorFocus'
      errors["GENDER_ID"] = <FormattedMessage id="ALERT_GENDER_ID" />;
    } else {
      if (fields["GENDER_ID"] === 4) {
        if (fields["GENDER_ID"]) {
          formIsValid = false;
          errorsFocus["GENDER_ID"] = 'errorFocus'
          errors["GENDER_ID"] = (
            <FormattedMessage id="ALERT_GENDER_ID" />
          );
        }
      }
    }

    if (!fields["BIRTH_DATE"] || fields["BIRTH_DATE"] == 'undefined') {
      formIsValid = false;
      errorsFocus["BIRTH_DATE"] = 'errorFocus'
      errors["BIRTH_DATE"] = <FormattedMessage id="ALERT_BIRTH_DATE" />;
    } else {
      var dateBeforDiff = moment(fields["BIRTH_DATE"]).format("YYYY-MM-DD");
      var diff = moment().diff(moment(dateBeforDiff, "YYYY-MM-DD"), "years");
      var diffDay = moment().diff(moment(dateBeforDiff, "YYYY-MM-DD"), "days");
      if (diff < 7 || diffDay <= 2556) {
        formIsValid = false;
        errorsFocus["BIRTH_DATE"] = 'errorFocus'
        errors["BIRTH_DATE"] = <FormattedMessage id="ALERT_BIRTH_DATE_YEAR" />;
      }
    }
    if(!fields["MARITAL_STATUS"] || fields["MARITAL_STATUS"] === '0'){
      formIsValid = false;
      errorsFocus["MARITAL_STATUS"] = 'errorFocus'
      errors["MARITAL_STATUS"] = <FormattedMessage id="MARITAL_STATUS" />
    }
    // if (!fields["USER_EMAIL"]) {
    //   formIsValid = false;
    //   errorsFocus["USER_EMAIL"] = 'errorFocus'
    //   errors["USER_EMAIL"] = <FormattedMessage id="ALERT_USER_EMAIL" />;
    // } else {
    if(fields["USER_EMAIL"] != "" && fields["USER_EMAIL"] != undefined && fields["USER_EMAIL"] != undefined){
      var validEmail = this.validateEmail(fields["USER_EMAIL"]);
      if (validEmail === false) {
        formIsValid = false;
        errorsFocus["USER_EMAIL"] = 'errorFocus'
        errors["USER_EMAIL"] = <FormattedMessage id="ALERT_EMAIL_FORMAT" />;
      } else {
        var chkValidEmail = await this.checkEmail();
        if (chkValidEmail === true) {
          formIsValid = false;
          errorsFocus["USER_EMAIL"] = 'errorFocus'
          errors["USER_EMAIL"] = <FormattedMessage id="ALERT_EMAIL_CANT_USE" />;
        }
      }
    }
    if (!fields["PHONE_NO"]) {
      formIsValid = false;
      errorsFocus["PHONE_NO"] = 'errorFocus'
      errors["PHONE_NO"] = <FormattedMessage id="ALERT_PHONE_NO" />;
    } else {
      if(fields["PHONE_NO"].toString().length ) {}
      var validPhone = this.validatePhone(fields["PHONE_NO"])
      if(validPhone === false) {
        formIsValid = false;
        errorsFocus["PHONE_NO"] = 'errorFocus'
        errors["PHONE_NO"] = <FormattedMessage id="ALERT_PHONE_FORMAT" />
      } else {
        var chkValidPhone = await this.checkPhoneExist();
        if (chkValidPhone.status === true) {
          formIsValid = false;
          errorsFocus["PHONE_NO"] = 'errorFocus'
          errors["PHONE_NO"] = (chkValidPhone.msg === "" ? <FormattedMessage id="ALERT_PHONE_CANT_USE" /> : chkValidPhone.msg)
        }
        // var chkValidPhone = await this.checkPhoneExist();
        // if (chkValidPhone === true) {
        //   formIsValid = false;
        //   errorsFocus["PHONE_NO"] = 'errorFocus'
        //   errors["PHONE_NO"] = <FormattedMessage id="ALERT_PHONE_CANT_USE" />;
        // }
      }
      if (!fields["Laser"]) {
        formIsValid = false;
        errorsFocus["Laser"] = 'errorFocus'
        errors["Laser"] = "กรุณาระบุหมายเลข Laser Code";
      } else if(fields["Laser"].length < 12) {
        formIsValid = false;
        errorsFocus["Laser"] = 'errorFocus'
        errors["Laser"] = "กรุณาระบุหมายเลข Laser Code 12 หลัก";
      }
      if (this.state.DopaBirthDayValue === "") {
        formIsValid = false;
        errorsFocus["DopaBirthDayValue"] = 'errorFocus'
        errors["DopaBirthDayValue"] = "กรุณาเลือก";
      } 
    }
    this.setState({
      errors: errors,
      errorsFocus: errorsFocus
    });

    return formIsValid;
  }

  handleChange(e) {
    let { errors,errorsFocus, fields, GENDER_ID, TITLE_NAME_REMARK } = this.state;
    if (e.target.name === "TITLE_NAME_ID") {
      if (
        e.target.value === "1" ||
        e.target.value === "2" ||
        e.target.value === "3"
      ) {
        GENDER_ID = e.target.value === "1" ? 1 : 2;
        fields["GENDER_ID"] = GENDER_ID;
        genderClass = "form-control disabled";
        TITLE_NAME_REMARK = "none";
        fields["TITLE_NAME_REMARK"] = '';
        errors["GENDER_ID"]  = null;
        errorsFocus["GENDER_ID"]  = ''

      } else {
        fields["GENDER_ID"] = 4;
        genderClass = "form-control";
        TITLE_NAME_REMARK = "block";
      }
      errors[e.target.name] = null;
      errorsFocus[e.target.name] = ''
      // errors["GENDER_ID"] = null;
      // errorsFocus["GENDER_ID"] = '';
      fields[e.target.name] = parseInt(e.target.value);
    } else if (e.target.name === "CARD_ID") {
      const re = /^[0-9\b]+$/;
      if (e.target.value === "" || re.test(e.target.value)) {
        fields[e.target.name] = e.target.value;
        errors[e.target.name] = null;
        errorsFocus[e.target.name] = ''
      } else {
        e.preventDefault();
        return false;
      }
    } else if (e.target.name === "PHONE_NO") {
      const re = /^[0-9\b]+$/;
      if (e.target.value === "" || re.test(e.target.value)) {
        fields[e.target.name] = e.target.value;
        errors[e.target.name] = null;
        errorsFocus[e.target.name] = ''
      } else {
        errors[e.target.name] = <FormattedMessage id="PREVENT_PHONE_NO" />;
        errorsFocus[e.target.name] = ''
        e.preventDefault();
      }
    } else if (e.target.name === "Laser") {
        fields[e.target.name] = e.target.value.replace(/[^A-Za-z][^0-9]/g, '')
        errors[e.target.name] = null;
        errorsFocus[e.target.name] = ''
    } else {
      fields[e.target.name] = e.target.value;
      errors[e.target.name] = null;
      errorsFocus[e.target.name] = ''
    }

    
    this.setState({ errors, fields, GENDER_ID, TITLE_NAME_REMARK });
  }

  validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }

  async checkEmail() {
    return regisMaxCard.checkEmailExist(this.state.fields["USER_EMAIL"]).then(e => {
      var resp = e.data.responsE_INFO;
      if(resp != null){
        if (resp.rescode !== "000") {
          ERROR_USER_EMAIL = false
        } else {
          ERROR_USER_EMAIL = true
        }
      }else{
        console.log('null')
      }

      return ERROR_USER_EMAIL
    });
  }

  async checkPhoneExist() {
    return regisMaxCard.checkPhoneExist(this.state.fields["PHONE_NO"]).then(e => {
      // var resp = e.data.responsE_INFO;
      // if (resp.rescode != "000") {
      //   ERROR_USER_PHONE = false
      // } else {
      //   ERROR_USER_PHONE = true
      // }
      // return ERROR_USER_PHONE
      var resp = e.data.responsE_INFO;
      var response = {}
      if (resp.rescode !== "041")   {
        if(resp.rescode === "042") {
          response = {
            status: true,
            msg: <FormattedMessage id="ALERT_PHONE_NOTFORMAT" />
          }
        } else {
          response = {
            status: true,
            msg: ""
          }
        }
      } else {
        response = {
          status: false,
          msg: ""
        }
      }
      return response
    });
  }

  validatePhone(phonenumber) {
    // var re = /^[0]{1}[689]{1}[0-9]{8,9}/;
    var re = /^[0]{1}[689]/;
    return re.test(String(phonenumber));
  }

  async checkCard(type, card) {
    return regisMaxCard.checkexistingIdCard(type, card).then(e => {
      var resp = e.data.responsE_INFO;
      var response = {}
      if(resp != null){
        if (resp.rescode !== "031")   {
          if(resp.rescode === "032") {
            response = {
              status: true,
              msg: <FormattedMessage id="ALERT_CARD_FALSE" />
            }
          } else {
            response = {
              status: true,
              msg: ""
            }
          }
        } else {
          response = {
            status: false,
            msg: ""
          }
        }
      }else{
        response = {
          status: false,
          msg: ""
        }
      }
      return response
    });
  }

  onChangeDopaBirthDay(type, text){
    this.setState({ 
      DopaBirthDay: type,
      DopaBirthDayText: text,
      DopaBirthDayValue: "",
      DopaBirthDayShow: "",
    })
  }

  onOpenDOPASelectDate = () => {
      this.setState({ 
        isOpenDOPASelectDate: true,
      });
  }

  onCloseDOPASelectDate = () => {
      this.setState({ 
        isOpenDOPASelectDate: false,
      });
  }

  onSelectDOPASelectDate = (time) => {
    const { DopaBirthDay, errors,errorsFocus, } = this.state
    if(DopaBirthDay === "DMY"){
      if(this.state.language === "th"){
        var DopaBirthDayValue = moment(time).add(543,'y').format('DD-MM-YYYY');
        var DopaBirthDayShow = moment(time).add(543,'y').format('DD/MM/YYYY');
      } else {
        var DopaBirthDayValue = moment(time).format('DD-MM-YYYY');
        var DopaBirthDayShow = moment(time).format('DD/MM/YYYY');
      }
    } else if(DopaBirthDay === "DY"){
      var getDay = moment(time).format('DD');
      if(this.state.language === "th"){
        var getYear = moment(time).add(543,'y').format('YYYY');
      } else {
        var getYear = moment(time).format('YYYY');
      }
      var DopaBirthDayValue = getDay + "-00-" + getYear
      var DopaBirthDayShow = getDay + "/" + getYear
    } else if(DopaBirthDay === "MY"){
      var getMonth = moment(time).format('MM');
      if(this.state.language === "th"){
        var getYear = moment(time).add(543,'y').format('YYYY');
      } else {
        var getYear = moment(time).format('YYYY');
      }
      var DopaBirthDayValue = "00-" + getMonth + "-" + getYear
      var DopaBirthDayShow = getMonth + "/" + getYear
    } else if(DopaBirthDay === "Y"){
      if(this.state.language === "th"){
        var getYear = moment(time).add(543,'y').format('YYYY');
      } else {
        var getYear = moment(time).format('YYYY');
      }
      var DopaBirthDayValue = "00-00-" + getYear
      var DopaBirthDayShow = getYear
    }
    errors['DopaBirthDayValue'] = null;
    errorsFocus['DopaBirthDayValue'] = ''
    this.setState({ 
      DopaBirthDayValue: DopaBirthDayValue, 
      DopaBirthDayShow: DopaBirthDayShow, 
      isOpenDOPASelectDate: false,
      errors,
      errorsFocus
    });
  } 

  render() {
    var yearnow = new Date().getFullYear()
    var start = yearnow - parseInt(100);
    var setYear = [];
    // for(start ; yearnow >= start ; yearnow--){
    //   setYear.push(yearnow + 543)
    // }
    var minDate = new Date();
    minDate.setFullYear(minDate.getFullYear() - 100);

    var date_Config = (this.state.fields['CARD_TYPE_ID'] == 2 || this.state.language == 'en' ? dateConfig : dateConfigTH)

    const { onOpenAlert, DopaBirthDay } = this.state
    if(DopaBirthDay === "DMY"){
      var Dopa_date_Config = (this.state.fields['CARD_TYPE_ID'] == 2 || this.state.language == 'en' ? date_Config : dateConfigTH)
    } else if(DopaBirthDay === "DY"){
      var Dopa_date_Config = (this.state.fields['CARD_TYPE_ID'] == 2 || this.state.language == 'en' ? Dopa_dateConfigOnlyDayYear : Dopa_dateConfigOnlyDayYearTH)
    } else if(DopaBirthDay === "MY"){
      var Dopa_date_Config = (this.state.fields['CARD_TYPE_ID'] == 2 || this.state.language == 'en' ? Dopa_dateConfigOnlyMonthYear : Dopa_dateConfigOnlyMonthYearTH)
    } else if(DopaBirthDay === "Y"){
      var Dopa_date_Config = (this.state.fields['CARD_TYPE_ID'] == 2 || this.state.language == 'en' ? Dopa_dateConfigOnlyYear : Dopa_dateConfigOnlyYearTH)
    }

    var selectTime = new Date()

    return (
      <IntlProvider
        locale={this.state.language}
        messages={messages[this.state.language].formprofile1}
      >
        <form className="form-horizontal formprofile_1" onSubmit={e => this.handleSubmit(e)}>
          <div className="bg_full">
            {onOpenAlert}
            <div id="loading" style={{ display: this.state.content_loading}}>
                <div className="content_class">
                    <img src={`${process.env.PUBLIC_URL}/images/Loading.gif`} />
                </div>
            </div>
            <div className="container-fluid">
              <div className="content_page"style={{ display: this.state.content_loading === "block" ? "none" : "block"}}>
                <div className="head_title_agree">
                  <FormattedMessage id="header_profile" />
                </div>
                <div 
                  className="form-group" 
                >
                  <label>
                    <span className="required">* </span><FormattedMessage id="title_idnumber" />
                  </label>
                  <input disabled={!this.state.inputcard}
                    type="text"
                    className={`form-control input_form ${this.state.errorsFocus['CARD_ID']}`}
                    maxLength="13"
                    name="CARD_ID"
                    onChange={e => this.handleChange(e)}
                    id="exampleFormControlInput1"
                    placeholder={
                      messages[this.state.language].formprofile1.idcard_input
                    }
                    value={this.state.fields["CARD_TYPE_ID"] == 1 ? this.state.fields["CARD_ID"] : ''}
                  />
                  <div className="errorMsg CARD_ID">{this.state.errors["CARD_ID"]}</div>
                </div>

                <div className="form-group" >
                    <label>
                        <span className="required">* </span>หมายเลข Laser code
                    </label>
                    <input disabled={!this.state.inputcard}
                        type="text"
                        className={`form-control input_form ${this.state.errorsFocus['Laser']}`}
                        maxLength="12"
                        name="Laser"
                        onChange={e => this.handleChange(e)}
                        id="exampleFormControlInput1"
                        placeholder="หมายเลข Laser code (หลังบัตรประชาชน)"
                        value={this.state.fields["CARD_TYPE_ID"] == 1 ? this.state.fields["Laser"] : ''}
                    />
                    <span className="required">ตัวอย่างกรอก Laser Code :</span> JT9999999999
                    <div className="errorMsg Laser">{this.state.errors["Laser"]}</div>
                </div>

                {/* --- open วันเกิดในบัตรประชาชน --- */}
                {/* {this.state.inputcard ? 
                <div className="pb-2 pt-1">
                  <div className="row">
                    <a className="col-12 d-flex mb-1" onClick={() => this.onChangeDopaBirthDay('DMY', "วัน/เดือน/ปีเกิด")}>
                      {this.state.DopaBirthDay === "DMY" ?
                      <img src={`${process.env.PUBLIC_URL}/images/checked.png`} className="my-auto dopa-birthdayimage"/>
                      :
                      <div className="dopa-birthdaycheck my-auto"></div>
                      }
                      <span className="dopa-birthdaytext">มีวัน/เดือน/ปีเกิด</span>
                    </a>
                    <a className="col-12 d-flex mb-1" onClick={() => this.onChangeDopaBirthDay('DY', "วัน/ปีเกิด")}>
                      {this.state.DopaBirthDay === "DY" ?
                      <img src={`${process.env.PUBLIC_URL}/images/checked.png`} className="my-auto dopa-birthdayimage"/>
                      :
                      <div className="dopa-birthdaycheck my-auto"></div>
                      }
                      <span className="dopa-birthdaytext">มีเฉพาะวันและปีเกิด</span>
                    </a>
                    <a className="col-12 d-flex mb-1" onClick={() => this.onChangeDopaBirthDay('MY', "เดือน/ปีเกิด")}>
                      {this.state.DopaBirthDay === "MY" ?
                      <img src={`${process.env.PUBLIC_URL}/images/checked.png`} className="my-auto dopa-birthdayimage"/>
                      :
                      <div className="dopa-birthdaycheck my-auto"></div>
                      }
                      <span className="dopa-birthdaytext">มีเฉพาะเดือนและปีเกิด</span>
                    </a>
                    <a className="col-12 d-flex mb-1" onClick={() => this.onChangeDopaBirthDay('Y', "ปีเกิด")}>
                      {this.state.DopaBirthDay === "Y" ?
                      <img src={`${process.env.PUBLIC_URL}/images/checked.png`} className="my-auto dopa-birthdayimage"/>
                      :
                      <div className="dopa-birthdaycheck my-auto"></div>
                      }
                      <span className="dopa-birthdaytext">มีเฉพาะปีเกิด</span>
                    </a>
                  </div>
                  <div className="form-group mt-1">
                    <div className={`form-control ${this.state.errorsFocus['DopaBirthDayValue']} input_form`} onClick={this.onOpenDOPASelectDate}>
                      {this.state.DopaBirthDayShow == "" ? this.state.DopaBirthDayText : (this.state.fields['CARD_TYPE_ID'] == 2 || this.state.DopaBirthDayShow) }
                    </div>
                    <div className="errorMsg DopaBirthDayValue">{this.state.errors["DopaBirthDayValue"]}</div>
                  </div>
                </div>
                  :
                  <div></div>
                 }    */}

                {/* --- close วันเกิดในบัตรประชาชน --- */}


                <div className="pb-2 pt-1">
                  <div className="row">
                    <a className="col-12 d-flex mb-1" onClick={() => this.onChangeDopaBirthDay('DMY', "วัน/เดือน/ปีเกิด")}>
                      {this.state.DopaBirthDay === "DMY" ?
                      <img src={`${process.env.PUBLIC_URL}/images/checked.png`} className="my-auto dopa-birthdayimage"/>
                      :
                      <div className="dopa-birthdaycheck my-auto"></div>
                      }
                      <span className="dopa-birthdaytext">มีวัน/เดือน/ปีเกิด</span>
                    </a>
                    <a className="col-12 d-flex mb-1" onClick={() => this.onChangeDopaBirthDay('DY', "วัน/ปีเกิด")}>
                      {this.state.DopaBirthDay === "DY" ?
                      <img src={`${process.env.PUBLIC_URL}/images/checked.png`} className="my-auto dopa-birthdayimage"/>
                      :
                      <div className="dopa-birthdaycheck my-auto"></div>
                      }
                      <span className="dopa-birthdaytext">มีเฉพาะวันและปีเกิด</span>
                    </a>
                    <a className="col-12 d-flex mb-1" onClick={() => this.onChangeDopaBirthDay('MY', "เดือน/ปีเกิด")}>
                      {this.state.DopaBirthDay === "MY" ?
                      <img src={`${process.env.PUBLIC_URL}/images/checked.png`} className="my-auto dopa-birthdayimage"/>
                      :
                      <div className="dopa-birthdaycheck my-auto"></div>
                      }
                      <span className="dopa-birthdaytext">มีเฉพาะเดือนและปีเกิด</span>
                    </a>
                    <a className="col-12 d-flex mb-1" onClick={() => this.onChangeDopaBirthDay('Y', "ปีเกิด")}>
                      {this.state.DopaBirthDay === "Y" ?
                      <img src={`${process.env.PUBLIC_URL}/images/checked.png`} className="my-auto dopa-birthdayimage"/>
                      :
                      <div className="dopa-birthdaycheck my-auto"></div>
                      }
                      <span className="dopa-birthdaytext">มีเฉพาะปีเกิด</span>
                    </a>
                  </div>
                  

                  <div className="form-group">
                  <div className={`form-control ${this.state.errorsFocus['BIRTH_DATE']} input_form`} onClick={this.renderDatepickerMobile}>
                    {this.state.fields['BIRTH_DATE'] == "" ? <FormattedMessage id="title_dateofbirth_select" /> : (this.state.fields['CARD_TYPE_ID'] == 2 || this.state.language == 'en' ? moment(this.state.fields['BIRTH_DATE']).format('DD/MM/YYYY') : moment(this.state.fields['BIRTH_DATE']).add(543,'y').format('DD/MM/YYYY')) }
                  </div>
                  <input
                    type="hidden"
                    name="BIRTH_DATE"
                    onChange={e => this.handleChange(e)}
                    className={`form-control ${this.state.errorsFocus['BIRTH_DATE']} input_form`}
                    id="date"
                    value={this.state.fields['BIRTH_DATE']}
                  />
                  <div className="errorMsg BIRTH_DATE">
                    {this.state.errors["BIRTH_DATE"]}
                  </div>
                </div>
                </div>

                {/* <label className="check_conditions_form">
                  <FormattedMessage id="title_foreigners" />
                  <input
                    type="checkbox"
                    name="aggreement"
                    checked={(this.state.fields["CARD_TYPE_ID"] === 1 ? false : true)}
                    onChange={e => this.changeHandle(e)}
                  />
                  <span className="checkmark_condition" />
                </label> */}

                <div
                  className="form-group"
                  style={{ display: this.state.isForeigner }}
                >
                  <input
                    type="text"
                    className={`form-control input_form ${this.state.errorsFocus['CARD_ID_ENG']}`}
                    name="CARD_ID_ENG"
                    maxLength="10"
                    onChange={(e) => this.handleChange(e)}
                    id="exampleFormControlInput1"
                    placeholder={
                      messages[this.state.language].formprofile1
                        .idcard_inputforeigners
                    }
                    value={this.state.fields['CARD_ID_ENG']}
                  />
                  <div className="errorMsg CARD_ID_ENG">{this.state.errors["CARD_ID_ENG"]}</div>
                </div>

                <div className="head_title_agree">
                  <FormattedMessage id="title_user" />
                </div>
                <div className="form-group">
                  <label>
                    <span className="required">* </span><FormattedMessage id="title_nametitle" />
                  </label>
                  <select
                    className={`form-control ${this.state.errorsFocus['TITLE_NAME_ID']}`}
                    name="TITLE_NAME_ID"
                    id="exampleFormControlSelect1"
                    onChange={e => this.handleChange(e)}
                    value={this.state.fields['TITLE_NAME_ID']}
                  >
                    <option value="0">
                      {messages[this.state.language].formprofile1.SELECT_TITLE}
                    </option>
                    {this.renderContent("type_title_name")}
                  </select>
                  <div className="errorMsg TITLE_NAME_ID">
                    {this.state.errors["TITLE_NAME_ID"]}
                  </div>
                  <div
                    className="form-group"
                    style={{ display: this.state.TITLE_NAME_REMARK }}
                  >
                    <input
                      type="text"
                      className={`form-control ${this.state.errorsFocus['TITLE_NAME_REMARK']} input_form`}
                      name="TITLE_NAME_REMARK"
                      onChange={e => this.handleChange(e)}
                      id="exampleFormControlInput1"
                      placeholder={
                        messages[this.state.language].formprofile1
                          .title_name_remark
                      }
                      value={this.state.fields['TITLE_NAME_REMARK']}
                    />
                    <div className="errorMsg TITLE_NAME_REMARK">
                      {this.state.errors["TITLE_NAME_REMARK"]}
                    </div>
                  </div>
                  {/* <FormattedHTMLMessage id="title_nametitle_list" /> */}
                </div>
                <div className="form-group">
                  <label>
                    <span className="required">* </span><FormattedMessage id="title_name" />
                  </label>
                  <input
                    type="text"
                    maxLength="50"
                    className={`form-control ${this.state.errorsFocus['FNAME_TH']} input_form`}
                    name="FNAME_TH"
                    onChange={e => this.handleChange(e)}
                    id="exampleFormControlInput1"
                    placeholder={
                      messages[this.state.language].formprofile1.name_input
                    }
                    value={this.state.fields['FNAME_TH']}
                  />
                  <div className="errorMsg FNAME_TH">
                    {this.state.errors["FNAME_TH"]}
                  </div>
                </div>
                <div className="form-group">
                  <label>
                    <span className="required">* </span><FormattedMessage id="title_lastname" />
                  </label>
                  <input
                    type="text"
                    maxLength="50"
                    className={`form-control ${this.state.errorsFocus['LNAME_TH']} input_form`}
                    name="LNAME_TH"
                    onChange={e => this.handleChange(e)}
                    id="exampleFormControlInput1"
                    placeholder={
                      messages[this.state.language].formprofile1.lastname_input
                    }
                    value={this.state.fields['LNAME_TH']}
                  />
                  <div className="errorMsg LNAME_TH">
                    {this.state.errors["LNAME_TH"]}
                  </div>
                </div>
                <div className="form-group">
                  <label>
                    <span className="required">* </span><FormattedMessage id="title_sex" />
                  </label>
                  <select
                    className={`${genderClass} form-control ${this.state.errorsFocus['GENDER_ID']} input_form`}
                    name="GENDER_ID"
                    id="exampleFormControlSelect1"
                    onChange={e => this.handleChange(e)}
                    value={this.state.fields["GENDER_ID"]}
                  >
                    <option value="0">
                      {messages[this.state.language].formprofile1.SELECT_GENDER}
                    </option>
                    {this.renderContent("type_gender")}
                    {/* <option value="4">Other. อื่นๆ</option> */}
                  </select>
                  <div className="errorMsg GENDER_ID">
                    {this.state.errors["GENDER_ID"]}
                  </div>
                  {/* <FormattedHTMLMessage id="title_sexlist" /> */}
                </div>
                {/* <div className="form-group">
                  <label>
                    <span className="required">* </span><FormattedMessage id="title_dateofbirth" />
                  </label>
                  <div className={`form-control ${this.state.errorsFocus['BIRTH_DATE']} input_form`} onClick={this.renderDatepickerMobile}>
                    {this.state.fields['BIRTH_DATE'] == "" ? <FormattedMessage id="title_dateofbirth_select" /> : (this.state.fields['CARD_TYPE_ID'] == 2 || this.state.language == 'en' ? moment(this.state.fields['BIRTH_DATE']).format('DD/MM/YYYY') : moment(this.state.fields['BIRTH_DATE']).add(543,'y').format('DD/MM/YYYY')) }
                  </div>
                  <input
                    type="hidden"
                    name="BIRTH_DATE"
                    onChange={e => this.handleChange(e)}
                    className={`form-control ${this.state.errorsFocus['BIRTH_DATE']} input_form`}
                    id="date"
                    value={this.state.fields['BIRTH_DATE']}
                  />
                  <div className="errorMsg BIRTH_DATE">
                    {this.state.errors["BIRTH_DATE"]}
                  </div>
                </div> */}
                <div className="form-group">
                  <label>
                    <span className="required">* </span><FormattedMessage id="title_status" />
                  </label>
                  <select
                    className={`form-control ${this.state.errorsFocus['MARITAL_STATUS']} input_form`}
                    name="MARITAL_STATUS"
                    id="exampleFormControlSelect1"
                    onChange={e => this.handleChange(e)}
                    value={this.state.fields['MARITAL_STATUS']}
                  >
                    <option value="0">
                      {
                        messages[this.state.language].formprofile1
                          .SELECT_MARITALSTATUS
                      }
                    </option>
                    {this.renderContent("type_status")}
                  </select>
                  <div className="errorMsg MARITAL_STATUS">
                    {this.state.errors["MARITAL_STATUS"]}
                  </div>
                  {/* <FormattedHTMLMessage id="title_statuslist" /> */}
                </div>
                <div className="form-group">
                  <label>
                    <FormattedMessage id="title_email" />
                  </label>
                  <input
                    type="text"
                    className={`form-control ${this.state.errorsFocus['USER_EMAIL']} input_form`}
                    name="USER_EMAIL"
                    onChange={e => this.handleChange(e)}
                    id="exampleFormControlInput1"
                    placeholder={
                      messages[this.state.language].formprofile1.email_input
                    }
                    value={this.state.fields['USER_EMAIL']}
                  />
                  <div className="errorMsg USER_EMAIL">
                    {this.state.errors["USER_EMAIL"]}
                  </div>
                </div>
                <div className="form-group">
                  <label>
                    <span className="required">* </span><FormattedMessage id="title_phonenumber" />
                  </label>
                  <input
                    type="text"
                    maxLength="10"
                    className={`form-control ${this.state.errorsFocus['PHONE_NO']} input_form ${(this.state.fields["PHONE_NO"] ? `` : '')}`}
                    name="PHONE_NO"
                    onChange={e => this.handleChange(e)}
                    id="exampleFormControlInput1"
                    placeholder={
                      messages[this.state.language].formprofile1.phone_input
                    }
                    value={this.state.fields["PHONE_NO"]}
                  />
                  <div className="errorMsg PHONE_NO">
                    {this.state.errors["PHONE_NO"]}
                  </div>
                </div>
                <div className="btn_nextstep">
                  <button
                    type="submit"
                    className="btn btn-secondary btn-block btn_agreement"
                  >
                    <FormattedMessage id="button_nextstep" />
                  </button>
                </div>
              </div>
            </div>
          </div>

          <DatePicker
                    value={selectTime}
                    isOpen={this.state.isOpenDatepickerMobile}
                    confirmText={messages[this.state.language].formprofile1.confirmDate}
                    cancelText={messages[this.state.language].formprofile1.cancelDate}
                    onSelect={this.handleSelectDatepickerMobile}
                    dateConfig={date_Config}
                    showHeader={false}
                    max={new Date()}
                    min={minDate}
                    theme="ios"
                    onCancel={this.handleCancelDatepickerMobile} />
          <DatePicker
            value={selectTime}
            isOpen={this.state.isOpenDOPASelectDate}
            confirmText={messages[this.state.language].formprofile1.confirmDate}
            cancelText={messages[this.state.language].formprofile1.cancelDate}
            onSelect={this.onSelectDOPASelectDate}
            dateConfig={Dopa_date_Config}
            showHeader={false}
            theme="ios"
            max={new Date()}
            min={minDate}
            onCancel={this.onCloseDOPASelectDate} 
          />
        </form>
      </IntlProvider>
    );
  }
}

export default Formprofile_1;