import React, { Component } from "react";
import { regisMaxCard } from "../../_actions/regiser_ptmaxcard";
import "./inform_updateprofile.css";
import {
  IntlProvider,
  FormattedMessage,
  FormattedHTMLMessage
} from "react-intl";
import { addLocaleData } from "react-intl";
import locale_en from "react-intl/locale-data/en";
import locale_th from "react-intl/locale-data/th";
import intlMessageEN from "./../../_translations/en.json";
import intlMessageTH from "./../../_translations/th.json";
import SweetAlert from "react-bootstrap-sweetalert";
import Img from "react-fix-image-orientation";
import { API_LIST } from '../../_constants/matcher'
import EXIF from "exif-js";

var alert;

var disabledimage = true
var disabledimage2 = true

addLocaleData([...locale_en, ...locale_th]);

var messages = {
  th: intlMessageTH,
  en: intlMessageEN
};

class Inform_updateprofile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      language: "th",
      file: "",
      agreement: false,
      imagePreviewUrl: "",
      nexbtn: "button_finish",
      disableAgreement: true,
      disabledBtn: "",
      resultApi: "",
      errorFileSize: "",
      closeBtn: {
        imgPreview1: `${localStorage.getItem('imgPreview1') != null ? 'block' : 'none'}`,
        imgPreview2: `${localStorage.getItem('imgPreview2') != null ? 'block' : 'none'}`,
        imgPreview3: `${localStorage.getItem('imgPreview3') != null ? 'block' : 'none'}`,

    },
      classPreview: {
        imgPreview1: `img-fluid`,
        imgPreview2: `img-fluid`,
        imgPreview3: `img-fluid`
      },
      imgPreview: {
        imgPreview1: `${localStorage.getItem('imgPreview1') != null ? localStorage.getItem('imgPreview1') : process.env.PUBLIC_URL+'/images/Group19@3x.png'}`,
        imgPreview2: `${localStorage.getItem('imgPreview2') != null ? localStorage.getItem('imgPreview2') : process.env.PUBLIC_URL+'/images/Group19@3x.png'}`,
        imgPreview3: `${localStorage.getItem('imgPreview3') != null ? localStorage.getItem('imgPreview3') : process.env.PUBLIC_URL+'/images/Group19@3x.png'}`,
    },
      fields: {
        CUSTOMER_IMG_INFO: []
      },
      renderPopup: "",
      show: false,
      modal: null
    };
  }

  componentDidMount() {
    const params = this.props.match.params;
    var tokenData = this.props.location;
    var sstr = tokenData.search.search("&");
    var sub_memberid = tokenData.search.substr(sstr);
    var memberid = sub_memberid.substr(5);
    this.setState({ language: params.lang ? params.lang.toString().toLowerCase() : API_LIST.defaultLang });

    sessionStorage.setItem("_usid", memberid);
    sessionStorage.setItem("_phoneno", params.phoneid);
    sessionStorage.setItem("_email", params.emailid);
  }

  handleLoadAvatar(e) {
    let { closeBtn, imgPreview, classPreview, fields } = this.state;
    this.setState({ target_name : e.target.name })
    var file = e.target.files[0];
    var reader = new FileReader();
    reader.onload = (e) => {
      var img = document.createElement("img");
      img.onload = () => {
        var canvas = document.createElement('canvas');
        var ctx = canvas.getContext("2d");
        ctx.drawImage(img, 0, 0);

        var MAX_WIDTH = 700;
        var MAX_HEIGHT = 700;
        var width = img.width;
        var height = img.height;
  
        if (width > height) {
          if (width > MAX_WIDTH) {
            height *= MAX_WIDTH / width;
            width = MAX_WIDTH;
          }
        } else {
          if (height > MAX_HEIGHT) {
            width *= MAX_HEIGHT / height;
            height = MAX_HEIGHT;
          }
        }
        canvas.width = width;
        canvas.height = height;

        var ctx = canvas.getContext("2d");
        ctx.drawImage(img, 0, 0, width, height);
        var dataurl = canvas.toDataURL("image/jpeg");

        this.setState({previewSrc: dataurl});

        var get64 = dataurl.substr(23)
        localStorage.setItem(this.state.target_name,"data:image/jpeg;base64," + get64)
        imgPreview[this.state.target_name] = dataurl
        classPreview[this.state.target_name] = ''
        var obj = {
          CUSTOMER_IMG: get64
        }
        fields['CUSTOMER_IMG_INFO'].push(obj)
        this.setState({ imgPreview, classPreview , fields })
        console.debug("file stored",dataurl);

        closeBtn[this.state.target_name] = 'block'
        this.setState({ closeBtn  })
      }
      img.src = e.target.result;
    }
    reader.readAsDataURL(file);
    // let { closeBtn, imgPreview, classPreview, fields } = this.state;
    // this.setState({ target_name : e.target.name })
    // var system = this;
    // EXIF.getData(e.target.files[0], function() {
    //   var orientation = EXIF.getTag(this, "Orientation");
    //   var can = document.createElement("canvas");
    //   var ctx = can.getContext("2d");

    //   var thisImage = new Image();
    //   thisImage.onload = function() {
    //     var MAX_WIDTH = 700;
    //     var MAX_HEIGHT = 700;
    //     var width = thisImage.width;
    //     var height = thisImage.height;

    //     if (width > height) {
    //       if (width > MAX_WIDTH) {
    //         height *= MAX_WIDTH / width;
    //         width = MAX_WIDTH;
    //       }
    //     } else {
    //       if (height > MAX_HEIGHT) {
    //         width *= MAX_HEIGHT / height;
    //         height = MAX_HEIGHT;
    //       }
    //     }
    //     can.width = width;
    //     can.height = height;
    //     // can.width  = thisImage.width;
    //     // can.height = thisImage.height;
    //     ctx.save();
    //     var width = can.width;
    //     var styleWidth = can.style.width;
    //     var height = can.height;
    //     var styleHeight = can.style.height;
    //     if (orientation) {
    //       if (orientation > 4) {
    //         can.width = height;
    //         can.style.width = styleHeight;
    //         can.height = width;
    //         can.style.height = styleWidth;
    //       }
    //       switch (orientation) {
    //         case 2:
    //           ctx.translate(width, 0);
    //           // ctx.scale(-1, 1);
    //           break;
    //         case 3:
    //           ctx.translate(width, height);
    //           ctx.rotate(Math.PI);
    //           break;
    //         case 4:
    //           ctx.translate(0, height);
    //           // ctx.scale(1, -1);
    //           break;
    //         case 5:
    //           ctx.rotate(0.5 * Math.PI);
    //           // ctx.scale(1, -1);
    //           break;
    //         case 6:
    //           ctx.rotate(0.5 * Math.PI);
    //           ctx.translate(0, -height);
    //           break;
    //         case 7:
    //           ctx.rotate(0.5 * Math.PI);
    //           ctx.translate(width, -height);
    //           // ctx.scale(-1, 1);
    //           break;
    //         case 8:
    //           ctx.rotate(-0.5 * Math.PI);
    //           ctx.translate(-width, 0);
    //           break;
    //       }
    //     }

    //     ctx.drawImage(thisImage, 0, 0, width, height);
    //     ctx.restore();
    //     var dataURL = can.toDataURL();
    //     var dataurlLink = can.toDataURL("image/jpeg");
    //     console.log(dataurlLink)
    //     // var get64 = dataURL
    //     var get64 = dataurlLink.substr(23)
    //     // var get64 = dataurlLink
    //     localStorage.setItem(system.state.target_name,get64)
    //     // imgPreview[system.state.target_name] = dataURL
    //     imgPreview[system.state.target_name] = dataurlLink
    //     classPreview[system.state.target_name] = ''
    //     var obj = {
    //       CUSTOMER_IMG: get64
    //     }
    //     fields['CUSTOMER_IMG_INFO'].push(obj)
    //     system.setState({ imgPreview, classPreview })

    //     closeBtn[system.state.target_name] = 'block'
    //     system.setState({ closeBtn })
    //     // img.src = e.target.result;


    //     imgPreview[system.state.target_name] = dataURL
    //     // imgPreview[system.state.target_name] = dataurlLink;
    //     classPreview[system.state.target_name] = ''
    //     // at this point you can save the image away to your back-end using 'dataURL'
    //     system.setState({ imgPreview, classPreview });
    //   };

    //   thisImage.src = URL.createObjectURL(this);
    // });
  }

  checkImg() {
    console.log(this.state.imgBase);
  }

  handleUploadFile = (e) => {
    if(e === 1) { 
      this.inputElement_1.value = "";
      this.inputElement_1.click();
      disabledimage = false;
    }
    if(e === 2) { 
      this.inputElement_2.value = "";
      this.inputElement_2.click(); 
      disabledimage2 = false;
    }
    if(e === 3) { 
      this.inputElement_3.value = "";
      this.inputElement_3.click(); 
    }
  }

  deleteLocalStorage(target) {
    if(target == 'imgPreview1'){
      disabledimage = true;
      disabledimage2 = true;
    }else if(target == 'imgPreview2'){
      disabledimage2 = true;
    }
    let { closeBtn, imgPreview, classPreview } = this.state;
    localStorage.removeItem(target);
    closeBtn[target] = "none";
    classPreview[target] = "preview-image";
    imgPreview[target] = `${process.env.PUBLIC_URL}/images/Group19@3x.png`;
    this.setState({ closeBtn });
  }

  handleChange(e) {
    this.setState({ TITLE: e.target.value });
  }

  changeHandle(event) {
    this.setState({ DESCRIPTION: event.target.value });
  }

  modalCheckSubmit(res, img) {
    alert = (
      <SweetAlert
        custom
        showCloseButton
        closeOnClickOutside={false}
        focusConfirmBtn={false}
        title=""
        customIcon={img}
        showConfirm={false}
        showCancelButton
        onCancel={() => this.handleChoice(false)}
        onConfirm={() => this.handleChoice(true)}
        // onOutsideClick={() => {
        //   this.wasOutsideClick = true;
        //   this.setState({ showConfirm: false });
        // }}
      >
        <div className="iconClose" onClick={() => this.handleChoice(false)}><i className="fas fa-times sizeCloseBtn"></i></div>
        <div className="fontSizeCase">{res}</div>
      </SweetAlert>
    );
    this.setState({ show: true, modal: alert });
  }

  handleChoice(choice) {
    if (choice === false) {
      if(this.state.disableAgreement == true){
        this.setState({ show: false , modal: null })
        window.location.href=`PTG:||updateinformation||success`
      }else{
        this.setState({ show: false , modal: null })
      }
    }
  }

  submitInform(){
    let { buttonDisabled } = this.state;
    var phoneno = sessionStorage.getItem('_phoneno')
    var email = sessionStorage.getItem('_email')

    // var phoneno = "0880493334"
    // var email = "liverpool20093555@hotmail.com"

    if(this.state.TITLE == undefined || this.state.TITLE == '0' || this.state.DESCRIPTION == undefined || this.state.DESCRIPTION == ''){
      this.setState({ disableAgreement: false })
      var renderPopup = messages[this.state.language].inform_updateprofile.select_edit;
      var imgPopup = `${process.env.PUBLIC_URL}/images/cancel.png`;
      console.log('1');
      this.modalCheckSubmit(renderPopup,imgPopup) 
    }else{ 
      if(phoneno !== "-"){
        console.log('2');

        this.setState({ buttonDisabled : true })
        regisMaxCard.insertCaseInsident(this.state.TITLE,this.state.DESCRIPTION,phoneno).then(e => {
          let data = {
            errCode: 111,
            errMsg: "ข้อมูลในระบบไม่ถูกต้อง",
            errMsg_en: "ข้อมูลในระบบไม่ถูกต้อง",
            isSuccess: true
          }
          
          if(data.isSuccess === true){
            console.log('3');

            this.setState({ buttonDisabled : true })
            this.setState({ disableAgreement : true })
            this.setState({ messageAlert : messages[this.state.language].inform_updateprofile.alert_success })
            var imgPopup = `${process.env.PUBLIC_URL}/images/checked.png`
            this.modalCheckSubmit(this.state.messageAlert , imgPopup) 
          }else{
            console.log('4');

            this.setState({ disableAgreement: false })
            this.setState({ resultApi: e.data.errMsg })
            this.setState({ disabledBtn: '' })
            var imgPopup = `${process.env.PUBLIC_URL}/images/cancel.png`
            this.modalCheckSubmit(this.state.resultApi,imgPopup)
          }
        })
      }else{
        this.setState({ buttonDisabled : true })
        regisMaxCard.insertCaseInsident(this.state.TITLE,this.state.DESCRIPTION,phoneno).then(e => {
          if(e.data.isSuccess === true){
            console.log('5');

            this.setState({ buttonDisabled : true })
            this.setState({ disableAgreement : true })
            this.setState({ messageAlert : messages[this.state.language].inform_updateprofile.alert_success })
            var imgPopup = `${process.env.PUBLIC_URL}/images/checked.png`
            this.modalCheckSubmit(this.state.messageAlert , imgPopup) 
          }else{
            console.log('6');

            this.setState({ disableAgreement: false })
            this.setState({ resultApi: e.data.errMsg })
            this.setState({ disabledBtn: '' })
            var imgPopup = `${process.env.PUBLIC_URL}/images/cancel.png`
            this.modalCheckSubmit(this.state.resultApi,imgPopup)
          }
        })
      }
    }
  }

  render() {
    return (
      <IntlProvider
        locale={this.state.language}
        messages={messages[this.state.language].inform_updateprofile}
      >
        <div className="bg_fullall">
          <div className="container-fluid">
            <div className="content_page">
              <div className="form-group">
                <select
                  className="form-control"
                  id="exampleFormControlSelect1"
                  name="SELECT_CASE"
                  onChange={e => this.handleChange(e)}
                >
                  <option value={0}>
                    {
                      messages[this.state.language].inform_updateprofile
                        .title_list
                    }
                  </option>
                  <option
                    value={
                      messages[this.state.language].inform_updateprofile
                        .title_list1
                    }
                  >
                    {
                      messages[this.state.language].inform_updateprofile
                        .title_list1
                    }
                  </option>
                  <option
                    value={
                      messages[this.state.language].inform_updateprofile
                        .title_list2
                    }
                  >
                    {
                      messages[this.state.language].inform_updateprofile
                        .title_list2
                    }
                  </option>
                  <option
                    value={
                      messages[this.state.language].inform_updateprofile
                        .title_liจst3
                    }
                  >
                    {
                      messages[this.state.language].inform_updateprofile
                        .title_list3
                    }
                  </option>
                </select>
              </div>
              <div className="form-group">
                <textarea
                  className="form-control input_form"
                  id="exampleFormControlTextarea1"
                  rows="4"
                  name="DESCRIPTION"
                  placeholder={
                    messages[this.state.language].inform_updateprofile
                      .title_additional
                  }
                  value={this.state.fields["DESCRIPTION"]}
                  onChange={event => this.changeHandle(event)}
                />
              </div>
              {/* <div className="head_title_agree"><FormattedMessage id="image" /></div> */}
              <div className="errorImageSize">{this.state.errorFileSize}</div>
              <div className="">
                {/* <form onSubmit={this._handleSubmit}> */}
                  <div className="row row_image">
                    <div className="col-4 col-sm-3 review_image_before">
                      <div className="upload_preview">
                        <div
                          className="close-btn"
                          onClick={() => this.deleteLocalStorage("imgPreview1")}
                          style={{
                            display: this.state.closeBtn["imgPreview1"]
                          }}
                        >
                          <i
                            className="fa fa-times-circle"
                            aria-hidden="true"
                          />
                        </div>
                        <div onClick={() => this.handleUploadFile(1)} className="div-centers">
                          <input
                            type="file"
                            name="imgPreview1"
                            style={{ display: "none" }}
                            ref={input => (this.inputElement_1 = input)}
                            onChange={e => this.handleLoadAvatar(e)}
                          />
                          <Img
                            src={this.state.imgPreview["imgPreview1"]}
                            alt=""
                            className={this.state.classPreview["imgPreview1"]}
                          />
                        </div>
                      </div>
                    </div>
                    <div className="col-4 col-sm-3 review_image_before">
                      <div className="upload_preview">
                        <div
                          className="close-btn"
                          onClick={() => this.deleteLocalStorage("imgPreview2")}
                          style={{
                            display: this.state.closeBtn["imgPreview2"]
                          }}
                        >
                          <i
                            className="fa fa-times-circle"
                            aria-hidden="true"
                          />
                        </div>
                        <div onClick={() => this.handleUploadFile(2)} className="div-centers">
                          {/* <input type="file" onChange={this._handleImageChange} style={{ display: "none" }} /> */}
                          <input
                            type="file"
                            name="imgPreview2"
                            style={{ display: "none" }}
                            ref={input => (this.inputElement_2 = input)}
                            onChange={e => this.handleLoadAvatar(e)}
                            disabled={disabledimage}
                          />
                          <Img
                            src={this.state.imgPreview["imgPreview2"]}
                            alt=""
                            className={this.state.classPreview["imgPreview2"]}
                          />
                        </div>
                      </div>
                    </div>
                    <div className="col-4 col-sm-3 review_image_before">
                      <div className="upload_preview">
                        <div
                          className="close-btn"
                          onClick={() => this.deleteLocalStorage("imgPreview3")}
                          style={{
                            display: this.state.closeBtn["imgPreview3"]
                          }}
                        >
                          <i
                            className="fa fa-times-circle"
                            aria-hidden="true"
                          />
                        </div>
                        <div onClick={() => this.handleUploadFile(3)} className="div-centers">
                          <input
                            type="file"
                            name="imgPreview3"
                            style={{ display: "none" }}
                            ref={input => (this.inputElement_3 = input)}
                            onChange={e => this.handleLoadAvatar(e)}
                            disabled={disabledimage2}
                          />
                          <Img
                            src={this.state.imgPreview["imgPreview3"]}
                            alt=""
                            className={this.state.classPreview["imgPreview3"]}
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                {/* </form> */}
              </div>
              {/* <label className="check_conditions">
              <FormattedMessage id="check_remind" />
              <input type="checkbox" onChange={(e) => this.changeHandle(e)} name="aggreement" disabled={this.state.disableAgreement}/>
              <span className="checkmark_condition"></span>
             </label> */}
              <FormattedHTMLMessage id="must_remind" />
              <div className="color-incident">** กรณีมีปัญหาบัตรพีทีแมกซ์การ์ด โปรดกรอกเบอร์ติดต่อหรืออีเมล เพื่อการติดต่อแก้ปัญหาสมาชิก</div>
              <div className="btn_nextstep">
                <button
                  type="button"
                  className="btn btn-secondary btn-block btn_agreement"
                  onClick={() => this.submitInform()}
                  disabled={this.state.buttonDisabled}
                >
                  <FormattedMessage id="button_nextstep" />
                </button>
              </div>
              {this.state.modal}
            </div>
          </div>
        </div>
      </IntlProvider>
    );
  }
}

// const getBase64 = (name, file) => {
//   return new Promise((resolve,reject) => {
//     const reader = new FileReader();
//     reader.onload = () => resolve({ res: reader.result, name: name});
//     reader.onerror = error => reject(error);
//     reader.readAsDataURL(file);
//   });
//   }

export default Inform_updateprofile;
