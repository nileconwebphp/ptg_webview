import React from "react";
import "./formprofile_3.css";
import {
  IntlProvider,
  FormattedMessage,
  FormattedHTMLMessage
} from "react-intl";
import { addLocaleData } from "react-intl";
import locale_en from "react-intl/locale-data/en";
import locale_th from "react-intl/locale-data/th";
import intlMessageEN from "./../../_translations/en.json";
import intlMessageTH from "./../../_translations/th.json";
import { API_LIST } from "../../_constants/matcher"
import { regisMaxCard } from "../../_actions/regiser_ptmaxcard";
import SweetAlert from "react-bootstrap-sweetalert";
var crypto = require('crypto');

addLocaleData([...locale_en, ...locale_th]);

  var messages = {
    th: intlMessageTH,
    en: intlMessageEN
  };
// localStorage.clear();

let alert
var disabledimage = true
var disabledimage2 = true

class Formprofile_3 extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      language: "th",
      file: '',
      agreement: false,
      imagePreviewUrl: '',
      nexbtn: 'button_finish',
      disableAgreement: false,
      disabledBtn: '',
      resultApi: '',
      errorFileSize: '',
      errors: {},
      errorsFocus: {},
      closeBtn: {
        imgPreview1: `${localStorage.getItem('imgPreview1') != null ? 'block' : 'none'}`,
        imgPreview2: `${localStorage.getItem('imgPreview2') != null ? 'block' : 'none'}`,
        imgPreview3: `${localStorage.getItem('imgPreview3') != null ? 'block' : 'none'}`,

    },
      classPreview: {
        imgPreview1: `img-fluid`,
        imgPreview2: `img-fluid`,
        imgPreview3: `img-fluid`
      },
      imgPreview: {
        imgPreview1: `${localStorage.getItem('imgPreview1') != null ? localStorage.getItem('imgPreview1') : process.env.PUBLIC_URL+'/images/Group19@3x.png'}`,
        imgPreview2: `${localStorage.getItem('imgPreview2') != null ? localStorage.getItem('imgPreview2') : process.env.PUBLIC_URL+'/images/Group19@3x.png'}`,
        imgPreview3: `${localStorage.getItem('imgPreview3') != null ? localStorage.getItem('imgPreview3') : process.env.PUBLIC_URL+'/images/Group19@3x.png'}`,
    },
      fields: {
        CUSTOMER_IMG_INFO: [],
        chkPDPA: ((JSON.parse(sessionStorage.getItem('chkPDPA')) != null ? JSON.parse(sessionStorage.getItem('chkPDPA')) : false)),
        // pdpacon1: (JSON.parse(sessionStorage.getItem('pdpacon1')) != null ? JSON.parse(sessionStorage.getItem('pdpacon1')) : false),
        // pdpacon2: (JSON.parse(sessionStorage.getItem('pdpacon2')) != null ? JSON.parse(sessionStorage.getItem('pdpacon2')) : false),
        // pdpacon3: (JSON.parse(sessionStorage.getItem('pdpacon3')) != null ? JSON.parse(sessionStorage.getItem('pdpacon3')) : false),
        // pdpacon4: (JSON.parse(sessionStorage.getItem('pdpacon4')) != null ? JSON.parse(sessionStorage.getItem('pdpacon4')) : false),
      },
      show: false,
      modal: null,
      chkPDPA:'none',
      btnBlock:'none',
    };
  }

  componentDidMount() {
    sessionStorage.setItem('test' , true);
    let { chkPDPA , btnBlock } = this.state;  
    if(this.props.location.state.data) {
      let newObject = Object.assign(this.props.location.state.data , this.state.fields)
      this.setState({ fields: newObject })
    }
    const params = this.props.match.params;
    this.setState({ language: (params.lang ? params.lang.toString().toLowerCase() : API_LIST.defaultLang) });

    if(sessionStorage.getItem('chkPDPA') === 'true'){
      chkPDPA = 'block'; 
      btnBlock = 'block';
    }

    if(localStorage.getItem('imgPreview1') !== null){
      disabledimage = false;
      if(localStorage.getItem('imgPreview2') !== null){
        disabledimage2 = false;
      }
    }

    this.setState({ chkPDPA , btnBlock })
  }

  changeHandle(e) {
    let { nexbtn , chkPDPA , btnBlock , pdpacon1 , pdpacon2 , pdpacon3 , pdpacon4 , fields , agreement } = this.state;
    if(e.target.name === "pdpacondition"){
      if(e.target.checked === true){
        sessionStorage.setItem('chkPDPA',true);
        fields['chkPDPA'] = e.target.checked;
        chkPDPA = 'block'; 
        btnBlock = 'block';
      }else{
        sessionStorage.setItem('chkPDPA',false);
        fields[e.target.name] = e.target.checked;
        chkPDPA = 'none'; 
        btnBlock = 'none';
      }
    }else if(e.target.name === "pdpacon1" || e.target.name === "pdpacon2" || e.target.name === "pdpacon3" || e.target.name === "pdpacon4"){
      sessionStorage.setItem(e.target.name, e.target.value);
      fields[e.target.name] = e.target.value;
    }
    this.setState({ nexbtn , chkPDPA , btnBlock , pdpacon1 , pdpacon2 , pdpacon3 , pdpacon4 , fields})
    this.setState({ agreement })
  }

  modalCheckSubmit(res){
    alert = (
      <SweetAlert
        custom
        confirmBtnBsStyle="success"
        closeOnClickOutside={true}
        cancelBtnBsStyle="default"
        focusConfirmBtn={false}
        title=""
        customIcon="../images/cancel.png"
        showConfirm={false}
        showCancelButton
        onCancel={() => this.handleChoice(false)}
        onConfirm={() => this.handleChoice(true)}
        onOutsideClick={() => {
          this.wasOutsideClick = true;
          this.setState({ showConfirm: false })
        }}
      >
        {res}
      </SweetAlert>
    );

    this.setState({ show: true , modal: alert })
  }

  handleChoice(choice) {
    if (choice === false) {
      this.setState({ show: false , modal: null })
    }
  }

  submitForm() {
      this.props.history.push({
        pathname: `${process.env.PUBLIC_URL}/formprofile_4/${this.state.language}`,
        state: {
          data: this.state.fields
        }
      });
  }

  submitFinalForm() {
    if (this.validateForm()) {
      var pdpaJson = 
        {
          "CUSTOMER_ID": "",
          "UPDATE_BY": "API",
          "CONSENT_INFO": [
            {
              "CONSENT_MASTER_ID": 16,
              "CONSENT_NAME": "ส่งข้อมูลข่าวสาร การโฆษณาที่เกี่ยวกับสินค้า หรือบริการ",
              "CONSENT_VERSION": 4,
              "IS_CHECK": this.state.fields['pdpacon1']
            },
            {
              "CONSENT_MASTER_ID": 17,
              "CONSENT_NAME": "เสนอโปรโมชั่นสินค้าและบริการ โดยตรงถึงสมาชิก",
              "CONSENT_VERSION": 4,
              "IS_CHECK": this.state.fields['pdpacon2']
            },
            {
              "CONSENT_MASTER_ID": 18,
              "CONSENT_NAME": "เชิญชวนเข้าร่วมกิจกรรมต่างๆ ที่สมาชิกได้ระบุความสนใจหรือความชื่นชอบในขณะสมัครสมาชิก",
              "CONSENT_VERSION": 4,
              "IS_CHECK": this.state.fields['pdpacon3']
            },
                {
              "CONSENT_MASTER_ID": 19,
              "CONSENT_NAME": "วิเคราะห์การใช้สินค้าหรือบริการของสมาชิก เพื่อจัดทำโปรโมชั่น หรือส่วนลดในสินค้าหรือบริการ",
              "CONSENT_VERSION": 4,
              "IS_CHECK": this.state.fields['pdpacon4']
            }
          ]
        }
        var resString = JSON.stringify(pdpaJson);
        const ENC_KEY = "kHyT9QwZX5nkPCx5Gs1NNiliCon@2019"; // set random encryption key
        const IV = "xRtyUw5Pt+58ZxqA"; // set random initialisation vector
        let cipher = crypto.createCipheriv('aes-256-cbc', Buffer.from(ENC_KEY), IV);
        let encrypted = cipher.update(resString);
        encrypted = Buffer.concat([encrypted, cipher.final()]);
        var en = encrypted.toString('base64');
        regisMaxCard.activateAndRegister(this.state.fields).then(e => {
          console.log(this.state.fields)
          if(e.data.isSuccess === false) {
            this.setState({ disableAgreement: false })
            this.setState({ resultApi: e.data.errMsg })
            this.setState({ disabledBtn: '' })
            var imgPopup = `${process.env.PUBLIC_URL}/images/cancel.png`;
            this.modalCheckSubmit(this.state.resultApi,imgPopup)
          } else {
            this.setState({ resultApi: e.data.errMsg });
            // sessionStorage.setItem('chkPDPA','');
            window.location.href=`PTG:||Register||sucess||phonenumber=${this.props.location.state.data.PHONE_NO}?data=${en}`;
            // localStorage.setItem('inputBody','{}');
          }
        })
    }else{
      console.log('false');
    }
  }

  validateForm() {
    let fields = this.state.fields
    let errors = {};
    let errorsFocus = {};
    let formIsValid = true;
    if (!fields["pdpacon1"] || !fields["pdpacon2"] || !fields["pdpacon3"] || !fields["pdpacon4"]) {
      formIsValid = false;
      errorsFocus["ck_agree_conditions"] = 'errorFocus'
      errors["ck_agree_conditions"] = <FormattedMessage id="CK_AGREE_CONDITIONS" />;
    }
    this.setState({
      errors: errors,
      errorsFocus: errorsFocus
    });

    return formIsValid;
  }

  handleLoadAvatar(e) {
    let { closeBtn, imgPreview, classPreview, fields } = this.state;
        this.setState({ target_name : e.target.name })
        var file = e.target.files[0];
        var reader = new FileReader();
        reader.onload = (e) => {
          var img = document.createElement("img");
          img.onload = () => {
            var canvas = document.createElement('canvas');
            var ctx = canvas.getContext("2d");
            ctx.drawImage(img, 0, 0);
    
            var MAX_WIDTH = 700;
            var MAX_HEIGHT = 700;
            var width = img.width;
            var height = img.height;
      
            if (width > height) {
              if (width > MAX_WIDTH) {
                height *= MAX_WIDTH / width;
                width = MAX_WIDTH;
              }
            } else {
              if (height > MAX_HEIGHT) {
                width *= MAX_HEIGHT / height;
                height = MAX_HEIGHT;
              }
            }
            canvas.width = width;
            canvas.height = height;
    
            var ctx = canvas.getContext("2d");
            ctx.drawImage(img, 0, 0, width, height);
            var dataurl = canvas.toDataURL("image/jpeg");
    
            this.setState({previewSrc: dataurl});
    
            var get64 = dataurl.substr(23)
            localStorage.setItem(this.state.target_name,"data:image/jpeg;base64," + get64)
            imgPreview[this.state.target_name] = dataurl
            classPreview[this.state.target_name] = ''
            var obj = {
              CUSTOMER_IMG: get64
            }
            fields['CUSTOMER_IMG_INFO'].push(obj)
            this.setState({ imgPreview, classPreview , fields })
            console.debug("file stored",dataurl);
    
            closeBtn[this.state.target_name] = 'block'
            this.setState({ closeBtn  })
          }
          img.src = e.target.result;
        }
        reader.readAsDataURL(file);
  }

  checkImg() {
    console.log(this.state.imgBase);
  }

  handleUploadFile = (e) => {
    if(e === 1) { 
      this.inputElement_1.value = "";
      this.inputElement_1.click(); 
      disabledimage = false;
    }
    if(e === 2) { 
      this.inputElement_2.value = "";
      this.inputElement_2.click(); 
      disabledimage2 = false;
    }
    if(e === 3) { 
      this.inputElement_3.value = "";
      this.inputElement_3.click(); 
    }
  }

  cutLink(){
    window.location.href = `https://www.ptmaxcard.com/MaxCardPoint/`;
  }

  cutLink1(){
    window.location.href = `https://www.ptmaxcard.com/MaxCardPoint/`;
  }

  deleteLocalStorage(target) {
    if(target == 'imgPreview1'){
      disabledimage = true;
      disabledimage2 = true;
    }else if(target == 'imgPreview2'){
      disabledimage2 = true;
    }
    let { closeBtn, imgPreview, classPreview } = this.state;
    localStorage.removeItem(target)
    closeBtn[target] = 'none'
    classPreview[target] = 'preview-image'
    imgPreview[target] = `${process.env.PUBLIC_URL}/images/Group19@3x.png`
    this.setState({ closeBtn })
  }

  render() {
    console.log(this.state.fields);
    return (
      <IntlProvider
      locale={this.state.language}
      messages={messages[this.state.language].formprofile3}
      >

      <div className="bg_fullall">
      {/* <SweetAlert
          show={this.state.show}
          title=""
          text={this.state.resultApi}
          onOutsideClick={() => this.setState({ show: false })}
        /> */}
        <div className="container-fluid">
        <div className="content_page">
            <div className="head_title_agree"><FormattedMessage id="image" /></div>
            <div className="errorImageSize">{this.state.errorFileSize}</div>
            <div className="">
              <form onSubmit={this._handleSubmit}>
                {/* <button type="submit" onClick={this._handleSubmit}>Upload Image</button> */}
              <div className="row row_image">
                <div className="col-4 col-sm-3 review_image_before">
                  <div className="upload_preview" disabled={false}>
                    <div className="close-btn" 
                      onClick={() => this.deleteLocalStorage('imgPreview1')} 
                      style={{ display: this.state.closeBtn['imgPreview1'] }}>
                        <i className="fa fa-times-circle" aria-hidden="true"></i></div>
                    <div onClick={() => this.handleUploadFile(1)} className="div-centers">
                      {/* <input type="file" onChange={this._handleImageChange} style={{ display: "none" }} /> */}
                      <input type="file" name="imgPreview1" style={{ display: "none" }} ref={input => this.inputElement_1 = input} onChange={(e) => this.handleLoadAvatar(e)}/>
                      <img src={this.state.imgPreview['imgPreview1']} alt="" className={this.state.classPreview['imgPreview1']} />
                    </div>
                  </div>
                </div>
                <div className="col-4 col-sm-3 review_image_before">
                  <div className="upload_preview">
                    <div className="close-btn" onClick={() => this.deleteLocalStorage('imgPreview2')} style={{ display: this.state.closeBtn['imgPreview2'] }}><i className="fa fa-times-circle" aria-hidden="true"></i></div>
                    <div onClick={() => this.handleUploadFile(2)} className="div-centers">
                      {/* <input type="file" onChange={this._handleImageChange} style={{ display: "none" }} /> */}
                      <input type="file" name="imgPreview2" style={{ display: "none" }} ref={input => this.inputElement_2 = input} onChange={(e) => this.handleLoadAvatar(e)} disabled={disabledimage}/>
                      <img src={this.state.imgPreview['imgPreview2']} alt="" className={this.state.classPreview['imgPreview2']} />
                    </div>
                  </div>
                </div>
                <div className="col-4 col-sm-3 review_image_before">
                  <div className="upload_preview">
                    <div className="close-btn" onClick={() => this.deleteLocalStorage('imgPreview3')} style={{ display: this.state.closeBtn['imgPreview3'] }}><i className="fa fa-times-circle" aria-hidden="true"></i></div>
                    <div onClick={() => this.handleUploadFile(3)} className="div-centers">
                      {/* <input type="file" onChange={this._handleImageChange} style={{ display: "none" }} /> */}
                      <input type="file" name="imgPreview3" style={{ display: "none" }} ref={input => this.inputElement_3 = input} onChange={(e) => this.handleLoadAvatar(e)} disabled={disabledimage2}/>
                      <img src={this.state.imgPreview['imgPreview3']} alt="" className={this.state.classPreview['imgPreview3']} />
                    </div>
                  </div>
                </div>
              </div>
              </form>
            </div>
            <FormattedHTMLMessage id="must_remind" />
            <div className="d-flex">
              <div className="pl-2">
                <label className="check_conditions">
                  <input type="checkbox" onChange={(e) => this.changeHandle(e)} checked={JSON.parse(sessionStorage.getItem('chkPDPA'))} value={this.state.fields['chkPDPA']} onClick={(e) => this.changeHandle(e)} name="pdpacondition"/>
                  <span className="checkmark_condition"></span>
                </label>
              </div>
              <div>
                <FormattedHTMLMessage id="pdpacondition" />  
                <a style={{color:'#03ac4f'}} onClick={() => this.cutLink()}><FormattedHTMLMessage id="pdpacondition1" /></a>
                <FormattedHTMLMessage id="pdpacondition2" /> 
                <a style={{color:'#03ac4f'}} onClick={() => this.cutLink1()}><FormattedHTMLMessage id="pdpacondition3" /></a>
              </div>
            </div>
            <div className="check_agree" style={{display:this.state.chkPDPA}}>
              <FormattedHTMLMessage id="pdpacon" />
              <div className="row mt-3">
                <div className="col-2">
                  <FormattedMessage id="agree" />
                </div>
                <div className="col-3">
                  <FormattedMessage id="notagree" />
                </div>
                <div className="col-7 p-0">
                  <FormattedMessage id="activity" />
                </div>
              </div>
              <div className="row">
                <div className="col-2 check_conditions">
                  <label>
                    <input type="radio" 
                      name="pdpacon1" 
                      id="pdpacon1_1"
                      onChange={(e) => this.changeHandle(e)}
                      onClick={(e) => this.changeHandle(e)} 
                      defaultChecked={this.state.fields['pdpacon1'] == true ? true : false} 
                      value={true} 
                    />
                    <span className="radio_checkmark_condition"></span>
                  </label>
                </div>
                <div className="col-3 check_conditions">
                  <label>
                    <input type="radio" 
                      name="pdpacon1" 
                      id="pdpacon1_2"
                      onChange={(e) => this.changeHandle(e)}
                      onClick={(e) => this.changeHandle(e)} 
                      defaultChecked={this.state.fields['pdpacon1'] == false ? true : false} 
                      value={false} 
                    />
                    <span className="radio_checkmark_condition"></span>
                  </label>
                </div>
                <div className="col-7 check_conditions">
                  <FormattedHTMLMessage id="pdpacon1" />
                </div>
              </div>

              <div className="row">
                <div className="col-2 check_conditions">
                  <label>
                    <input type="radio" 
                      name="pdpacon2" 
                      id="pdpacon2_1"
                      onChange={(e) => this.changeHandle(e)}
                      onClick={(e) => this.changeHandle(e)} 
                      defaultChecked={this.state.fields['pdpacon2'] == true ? true : false} 
                      value={true} 
                    />
                    <span className="radio_checkmark_condition"></span>
                  </label>
                </div>
                <div className="col-3 check_conditions">
                  <label>
                    <input type="radio" 
                      name="pdpacon2" 
                      id="pdpacon2_2"
                      onChange={(e) => this.changeHandle(e)}
                      onClick={(e) => this.changeHandle(e)} 
                      defaultChecked={this.state.fields['pdpacon2'] == false ? true : false} 
                      value={false} 
                    />
                    <span className="radio_checkmark_condition"></span>
                  </label>
                </div>
                <div className="col-7 check_conditions">
                  <FormattedHTMLMessage id="pdpacon2" />
                </div>
              </div>

              <div className="row">
                <div className="col-2 check_conditions">
                  <label>
                    <input type="radio" 
                      name="pdpacon3" 
                      id="pdpacon3_1"
                      onChange={(e) => this.changeHandle(e)}
                      onClick={(e) => this.changeHandle(e)} 
                      defaultChecked={this.state.fields['pdpacon3'] == true ? true : false} 
                      value={true} 
                    />
                    <span className="radio_checkmark_condition"></span>
                  </label>
                </div>
                <div className="col-3 check_conditions">
                  <label>
                    <input type="radio" 
                      name="pdpacon3" 
                      id="pdpacon3_2"
                      onChange={(e) => this.changeHandle(e)}
                      onClick={(e) => this.changeHandle(e)} 
                      defaultChecked={this.state.fields['pdpacon3'] == false ? true : false} 
                      value={false} 
                    />
                    <span className="radio_checkmark_condition"></span>
                  </label>
                </div>
                <div className="col-7 check_conditions">
                  <FormattedHTMLMessage id="pdpacon3" />
                </div>
              </div>

              <div className="row">
                <div className="col-2 check_conditions">
                  <label>
                    <input type="radio" 
                      name="pdpacon4" 
                      id="pdpacon4_1"
                      onChange={(e) => this.changeHandle(e)}
                      onClick={(e) => this.changeHandle(e)} 
                      defaultChecked={this.state.fields['pdpacon4'] == true ? true : false} 
                      value={true}
                    />
                    <span className="radio_checkmark_condition"></span>
                  </label>
                </div>
                <div className="col-3 check_conditions">
                  <label>
                    <input type="radio" 
                      name="pdpacon4" 
                      id="pdpacon4_2"
                      onChange={(e) => this.changeHandle(e)}
                      onClick={(e) => this.changeHandle(e)} 
                      defaultChecked={this.state.fields['pdpacon4'] == false ? true : false} 
                      value={false} 
                    />
                    <span className="radio_checkmark_condition"></span>
                  </label>
                </div>
                <div className="col-7 check_conditions">
                  <FormattedHTMLMessage id="pdpacon4" />
                </div>
              </div>

              <div className="errorMsg">{this.state.errors["ck_agree_conditions"]}</div>
              
              {/* <label className="check_conditions check_conditions-margin">
                <FormattedHTMLMessage id="pdpacon2" />
                <input type="radio" onChange={(e) => this.changeHandle(e)} defaultChecked={this.state.fields['pdpacon2']} value={this.state.fields['pdpacon2']}  onClick={(e) => this.changeHandle(e)}name="pdpacon2"/>
                <span className="radio_checkmark_condition"></span>
              </label> */}
              {/* <label className="check_conditions check_conditions-margin">
                <FormattedHTMLMessage id="pdpacon4" />
                <input type="radio" onChange={(e) => this.changeHandle(e)} defaultChecked={this.state.fields['pdpacon3']} value={this.state.fields['pdpacon3']} onClick={(e) => this.changeHandle(e)} name="pdpacon3"/>
                <span className="radio_checkmark_condition"></span>
              </label> */}
              {/* <label className="check_conditions check_conditions-margin">
                <FormattedHTMLMessage id="pdpacon4" />
                <input type="radio" onChange={(e) => this.changeHandle(e)} defaultChecked={this.state.fields['pdpacon4']} value={this.state.fields['pdpacon4']} onClick={(e) => this.changeHandle(e)} name="pdpacon4"/>
                <span className="radio_checkmark_condition"></span>
              </label> */}
              {/* <label className="check_conditions">
                <FormattedMessage id="check_remind" />
                <input type="checkbox" onChange={(e) => this.changeHandle(e)} name="aggreement" disabled={this.state.disableAgreement}/>
                <span className="checkmark_condition"></span>
              </label> */}
             </div>
             <div className="btn_nextstep">
              <div className="" style={{display:this.state.btnBlock}}>
                  <button type="button" className={`btn btn-secondary btn-block btn_agreement ${this.state.disabledBtn}`} onClick={() => this.submitFinalForm()}>{(this.state.disabledBtn === 'disabled' ? <i className="fa fa-circle-o-notch fa-spin" style={{ marginRight: "10px",marginTop: "5px" }}/> : '')}<FormattedMessage id="button_finish" /></button>
              </div>
              <div className="" style={{display:this.state.btnBlock}}>
                  <button type="button" className={`btn btn-secondary btn-block btn_agreement ${this.state.disabledBtn}`} onClick={() => this.submitForm()}>{(this.state.disabledBtn === 'disabled' ? <i className="fa fa-circle-o-notch fa-spin" style={{ marginRight: "10px",marginTop: "5px" }}/> : '')}<FormattedMessage id="check_remind" /></button>
              </div>
            </div>
            {this.state.modal}
          </div>
        </div>
      </div>
      </IntlProvider>
    )
  }
}

const getBase64 = (name, file) => {
  return new Promise((resolve,reject) => {
    const reader = new FileReader();
    reader.onload = () => resolve({ res: reader.result, name: name});
    reader.onerror = error => reject(error);
    reader.readAsDataURL(file);
  });
  }

export default Formprofile_3;
