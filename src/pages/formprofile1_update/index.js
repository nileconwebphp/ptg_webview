import React from "react";
import { regisMaxCard } from "../../_actions/regiser_ptmaxcard";
import "react-datepicker/dist/react-datepicker.css";
import "./formprofile1_update.css";
import {
  IntlProvider,
  FormattedMessage
  // FormattedHTMLMessage
} from "react-intl";
import { addLocaleData } from "react-intl";
import locale_en from "react-intl/locale-data/en";
import locale_th from "react-intl/locale-data/th";
import intlMessageEN from "./../../_translations/en.json";
import intlMessageTH from "./../../_translations/th.json";
import { API_LIST } from "../../_constants/matcher";
import moment from "moment";
import SweetAlert from "react-bootstrap-sweetalert";
import DatePicker from 'react-mobile-datepicker';

var alert;

var maxDate = moment().format("YYYY-MM-DD");
var genderClass = "form-control disabled";


addLocaleData([...locale_en, ...locale_th]);

var messages = {
  th: intlMessageTH,
  en: intlMessageEN
};

var ERROR_USER_EMAIL = false
var ERROR_USER_PHONE = false

sessionStorage.setItem('inputBody','{}')

let SessionData = JSON.parse(sessionStorage.getItem('inputBody'))



class Updateprofile extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      startDate: new Date(),
      language: "th",
      isForeigner: (JSON.parse(sessionStorage.getItem('inputBody')).CARD_TYPE_ID != null && JSON.parse(sessionStorage.getItem('inputBody')).CARD_TYPE_ID === 2 ? "block" : "none" ),
      inputcard: (JSON.parse(sessionStorage.getItem('inputBody')).CARD_TYPE_ID != null && JSON.parse(sessionStorage.getItem('inputBody')).CARD_TYPE_ID === 2 ? false : true ),
      agreement: false,
      title_name: "",
      gender: "",
      errors: {},
      errorsFocus: {
        CARD_ID: "",
        CARD_TYPE_ID: "",
        TITLE_NAME_ID: "",
        GENDER_ID: "",
        PHONE_NO: "",
        MARITAL_STATUS: "",
        FNAME_TH:"",
        LNAME_TH:""
      },
      content_page: "none",
      content_loading: "block",
      marital_status: "",
      GENDER_ID: 1,
      fields: {

      },
      TITLE_NAME_ID: 1,
      TITLE_NAME_REMARK: "none",
      data : '',
      show: false,
      modal: null,
    };
    this.handleChange = this.handleChange.bind(this);
  }

  renderDatepickerMobile = () => {
    this.setState({ isOpenDatepickerMobile: true });
  }

  handleCancelDatepickerMobile = () => {
      this.setState({ isOpenDatepickerMobile: false });
  }

  handleSelectDatepickerMobile = () => {
    let { fields } = this.state;
    fields['BIRTH_DATE'] = moment(this.state.fields['BIRTH_DATE']).format('DD/MM/YYYY');
    this.setState({ fields, isOpenDatepickerMobile: false });
  }

  componentDidMount() {
    let { fields } = this.state;

    const params = this.props.match.params;
    var tokenData = this.props.location;
    var sstr = tokenData.search.search("&");
    var sub_memberid = tokenData.search.substr(sstr);
    var memberid = sub_memberid.substr(5);
    this.setState({
      language: params.lang
        ? params.lang.toString().toLowerCase()
        : API_LIST.defaultLang
    });
    this.getDropdownlist_type("TITLE").then(result => {
      this.setState({ title_name: result.dropdowN_INFO });
    });
    this.getDropdownlist_type("GENDER").then(result => {
      this.setState({ gender: result.dropdowN_INFO });
    });
    this.getDropdownlist_type("MARITAL_STATUS").then(result => {
      this.setState({ marital_status: result.dropdowN_INFO });
    });

    if(SessionData != null) {
      let newObject = Object.assign(SessionData , this.state.fields)
      fields = newObject
      this.setState({ fields })
    }

    sessionStorage.setItem('_usid' , memberid)
    sessionStorage.setItem('_tokenid' , params.tokenid)
    sessionStorage.setItem('_customerid' , params.customerid)
    sessionStorage.setItem('_udid' , params.udid)
    sessionStorage.setItem('_deviceos' , params.deviceos)
    
    this.updateProfile()
  }

  updateProfile(){
    let { fields , TITLE_NAME_REMARK , getToken } = this.state;
    var token = sessionStorage.getItem('_tokenid')
    var customer = sessionStorage.getItem('_customerid')
    var userid = sessionStorage.getItem('_usid')
    var content_page = ''
    var content_loading = ''

    regisMaxCard.updateProfileList(token,customer,userid).then(e => {


      var customerData = JSON.parse(e.data.data.data)
      var customerList = customerData.CUSTOMER_PROFILE_INFO[0]
      this.setState({
        emailApi : customerList.USER_EMAIL
      })
      let newObject = Object.assign(customerList , this.state.fields)
      fields = newObject
      if(fields['CARD_TYPE_ID'] === 2){
        fields['CARD_ID_ENG'] = fields['CARD_ID']
      }

      if(fields['USER_EMAIL'] !== '' || fields['USER_EMAIL'] !== null) {
        fields['alreadyEmail'] = true
      } else {
        fields['alreadyEmail'] = false
      }

      if(fields['TITLE_NAME_ID'] == 4 ) {
        TITLE_NAME_REMARK = "block";
      }
      fields['SUBSCRIBE_EMAIL'] = (JSON.parse(sessionStorage.getItem('inputBody')).SUBSCRIBE_EMAIL != null ? JSON.parse(sessionStorage.getItem('inputBody')).SUBSCRIBE_EMAIL : (fields["SUBSCRIBE_EMAIL"] === 1 ? true : false)),
      fields['SUBSCRIBE_LETTER'] = (JSON.parse(sessionStorage.getItem('inputBody')).SUBSCRIBE_LETTER != null ? JSON.parse(sessionStorage.getItem('inputBody')).SUBSCRIBE_LETTER : (fields["SUBSCRIBE_LETTER"] === 1 ? true : false)),
      fields['SUBSCRIBE_NONE'] = (JSON.parse(sessionStorage.getItem('inputBody')).SUBSCRIBE_NONE != null ? JSON.parse(sessionStorage.getItem('inputBody')).SUBSCRIBE_NONE : (fields["SUBSCRIBE_NONE"] === 1 ? true : false)),
      fields['SUBSCRIBE_SMS'] = (JSON.parse(sessionStorage.getItem('inputBody')).SUBSCRIBE_SMS != null ? JSON.parse(sessionStorage.getItem('inputBody')).SUBSCRIBE_SMS : (fields["SUBSCRIBE_SMS"] === 1 ? true : false)),
      fields['PMT_BEAUTY'] = (JSON.parse(sessionStorage.getItem('inputBody')).PMT_BEAUTY != null ? JSON.parse(sessionStorage.getItem('inputBody')).PMT_BEAUTY : (fields["PMT_BEAUTY"] === 1 ? true : false)),
      fields['PMT_FOOD'] = (JSON.parse(sessionStorage.getItem('inputBody')).PMT_FOOD != null ? JSON.parse(sessionStorage.getItem('inputBody')).PMT_FOOD : (fields["PMT_FOOD"] === 1 ? true : false)),
      fields['PMT_OTHERS'] = (JSON.parse(sessionStorage.getItem('inputBody')).PMT_OTHERS != null ? JSON.parse(sessionStorage.getItem('inputBody')).PMT_OTHERS : (fields["PMT_OTHERS"] === 1 ? true : false)),
      fields['PMT_OTHERS_REMARK'] = (JSON.parse(sessionStorage.getItem('inputBody')).PMT_OTHERS_REMARK != null ? JSON.parse(sessionStorage.getItem('inputBody')).PMT_OTHERS_REMARK : fields["PMT_OTHERS_REMARK"]),
      fields['PMT_SHOPPING'] = (JSON.parse(sessionStorage.getItem('inputBody')).PMT_SHOPPING != null ? JSON.parse(sessionStorage.getItem('inputBody')).PMT_SHOPPING : (fields["PMT_SHOPPING"] === 1 ? true : false)),
      fields['PMT_SPORT_AND_CAR'] = (JSON.parse(sessionStorage.getItem('inputBody')).PMT_SPORT_AND_CAR != null ? JSON.parse(sessionStorage.getItem('inputBody')).PMT_SPORT_AND_CAR : (fields["PMT_SPORT_AND_CAR"] === 1 ? true : false)),
      fields['PMT_TRAVEL'] = (JSON.parse(sessionStorage.getItem('inputBody')).PMT_TRAVEL != null ? JSON.parse(sessionStorage.getItem('inputBody')).PMT_TRAVEL : (fields["PMT_TRAVEL"] === 1 ? true : false)),
      fields['ACT_EN_MUSIC'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_EN_MUSIC != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_EN_MUSIC : (fields["ACT_EN_MUSIC"] === 1 ? true : false)),
      fields['ACT_EN_CONCERT'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_EN_CONCERT != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_EN_CONCERT : (fields["ACT_EN_CONCERT"] === 1 ? true : false)),
      fields['ACT_EN_COUNTRY_CONCERT'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_EN_COUNTRY_CONCERT != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_EN_COUNTRY_CONCERT : (fields["ACT_EN_COUNTRY_CONCERT"] === 1 ? true : false )),
      fields['ACT_EN_MOVIES'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_EN_MOVIES != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_EN_MOVIES : (fields["ACT_EN_MOVIES"] === 1 ? true : false)),
      fields['ACT_EN_THEATER'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_EN_THEATER != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_EN_THEATER : (fields["ACT_EN_THEATER"] === 1 ? true : false )),
      fields['ACT_EN_OTHERS'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_EN_OTHERS != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_EN_OTHERS : (fields["ACT_EN_OTHERS"] === 1 ? true : false)),
      fields['ACT_SPT_FITNESS'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_SPT_FITNESS != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_SPT_FITNESS : (fields["ACT_SPT_FITNESS"] === 1 ? true : false)),
      fields['ACT_SPT_RUN'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_SPT_RUN != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_SPT_RUN : (fields["ACT_SPT_RUN"] === 1 ? true : false )),
      fields['ACT_SPT_CYCLING'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_SPT_CYCLING != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_SPT_CYCLING : (fields["ACT_SPT_CYCLING"] === 1 ? true : false)),
      fields['ACT_SPT_FOOTBALL'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_SPT_FOOTBALL != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_SPT_FOOTBALL : (fields["ACT_SPT_FOOTBALL"] === 1 ? true : false)),
      fields['ACT_SPT_OTHERS'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_SPT_OTHERS != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_SPT_OTHERS : (fields["ACT_SPT_OTHERS"] === 1 ? true : false)),
      fields['ACT_OD_SHOPPING'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_OD_SHOPPING != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_OD_SHOPPING : (fields["ACT_OD_SHOPPING"] === 1 ? true : false )),
      fields['ACT_OD_BEAUTY'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_OD_BEAUTY != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_OD_BEAUTY : (fields["ACT_OD_BEAUTY"] === 1 ? true : false)),
      fields['ACT_OD_RESTAURANT'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_OD_RESTAURANT != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_OD_RESTAURANT : (fields["ACT_OD_RESTAURANT"] === 1 ? true : false)),
      fields['ACT_OD_CLUB'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_OD_CLUB != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_OD_CLUB : (fields["ACT_OD_CLUB"] === 1 ? true : false)),
      fields['ACT_OD_MOMCHILD'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_OD_MOMCHILD != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_OD_MOMCHILD : (fields["ACT_OD_MOMCHILD"] === 1 ? true : false)),
      fields['ACT_OD_RALLY'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_OD_RALLY != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_OD_RALLY : (fields["ACT_OD_RALLY"] === 1 ? true : false )),
      fields['ACT_OD_ADVANTURE'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_OD_ADVANTURE != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_OD_ADVANTURE : (fields["ACT_OD_ADVANTURE"] === 1 ? true : false )),
      fields['ACT_OD_PHOTO'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_OD_PHOTO != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_OD_PHOTO : (fields["ACT_OD_PHOTO"] === 1 ? true : false )),
      fields['ACT_OD_PHILANTHROPY'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_OD_PHILANTHROPY != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_OD_PHILANTHROPY : (fields["ACT_OD_PHILANTHROPY"] === 1 ? true : false )),
      fields['ACT_OD_HOROSCOPE'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_OD_HOROSCOPE != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_OD_HOROSCOPE : (fields["ACT_OD_HOROSCOPE"] === 1 ? true : false )),
      fields['ACT_OD_ABROAD'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_OD_ABROAD != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_OD_ABROAD : (fields["ACT_OD_ABROAD"] === 1 ? true : false )),
      fields['ACT_OD_UPCOUNTRY'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_OD_UPCOUNTRY != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_OD_UPCOUNTRY : (fields["ACT_OD_UPCOUNTRY"] === 1 ? true : false )),
      fields['ACT_OD_OTHERS'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_OD_OTHERS != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_OD_OTHERS : (fields["ACT_OD_OTHERS"] === 1 ? true : false )),
      fields['ACT_ID_COOKING'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_ID_COOKING != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_ID_COOKING : (fields["ACT_ID_COOKING"] === 1 ? true : false )),
      fields['ACT_ID_BAKING'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_ID_BAKING != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_ID_BAKING : (fields["ACT_ID_BAKING"] === 1 ? true : false )),
      fields['ACT_ID_DECORATE'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_ID_DECORATE != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_ID_DECORATE : (fields["ACT_ID_DECORATE"] === 1 ? true : false )),
      fields['ACT_ID_GARDENING'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_ID_GARDENING != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_ID_GARDENING : (fields["ACT_ID_GARDENING"] === 1 ? true : false )),
      fields['ACT_ID_READING'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_ID_READING != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_ID_READING : (fields["ACT_ID_READING"] === 1 ? true : false )),
      fields['ACT_ID_GAMING'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_ID_GAMING != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_ID_GAMING : (fields["ACT_ID_GAMING"] === 1 ? true : false )),
      fields['ACT_ID_INTERNET'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_ID_INTERNET != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_ID_INTERNET : (fields["ACT_ID_INTERNET"] === 1 ? true : false)),
      fields['ACT_ID_OTHERS'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_ID_OTHERS != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_ID_OTHERS : (fields["ACT_ID_OTHERS"] === 1 ? true : false)),
      fields['ACT_HNGOUT_FRIEND'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_HNGOUT_FRIEND != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_HNGOUT_FRIEND : (fields["ACT_HNGOUT_FRIEND"] === 1 ? true : false )),
      fields['ACT_HNGOUT_COUPLE'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_HNGOUT_COUPLE != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_HNGOUT_COUPLE : (fields["ACT_HNGOUT_COUPLE"] === 1 ? true : false )),
      fields['ACT_HNGOUT_FAMILY'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_HNGOUT_FAMILY != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_HNGOUT_FAMILY : (fields["ACT_HNGOUT_FAMILY"] === 1 ? true : false )),
      fields['ACT_HNGOUT_NO'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_HNGOUT_NO != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_HNGOUT_NO : (fields["ACT_HNGOUT_NO"] === 1 ? true : false )),
      fields['ACT_EN_OTHERS_REMARK'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_EN_OTHERS_REMARK != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_EN_OTHERS_REMARK : fields["ACT_EN_OTHERS_REMARK"] ),
      fields['ACT_SPT_OTHERS_REMARK'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_SPT_OTHERS_REMARK != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_SPT_OTHERS_REMARK : fields["ACT_SPT_OTHERS_REMARK"] ),
      fields['ACT_OD_OTHERS_REMARK'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_OD_OTHERS_REMARK != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_OD_OTHERS_REMARK : fields["ACT_OD_OTHERS_REMARK"] ),
      fields['ACT_ID_OTHERS_REMARK'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_ID_OTHERS_REMARK != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_ID_OTHERS_REMARK : fields["ACT_ID_OTHERS_REMARK"])

      this.setState({ fields , TITLE_NAME_REMARK , getToken : e.data.data.tokenId })

    if (e.data.errMsg === "Success" ) {
        content_loading = "none";
        content_page = "block";
     } else {
        content_loading = "block";
        content_page = "none";
     }
     
     var th = this
     setTimeout(function(){
      th.setState({content_loading: content_loading})
      th.setState({content_page: content_page})
     }, 1000);
    })
  }

  changeHandle(e) {
    var { errorsFocus,errors, fields } = this.state;
    var checkedForeign = (e.target.checked === true) ? 2 : 1
    fields["CARD_TYPE_ID"] = checkedForeign
    if(checkedForeign === 1) {
      errorsFocus['CARD_ID_ENG'] = ''
      errors['CARD_ID_ENG'] = ''
      fields['CARD_ID_ENG'] = '';
    } else {
      errorsFocus['CARD_ID'] = ''
      errors['CARD_ID'] = ''
      fields['CARD_ID'] = '';
    }
      
    this.setState({ errorsFocus, errors, fields });
    this.setState({
      isForeigner: e.target.checked === true ? "block" : "none"
    });
    this.setState({
      inputcard: e.target.checked === true ? false : true
    })
  }

  modalCheckSubmit(res,img){
    alert = (
      <SweetAlert
        custom
        confirmBtnBsStyle="success"
        closeOnClickOutside={true}
        cancelBtnBsStyle="default"
        focusConfirmBtn={false}
        title=""
        customIcon={img}
        showConfirm={false}
        showCancelButton
        onCancel={() => this.handleChoice(false)}
        onConfirm={() => this.handleChoice(true)}
        onOutsideClick={() => {
          this.wasOutsideClick = true;
          this.setState({ showConfirm: false })
        }}
      >
        {res}
      </SweetAlert>
    );
    this.setState({ show: true , modal: alert })
  }

  handleChoice(choice) {
    if (choice === false) {
      this.setState({ show: false , modal: null })
    }
  }

  submitFinalForm(){
    var { fields } = this.state;
    if (this.validateForm()) {
      if(fields['CARD_TYPE_ID'] === 2) {
        fields['CARD_ID'] = fields['CARD_ID_ENG']
        this.setState({ fields })
      }
      regisMaxCard.updateProfile(this.state.fields,this.state.getToken).then(e => {
        if(e.data.isSuccess === false) {
          this.setState({ disableAgreement: false })
            this.setState({ resultApi: e.data.errMsg })
            this.setState({ disabledBtn: '' })
            var imgPopup = `${process.env.PUBLIC_URL}/images/cancel.png`;
            this.modalCheckSubmit(this.state.resultApi,imgPopup)
        } else {
          window.location.href=`PTG:||updateprofile||success||phonenumber=${this.state.fields.PHONE_NO}`
        }
      })
    } else {
      console.log('formsubmit ' + false);
    }
  }

  getDropdownlist_type(title) {
    return regisMaxCard.getDropdownlist(title);
  }

  renderContent(type) {
    const { title_name, gender, marital_status } = this.state;
    var option = [];
    var name_title;
    if (type === "type_title_name") {
      name_title = title_name;
    } else if (type === "type_gender") {
      name_title = gender;
    } else {
      name_title = marital_status;
    }
    for (var i in name_title) {
      option.push(
        <option key={i} value={name_title[i].code}>
          {name_title[i].values}
        </option>
      );
    }
    return option;
  }

  async handleSubmit(event) {
    var { fields } = this.state;
    event.preventDefault();
    if (await this.validateForm()) {
      if(fields['CARD_TYPE_ID'] === 2) {
        fields['CARD_ID'] = fields['CARD_ID_ENG']
        this.setState({ fields })
      }
      sessionStorage.setItem('inputBody', JSON.stringify(this.state.fields))
      this.submitFinalForm();
    } else {
      console.log('formsubmit ' + false);
    }
  }

  async validateForm() {
    let fields = this.state.fields;
    let errors = {};
    let errorsFocus = {};
    let formIsValid = true;
    
    // if(fields['CARD_TYPE_ID'] === 1) {
    //   if (!fields["CARD_ID"]) {
    //       formIsValid = false;
    //       // errorsFocus["CARD_ID"] = 'errorFocus'
    //       // errors["CARD_ID"] = <FormattedMessage id="ALERT_CARD_ID" />;
    //   } else {
    //       var chkValidCard = await this.checkCard(this.state.fields["CARD_TYPE_ID"],this.state.fields["CARD_ID"]);
    //       if (chkValidCard.status === true) {
    //         formIsValid = false;
    //         errorsFocus["CARD_ID"] = 'errorFocus'
    //         errors["CARD_ID"] = (chkValidCard.msg === "" ? <FormattedMessage id="ALERT_CARD_NOT_USE" /> : chkValidCard.msg)
    //       }
    //   }
    // } else {
    //   if (!fields["CARD_ID_ENG"]) {
    //       formIsValid = false;
    //       // errorsFocus["CARD_ID_ENG"] = 'errorFocus'
    //       // errors["CARD_ID_ENG"] = <FormattedMessage id="ALERT_CARD_ID" />;
    //   }
    // }

    if (!fields["TITLE_NAME_ID"]) {
      formIsValid = false;
      errorsFocus["TITLE_NAME_ID"] = 'errorFocus'
      errors["TITLE_NAME_ID"] = <FormattedMessage id="ALERT_TITLE_NAME_ID" />;
    }
    if(fields["TITLE_NAME_ID"] === 4) {
      if (!fields["TITLE_NAME_REMARK"]) {
        formIsValid = false;
        errorsFocus["TITLE_NAME_REMARK"] = 'errorFocus'
        errors["TITLE_NAME_REMARK"] = (
          <FormattedMessage id="ALERT_TITLE_NAME_REMARK" />
        );
      }
    }
    if (!fields["FNAME_TH"]) {
      formIsValid = false;
      errorsFocus["FNAME_TH"] = 'errorFocus'
      errors["FNAME_TH"] = <FormattedMessage id="ALERT_FNAME_TH" />;
    }
    if (!fields["LNAME_TH"]) {
      formIsValid = false;
      errorsFocus["LNAME_TH"] = 'errorFocus'
      errors["LNAME_TH"] = <FormattedMessage id="ALERT_LNAME_TH" />;
    }
    if (!fields["GENDER_ID"]) {
      formIsValid = false;
      errorsFocus["GENDER_ID"] = 'errorFocus'
      errors["GENDER_ID"] = <FormattedMessage id="ALERT_GENDER_ID" />;
    } else {
      if (fields["GENDER_ID"] === 4) {
        if (fields["GENDER_ID"]) {
          formIsValid = false;
          errorsFocus["GENDER_ID"] = 'errorFocus'
          errors["GENDER_ID"] = (
            <FormattedMessage id="ALERT_GENDER_ID" />
          );
        }
      }
    }
    if (!fields["BIRTH_DATE"]) {
      formIsValid = false;
      errorsFocus["BIRTH_DATE"] = 'errorFocus'
      errors["BIRTH_DATE"] = <FormattedMessage id="ALERT_BIRTH_DATE" />;
    } else {
      var dateBeforDiff = moment(fields["BIRTH_DATE"]).format("YYYY-MM-DD");
      var diff = moment().diff(moment(dateBeforDiff, "YYYY-MM-DD"), "years");
      if (diff < 7) {
        formIsValid = false;
        errorsFocus["BIRTH_DATE"] = 'errorFocus'
        errors["BIRTH_DATE"] = <FormattedMessage id="ALERT_BIRTH_DATE_YEAR" />;
      }
    }

    if(fields['alreadyEmail'] === false) {
      if (!fields["USER_EMAIL"]) {
        formIsValid = false;
        errorsFocus["USER_EMAIL"] = 'errorFocus'
        errors["USER_EMAIL"] = <FormattedMessage id="ALERT_USER_EMAIL" />;
      } else {
        var validEmail = this.validateEmail(fields["USER_EMAIL"]);
        if (validEmail === false) {
          formIsValid = false;
          errorsFocus["USER_EMAIL"] = 'errorFocus'
          errors["USER_EMAIL"] = <FormattedMessage id="ALERT_EMAIL_FORMAT" />;
        } else {
          var chkValidEmail = await this.checkEmail();
          if (chkValidEmail === true) {
            formIsValid = false;
            errorsFocus["USER_EMAIL"] = 'errorFocus'
            errors["USER_EMAIL"] = <FormattedMessage id="ALERT_EMAIL_CANT_USE" />;
          }
        }
      }
    }else{
      if(this.state.emailApi !== fields['USER_EMAIL']){
        var validEmail = this.validateEmail(fields["USER_EMAIL"]);
        if (validEmail === false) {
          formIsValid = false;
          errorsFocus["USER_EMAIL"] = 'errorFocus'
          errors["USER_EMAIL"] = <FormattedMessage id="ALERT_EMAIL_FORMAT" />;
        } else {
          var chkValidEmail = await this.checkEmail();
          if (chkValidEmail === true) {
            formIsValid = false;
            errorsFocus["USER_EMAIL"] = 'errorFocus'
            errors["USER_EMAIL"] = <FormattedMessage id="ALERT_EMAIL_CANT_USE" />;
          }
        }
      }
    }

    if(!fields["MARITAL_STATUS"] || fields["MARITAL_STATUS"] === '0'){
      formIsValid = false;
      errorsFocus["MARITAL_STATUS"] = 'errorFocus'
      errors["MARITAL_STATUS"] = <FormattedMessage id="MARITAL_STATUS" />
    }
    // if (!fields["PHONE_NO"]) {
    //   formIsValid = false;
    //   errorsFocus["PHONE_NO"] = 'errorFocus'
    //   errors["PHONE_NO"] = <FormattedMessage id="ALERT_PHONE_NO" />;
    // } else {
    //   if(fields["PHONE_NO"].toString().length ) {}
    //   var validPhone = this.validatePhone(fields["PHONE_NO"])
    //   if(validPhone === false) {
    //     formIsValid = false;
    //     errorsFocus["PHONE_NO"] = 'errorFocus'
    //     errors["PHONE_NO"] = <FormattedMessage id="ALERT_PHONE_FORMAT" />
    //   } else {
    //     var chkValidPhone = await this.checkPhoneExist();
    //     if (chkValidPhone.status === true) {
    //       formIsValid = false;
    //       errorsFocus["PHONE_NO"] = 'errorFocus'
    //       errors["PHONE_NO"] = (chkValidPhone.msg === "" ? <FormattedMessage id="ALERT_PHONE_CANT_USE" /> : chkValidPhone.msg)
    //     }
    //     // var chkValidPhone = await this.checkPhoneExist();
    //     // if (chkValidPhone === true) {
    //     //   formIsValid = false;
    //     //   errorsFocus["PHONE_NO"] = 'errorFocus'
    //     //   errors["PHONE_NO"] = <FormattedMessage id="ALERT_PHONE_CANT_USE" />;
    //     // }
    //   }
    // }
    this.setState({
      errors: errors,
      errorsFocus: errorsFocus
    });

    return formIsValid;
  }

  handleChange(e) {
    let { errors,errorsFocus, fields, GENDER_ID, TITLE_NAME_REMARK } = this.state;
    if (e.target.name === "TITLE_NAME_ID") {
      if (
        e.target.value === "1" ||
        e.target.value === "2" ||
        e.target.value === "3"
      ) {
        GENDER_ID = e.target.value === "1" ? 1 : 2;
        fields["GENDER_ID"] = GENDER_ID;
        genderClass = "form-control disabled";
        TITLE_NAME_REMARK = "none";
        fields["TITLE_NAME_REMARK"] = '';
        errors["GENDER_ID"]  = null;
        errorsFocus["GENDER_ID"]  = ''
      } else {
        fields["GENDER_ID"] = 4;
        genderClass = "form-control";
        TITLE_NAME_REMARK = "block";
      }
      errors[e.target.name] = null;
      errorsFocus[e.target.name] = ''
      // errors["GENDER_ID"] = null;
      // errorsFocus["GENDER_ID"] = '';
      fields[e.target.name] = parseInt(e.target.value);
    } else if (e.target.name === "CARD_ID") {
      const re = /^[0-9\b]+$/;
      if (e.target.value === "" || re.test(e.target.value)) {
        fields[e.target.name] = e.target.value;
        errors[e.target.name] = null;
        errorsFocus[e.target.name] = ''
      } else {
        e.preventDefault();
        return false;
      }
    } else if (e.target.name === "PHONE_NO") {
      const re = /^[0-9\b]+$/;
      if (e.target.value === "" || re.test(e.target.value)) {
        fields[e.target.name] = e.target.value;
        errors[e.target.name] = null;
        errorsFocus[e.target.name] = ''
      } else {
        errors[e.target.name] = <FormattedMessage id="PREVENT_PHONE_NO" />;
        errorsFocus[e.target.name] = ''
        e.preventDefault();
      }
    } else {  
      fields[e.target.name] = e.target.value;
      errors[e.target.name] = null;
      errorsFocus[e.target.name] = ''
    }
    this.setState({ errors, fields, GENDER_ID, TITLE_NAME_REMARK });
  }

  validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }

  async checkEmail() {
    return regisMaxCard.checkEmailExist(this.state.fields["USER_EMAIL"]).then(e => {
      var resp = e.data.responsE_INFO;
      if (resp.rescode !== "000") {
        ERROR_USER_EMAIL = false
      } else {
        ERROR_USER_EMAIL = true
      }
      return ERROR_USER_EMAIL
    });
  }

  async checkPhoneExist() {
    return regisMaxCard.checkPhoneExist(this.state.fields["PHONE_NO"]).then(e => {
      // var resp = e.data.responsE_INFO;
      // if (resp.rescode != "000") {
      //   ERROR_USER_PHONE = false
      // } else {
      //   ERROR_USER_PHONE = true
      // }
      // return ERROR_USER_PHONE
      var resp = e.data.responsE_INFO;
      var response = {}
      if (resp.rescode !== "041")   {
        if(resp.rescode === "042") {
          response = {
            status: true,
            msg: <FormattedMessage id="ALERT_PHONE_NOTFORMAT" />
          }
        } else {
          response = {
            status: true,
            msg: ""
          }
        }
      } else {
        response = {
          status: false,
          msg: ""
        }
      }
      return response
    });
  }

  validatePhone(phonenumber) {
    // var re = /^[0]{1}[689]{1}[0-9]{8,9}/;
    var re = /^[0]{1}[689]/;
    return re.test(String(phonenumber));
  }

  async checkCard(type, card) {
    return regisMaxCard.checkexistingIdCard(type, card).then(e => {
      var resp = e.data.responsE_INFO;
      var response = {}
      if (resp.rescode !== "031")   {
        if(resp.rescode === "032") {
          response = {
            status: true,
            msg: <FormattedMessage id="ALERT_CARD_FALSE" />
          }
        } else {
          response = {
            status: true,
            msg: ""
          }
        }
      } else {
        response = {
          status: false,
          msg: ""
        }
      }
      return response
    });
  }


  render() {
    return (
      <IntlProvider
        locale={this.state.language}
        messages={messages[this.state.language].formprofile1}
      >
        <form className="form-horizontal formprofile_1" onSubmit={e => this.handleSubmit(e)}>
          <div className="bg_full">
          <SweetAlert
            show={this.state.show}
            title=""
            text={this.state.resultApi}
            onOutsideClick={() => this.setState({ show: false })}
          />
           <div id="loading" style={{ display: this.state.content_loading}}>
                  <div className="content_class">
                      <img src={`${process.env.PUBLIC_URL}/images/Loading.gif`} />
                  </div>
            </div>
            <div className="container-fluid">
              <div className="content_page" style={{ display: this.state.content_page}}>
                <div className="head_title_agree">
                  <FormattedMessage id="header_profile" />
                </div>
                <div className="form-group">
                  <label>
                    <span className="required">* </span><FormattedMessage id="title_idnumber" />
                  </label>
                  <div className="form-control input_form disabled" disabled={true}>
                    {this.state.fields['CARD_ID']}
                  </div>
                  <input disabled={!this.state.inputcard} 
                    type="hidden"
                    className="form-control input_form"
                    // className={`form-control input_form ${this.state.errorsFocus['CARD_ID']}`}
                    maxLength="13"
                    name="CARD_ID"
                    onChange={e => this.handleChange(e)}
                    id="exampleFormControlInput1"
                    placeholder={
                      messages[this.state.language].formprofile1.idcard_input
                    }
                    value={this.state.fields["CARD_ID"]}
                    disabled={true}
                  />
                  <div className="errorMsg CARD_ID">{this.state.errors["CARD_ID"]}</div>
                </div>
                {/* <label className="check_conditions_form">
                  <FormattedMessage id="title_foreigners" />
                  <input
                    type="checkbox"
                    name="aggreement"
                    checked={(this.state.fields["CARD_TYPE_ID"] === 1 ? false : true)}
                    onChange={e => this.changeHandle(e)}
                  />
                  <span className="checkmark_condition" />
                </label> */}
                <div
                  className="form-group"
                  style={{ display: this.state.isForeigner }}
                >
                  <div className="form-control input_form disabled" disabled={true}>
                    {this.state.fields['CARD_ID_ENG']}
                  </div>
                  <input
                    type="hidden"
                    className={`form-control input_form ${this.state.errorsFocus['CARD_ID_ENG']}`}
                    name="CARD_ID_ENG"
                    maxLength="10"
                    onChange={(e) => this.handleChange(e)}
                    id="exampleFormControlInput1"
                    placeholder={
                      messages[this.state.language].formprofile1
                        .idcard_inputforeigners
                    }
                    disabled={true}
                    value={this.state.fields['CARD_ID_ENG']}
                  />
                  <div className="errorMsg CARD_ID_ENG">{this.state.errors["CARD_ID_ENG"]}</div>
                </div>
                <div className="head_title_agree">
                  <FormattedMessage id="title_user" />
                </div>
                <div className="form-group">
                  <label>
                    <span className="required">* </span><FormattedMessage id="title_nametitle" />
                  </label>
                  <select
                    disabled={true}
                    className={`form-control ${this.state.errorsFocus['TITLE_NAME_ID']}`}
                    name="TITLE_NAME_ID"
                    id="exampleFormControlSelect1"
                    onChange={e => this.handleChange(e)}
                    value={this.state.fields.TITLE_NAME_ID == 0 ? this.state.fields.TITLE_NAME_ID = '' : this.state.fields.TITLE_NAME_ID}
                  >
                    <option value="0">
                      {messages[this.state.language].formprofile1.SELECT_TITLE}
                    </option>
                    {this.renderContent("type_title_name")}
                  </select>
                  <div className="errorMsg TITLE_NAME_ID">
                    {this.state.errors["TITLE_NAME_ID"]}
                  </div>
                  <div
                    className="form-group"
                    style={{ display: this.state.TITLE_NAME_REMARK }}
                  >
                    <input
                      type="text"
                      className={`form-control ${this.state.errorsFocus['TITLE_NAME_REMARK']} input_form`}
                      name="TITLE_NAME_REMARK"
                      onChange={e => this.handleChange(e)}
                      id="exampleFormControlInput1"
                      placeholder={
                        messages[this.state.language].formprofile1
                          .title_name_remark
                      }
                      value={this.state.fields['TITLE_NAME_REMARK']}
                    />
                    <div className="errorMsg TITLE_NAME_REMARK">
                      {this.state.errors["TITLE_NAME_REMARK"]}
                    </div>
                  </div>
                  {/* <FormattedHTMLMessage id="title_nametitle_list" /> */}
                </div>
                <div className="form-group">
                  <label>
                    <span className="required">* </span><FormattedMessage id="title_name" />
                  </label>
                  <div className="form-control input_form disabled" disabled={true}>
                    {this.state.fields['FNAME_TH']}
                  </div>
                  <input
                    type="hidden"
                    className={`form-control ${this.state.errorsFocus['FNAME_TH']} input_form`}
                    name="FNAME_TH"
                    onChange={e => this.handleChange(e)}
                    id="exampleFormControlInput1"
                    placeholder={
                      messages[this.state.language].formprofile1.name_input
                    }
                    value={this.state.fields.FNAME_TH}
                  />
                  <div className="errorMsg FNAME_TH">
                    {this.state.errors["FNAME_TH"]}
                  </div>
                </div>
                <div className="form-group">
                  <label>
                    <span className="required">* </span><FormattedMessage id="title_lastname" />
                  </label>
                  <div className="form-control input_form disabled" disabled={true}>
                    {this.state.fields['LNAME_TH']}
                  </div>
                  <input
                    type="hidden"
                    className={`form-control ${this.state.errorsFocus['LNAME_TH']} input_form`}
                    name="LNAME_TH"
                    onChange={e => this.handleChange(e)}
                    id="exampleFormControlInput1"
                    placeholder={
                      messages[this.state.language].formprofile1.lastname_input
                    }
                    value={this.state.fields['LNAME_TH']}
                  />
                  <div className="errorMsg LNAME_TH">
                    {this.state.errors["LNAME_TH"]}
                  </div>
                </div>
                <div className="form-group">
                  <label>
                    <span className="required">* </span><FormattedMessage id="title_sex" />
                  </label>
                  <select
                    className={`${genderClass} form-control ${this.state.errorsFocus['GENDER_ID']} input_form`}
                    name="GENDER_ID"
                    id="exampleFormControlSelect1"
                    onChange={e => this.handleChange(e)}
                    value={this.state.fields["GENDER_ID"]}
                  >
                    <option value="0">
                      {messages[this.state.language].formprofile1.SELECT_GENDER}
                    </option>
                    {this.renderContent("type_gender")}
                    {/* <option value="4">Other. อื่นๆ</option> */}
                  </select>
                  <div className="errorMsg GENDER_ID">
                    {this.state.errors["GENDER_ID"]}
                  </div>
                  {/* <FormattedHTMLMessage id="title_sexlist" /> */}
                </div>
                <div className="form-group">
                  <label>
                    <span className="required">* </span><FormattedMessage id="title_dateofbirth" />
                  </label>
                  <div className="form-control input_form disabled" disabled={true}>
                    {(this.state.language == 'en' ? moment(this.state.fields['BIRTH_DATE']).format('DD/MM/YYYY') : moment(this.state.fields['BIRTH_DATE']).add(543,'y').format('DD/MM/YYYY'))}
                  </div>
                  <input
                    type="hidden"
                    // max={maxDate}
                    name="BIRTH_DATE"
                    onChange={e => this.handleChange(e)}
                    className={`form-control ${this.state.errorsFocus['BIRTH_DATE']} input_form`}
                    id="date"
                    disabled={true}
                    value={this.state.fields['BIRTH_DATE']}
                  /> 
                  <div className="errorMsg BIRTH_DATE">
                    {this.state.errors["BIRTH_DATE"]}
                  </div>
                </div>
                <div className="form-group">
                  <label>
                    <span className="required">* </span><FormattedMessage id="title_status" />
                  </label>
                  <select
                    className={`form-control ${this.state.errorsFocus['MARITAL_STATUS']} input_form`}
                    name="MARITAL_STATUS"
                    id="exampleFormControlSelect1"
                    onChange={e => this.handleChange(e)}
                    value={this.state.fields['MARITAL_STATUS']}
                  >
                    <option value="0">
                      {
                        messages[this.state.language].formprofile1
                          .SELECT_MARITALSTATUS
                      }
                    </option>
                    {this.renderContent("type_status")}
                  </select>
                  <div className="errorMsg MARITAL_STATUS">
                    {this.state.errors["MARITAL_STATUS"]}
                  </div>
                  {/* <FormattedHTMLMessage id="title_statuslist" /> */}
                </div>
                <div className="form-group">
                  <label>
                    <FormattedMessage id="title_email" />
                  </label>
                  {/* <div className="form-control input_form disabled" disabled={true}>
                    {this.state.fields['USER_EMAIL']}
                  </div> */}
                  <input
                    type="text"
                    className={`form-control ${this.state.errorsFocus['USER_EMAIL']} input_form`}
                    name="USER_EMAIL"
                    onChange={e => this.handleChange(e)}
                    id="exampleFormControlInput1"
                    placeholder={
                      messages[this.state.language].formprofile1.email_input
                    }
                    // disabled={ (this.state.fields['USER_EMAIL'] ? true : false ) }
                    value={this.state.fields['USER_EMAIL']}
                  />
                  <div className="errorMsg USER_EMAIL">
                    {this.state.errors["USER_EMAIL"]}
                  </div>
                </div>
                <div className="form-group">
                  <label>
                    <span className="required">* </span><FormattedMessage id="title_phonenumber" />
                  </label>
                  <div className="form-control input_form disabled" disabled={true}>
                    {this.state.fields['PHONE_NO']}
                  </div>
                  <input
                    type="hidden"
                    maxLength="10"
                    // className={`form-control ${this.state.errorsFocus['PHONE_NO']} input_form ${(this.state.fields["PHONE_NO"] ? `` : '')}`}
                    className="form-control input_form"
                    name="PHONE_NO"
                    onChange={e => this.handleChange(e)}
                    id="exampleFormControlInput1"
                    placeholder={
                      messages[this.state.language].formprofile1.phone_input
                    }
                    value={this.state.fields["PHONE_NO"]}
                    disabled={(this.state.fields["PHONE_NO"] ? true : false )}
                  />
                  <div className="errorMsg PHONE_NO">
                    {this.state.errors["PHONE_NO"]}
                  </div>
                </div>
                <div className="btn_nextstep">
                  <button
                    type="submit"
                    className="btn btn-secondary btn-block btn_agreement"
                    // onClick={() => this.submitFinalForm()}
                  >
                    <FormattedMessage id="submitfinalform" />
                  </button>
                </div>
                {this.state.modal}
              </div>
            </div>
          </div>
          <DatePicker
                    value={this.state.time}
                    isOpen={this.state.isOpenDatepickerMobile}
                    confirmText={messages[this.state.language].formprofile1.confirmDate}
                    cancelText={messages[this.state.language].formprofile1.cancelDate}
                    onSelect={this.handleSelectDatepickerMobile}
                    headerFormat="DD/MM/YYYY"
                    showHeader={false}
                    theme="ios"
                    onCancel={this.handleCancelDatepickerMobile} />
        </form>
      </IntlProvider>
    );
  }
}

export default Updateprofile;