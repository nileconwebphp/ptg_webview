import React from 'react'

export default () => (
  <div>
    <div className="text-center content-group">
      <h1 className="error-title">404</h1>
      <h5>Oops, an error has occurred. Page not found!</h5>
    </div>
  </div>
)
