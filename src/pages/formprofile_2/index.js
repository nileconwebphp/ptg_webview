import React from "react";
import { regisMaxCard } from "../../_actions/regiser_ptmaxcard"
import "./formprofile_2.css";
import {
  IntlProvider,
  FormattedMessage,
} from "react-intl";
import { addLocaleData } from "react-intl";
import locale_en from "react-intl/locale-data/en";
import locale_th from "react-intl/locale-data/th";
import intlMessageEN from "./../../_translations/en.json";
import intlMessageTH from "./../../_translations/th.json";
import { API_LIST } from "../../_constants/matcher"

let amphureListData = [];
let tumbonListData = [];
let postcodeListData = [];
let postcodeListDataNew = [];

var getComponent;

addLocaleData([...locale_en, ...locale_th]);

var messages = {
  th: intlMessageTH,
  en: intlMessageEN
};


export default class Formprofile_2 extends React.Component {

  SessionData = JSON.parse(localStorage.getItem('inputBody'))

  constructor(props) {
    super(props);
    this.state = {
      language: "th",
      province: '',
      tumbon: '',
      amphure: '',
      amphureList: [],
      district: '',
      postcode: '',
      address_no: '',
      errorsFocus: {
        ADDR_NO: '',
        ADDR_ORG_PROVINCE_ID: '',
        ADDR_MOO: ''
      },
      errors: {},
      getPostCode: false,
      fields: {
        ADDR_NO: (localStorage.getItem('inputBody') !== null ? (JSON.parse(localStorage.getItem('inputBody')).ADDR_NO != null ? JSON.parse(localStorage.getItem('inputBody')).ADDR_NO : (this.props.location.state.data.CARD_TYPE_ID === 2 ? "90" : "")) : (this.props.location.state.data.CARD_TYPE_ID === 2 ? "90" : "")),
        ADDR_STREET: (localStorage.getItem('inputBody') !== null ? (JSON.parse(localStorage.getItem('inputBody')).ADDR_STREET != null ? JSON.parse(localStorage.getItem('inputBody')).ADDR_STREET : (this.props.location.state.data.CARD_TYPE_ID === 2 ? 'ถนนรัชดาภิเษก' : "")) : (this.props.location.state.data.CARD_TYPE_ID === 2 ? 'ถนนรัชดาภิเษก' : "")),
        ADDR_MOO: (localStorage.getItem('inputBody') !== null ? (JSON.parse(localStorage.getItem('inputBody')).ADDR_MOO != null ? JSON.parse(localStorage.getItem('inputBody')).ADDR_MOO : "" ) : ''),
        ADDR_VILLAGE: (localStorage.getItem('inputBody') !== null ? (JSON.parse(localStorage.getItem('inputBody')).ADDR_VILLAGE != null ? JSON.parse(localStorage.getItem('inputBody')).ADDR_VILLAGE : (this.props.location.state.data.CARD_TYPE_ID === 2 ? 'CW TOWER A (33rd Floor)' : "")): (this.props.location.state.data.CARD_TYPE_ID === 2 ? 'CW TOWER A (33rd Floor)' : "")),
        ADDR_SOI: (localStorage.getItem('inputBody') !== null ? (JSON.parse(localStorage.getItem('inputBody')).ADDR_SOI != null ? JSON.parse(localStorage.getItem('inputBody')).ADDR_SOI : "" ) : ''),
        ADDR_ORG_PROVINCE_ID: (localStorage.getItem('inputBody') !== null ? (JSON.parse(localStorage.getItem('inputBody')).ADDR_ORG_PROVINCE_ID != null ? JSON.parse(localStorage.getItem('inputBody')).ADDR_ORG_PROVINCE_ID : (this.props.location.state.data.CARD_TYPE_ID === 2 ? '5957AE60-4992-E311-9402-0050568975FF' : "")): (this.props.location.state.data.CARD_TYPE_ID === 2 ? '5957AE60-4992-E311-9402-0050568975FF' : "")),
        ADDR_ORG_AMPHURE_ID: (localStorage.getItem('inputBody') !== null ?(JSON.parse(localStorage.getItem('inputBody')).ADDR_ORG_AMPHURE_ID != null ? JSON.parse(localStorage.getItem('inputBody')).ADDR_ORG_AMPHURE_ID : (this.props.location.state.data.CARD_TYPE_ID === 2 ? '9D16266E-C692-E311-9402-0050568975FF' : "")): (this.props.location.state.data.CARD_TYPE_ID === 2 ? '9D16266E-C692-E311-9402-0050568975FF' : "")),
        ADDR_ORG_SUB_DISTRICT_ID: (localStorage.getItem('inputBody') !== null ?(JSON.parse(localStorage.getItem('inputBody')).ADDR_ORG_SUB_DISTRICT_ID != null ? JSON.parse(localStorage.getItem('inputBody')).ADDR_ORG_SUB_DISTRICT_ID : (this.props.location.state.data.CARD_TYPE_ID === 2 ? 'B2B68FBA-D492-E311-9402-0050568975FF' : "")) : (this.props.location.state.data.CARD_TYPE_ID === 2 ? 'B2B68FBA-D492-E311-9402-0050568975FF' : "")),
        ADDR_ORG_POSTALCODE_ID: (localStorage.getItem('inputBody') !== null ? (JSON.parse(localStorage.getItem('inputBody')).ADDR_ORG_POSTALCODE_ID != null ? JSON.parse(localStorage.getItem('inputBody')).ADDR_ORG_POSTALCODE_ID : (this.props.location.state.data.CARD_TYPE_ID === 2 ? '84ED344B-D592-E311-9402-0050568975FF' : "")) : (this.props.location.state.data.CARD_TYPE_ID === 2 ? '84ED344B-D592-E311-9402-0050568975FF' : "")),
        ADDR_OTHER: (localStorage.getItem('inputBody') !== null ? (JSON.parse(localStorage.getItem('inputBody')).ADDR_OTHER != null ? JSON.parse(localStorage.getItem('inputBody')).ADDR_OTHER : (this.props.location.state.data.CARD_TYPE_ID === 2 ? '' : "")): (this.props.location.state.data.CARD_TYPE_ID === 2 ? '' : "")),
      },
    };
  }

  componentDidMount() {
    sessionStorage.setItem('test' , true);
    let { fields } = this.state;
    const params = this.props.match.params;
    this.setState({ language: (params.lang ? params.lang.toString().toLowerCase() : API_LIST.defaultLang) });
    this.getProvinceNewApi().then(result => { 
      this.setState({ province: result.data.dropdowN_INFO }) 
    })
    this.getAmphure(this.state.fields['ADDR_ORG_PROVINCE_ID'])

    if(this.props.location.state.data) {
      let newObject = Object.assign(this.props.location.state.data , this.state.fields)
      fields = newObject
      this.setState({ fields })
    }
    if(this.props.location.state.data.CARD_TYPE_ID === 2){
      fields['ADDR_NO'] = '90';
      fields['ADDR_STREET'] = 'ถนนรัชดาภิเษก';
      fields['ADDR_VILLAGE'] = 'CW TOWER A (33rd Floor)';
      fields['ADDR_ORG_PROVINCE_ID'] = '5957AE60-4992-E311-9402-0050568975FF';
      fields['ADDR_ORG_AMPHURE_ID'] ='9D16266E-C692-E311-9402-0050568975FF';
      fields['ADDR_ORG_SUB_DISTRICT_ID'] = 'B2B68FBA-D492-E311-9402-0050568975FF';
      fields['ADDR_ORG_POSTALCODE_ID'] = '84ED344B-D592-E311-9402-0050568975FF';
      this.setState({ fields })
    }
  }

  handleSubmit(event) {
    // event.preventDefault()
    // localStorage.setItem('inputBody', JSON.stringify(this.state.fields))
    // this.props.history.push({
    //   pathname: `${process.env.PUBLIC_URL}/formprofile_3/${this.state.language}`,
    //   state: {
    //     data: this.state.fields
    //   }
    // });

    event.preventDefault()
    if (this.validateform()) {
      localStorage.setItem('inputBody', JSON.stringify(this.state.fields));
      this.props.history.push({
        pathname: `${process.env.PUBLIC_URL}/formprofile_3/${this.state.language}`,
        state: {
          data: this.state.fields
        }
      });
    } else {
      console.log(false)
    }
  }

  getProvinceNewApi(){
    return regisMaxCard.getProvinceNew()
  }

  getDropdownlist_type(title) {
    return regisMaxCard.getDropdownlist(title)
  }


  getAmphure(val, type) {
    let { fields } = this.state;
    if(type) {
      fields['ADDR_ORG_AMPHURE_ID'] = ''
      fields['ADDR_ORG_SUB_DISTRICT_ID'] = ''
      fields['ADDR_ORG_POSTALCODE_ID'] = ''
    }
    this.setState({ amphure: '' })
    var value = val;
    fields['ADDR_ORG_PROVINCE_ID'] = value
    this.setState({ fields })
    regisMaxCard.getAmphure(value).then(e => {
      this.setState({
        amphure: e.data.data
      })
      this.getTumbon(this.state.fields['ADDR_ORG_AMPHURE_ID'])
    })
  }

  getTumbon(val, type) {
    let { fields } = this.state;
    if(type) {
      fields['ADDR_ORG_SUB_DISTRICT_ID'] = ''
      fields['ADDR_ORG_POSTALCODE_ID'] = ''
    }
    fields['ADDR_ORG_AMPHURE_ID'] = val
    this.setState({ fields })
    var tumbon = val
    regisMaxCard.getTumbon(tumbon).then(e => {
      this.setState({
        tumbon: e.data.data
      })
      this.getPostcode(this.state.fields['ADDR_ORG_SUB_DISTRICT_ID'])
    })
  }

  getPostcode(val, type) {
    let { fields } = this.state;
    if(type) {
      fields['ADDR_ORG_POSTALCODE_ID'] = ''
    }
    fields['ADDR_ORG_SUB_DISTRICT_ID'] = val
    this.setState({ fields })
    var postcode = val
    regisMaxCard.getPostcode(postcode).then(e => {
      if(e.data.data.length === 1){
          fields['ADDR_ORG_POSTALCODE_ID'] = e.data.data[0].codE_GUID
          this.setState({ fields })
      }
        this.setState({
          postcode: e.data.data,
        })
    })
  }

  validateform() {
    let fields = this.state.fields;
    let errors = {};
    let errorsFocus = {};
    let formIsValid = true;
    var firstChar = fields.ADDR_NO.charAt(0)
    var check = /^[a-zA-Zก-ฮ\b]+$/;
    var getTrue = false
    var getSymbol = false
    var getReplaceChar = false
    var getReplace = /^[^0][0-9]{1,3}(\/[^0][0-9]{1,3})(\-[^0][0-9]{1,3})?$/;
    var getcheckreplace = /^[0-9]{1,5}(\-[0-9]{1,5})?$/
    var getChecklastChar = /^[0-9]{1,4}(\/[0-9]{1,4})([a-zA-Zก-ฮ]{1,})?$/
    var getcheck = false

    if (!fields["ADDR_NO"]) {
      formIsValid = false;
      errors["ADDR_NO"] = <FormattedMessage id="ALERT_ADDRESS_NO" />
      errorsFocus["ADDR_NO"] = 'errorFocus'
    }else{
      for(var i in fields.ADDR_NO){
        if(check.test(fields.ADDR_NO[i]) == true){
          getTrue = true
          if(getChecklastChar.test(fields.ADDR_NO) == false){
            getcheck = true
          }
        }else{
          getTrue = false
        }
        if(fields.ADDR_NO[i] == '-'){
          getSymbol = true
          if(getReplace.test(fields.ADDR_NO) == false ){
            getReplaceChar = true
          }
          if(getcheckreplace.test(fields.ADDR_NO) == true){
            getReplaceChar = false
          }
        }
      }
      if(getcheck == true){
        formIsValid = false;
        errors["ADDR_NO"] = <FormattedMessage id="ALERT_SHOW_NOT_NO"/>
        errorsFocus["ADDR_NO"] = 'errorFocus'
      } 
      if(getReplaceChar == true){
        formIsValid = false;
        errors["ADDR_NO"] = <FormattedMessage id="ALERT_SHOW_NOT_NO"/>
        errorsFocus["ADDR_NO"] = 'errorFocus'
      }
      if(getSymbol == true && getTrue == true){
        formIsValid = false;
        errors["ADDR_NO"] = <FormattedMessage id="ALERT_SYMBOL_CHAR"/>
        errorsFocus["ADDR_NO"] = 'errorFocus'
      }
      if(firstChar == 0){
        formIsValid = false;
        errors["ADDR_NO"] = <FormattedMessage id="ADDR_ADD_ZERO"/>
        errorsFocus["ADDR_NO"] = 'errorFocus'
      }
      var count = (fields.ADDR_NO.match(/[a-zA-Zก-ฮ]/g) || []).length;
      if(count > 1){
        formIsValid = false;
        errors["ADDR_NO"] = <FormattedMessage id="ALERT_CHARACTER_MORE"/>
        errorsFocus["ADDR_NO"] = 'errorFocus'
      }
    }

    if (!fields["ADDR_ORG_PROVINCE_ID"]) {
      formIsValid = false;
      errors["ADDR_ORG_PROVINCE_ID"] = <FormattedMessage id="ALERT_ADDRESS_PROVINCE" />
      errorsFocus["ADDR_ORG_PROVINCE_ID"] = 'errorFocus'
    }

    if (!fields["ADDR_ORG_AMPHURE_ID"]) {
      formIsValid = false;
      errors["ADDR_ORG_AMPHURE_ID"] = <FormattedMessage id="ALERT_ADDRESS_AMPHURE" />
      errorsFocus["ADDR_ORG_AMPHURE_ID"] = 'errorFocus'
    }

    if (!fields["ADDR_ORG_SUB_DISTRICT_ID"]) {
      formIsValid = false;
      errors["ADDR_ORG_SUB_DISTRICT_ID"] = <FormattedMessage id="ALERT_ADDRESS_DISTRICT" />
      errorsFocus["ADDR_ORG_SUB_DISTRICT_ID"] = 'errorFocus'
    }

    if (!fields["ADDR_ORG_POSTALCODE_ID"] || fields["ADDR_ORG_POSTALCODE_ID"] == '') {
      formIsValid = false;
      errors["ADDR_ORG_POSTALCODE_ID"] = <FormattedMessage id="ALERT_ADDRESS_POSTALCODE" />
      errorsFocus["ADDR_ORG_POSTALCODE_ID"] = 'errorFocus'
    }

    if(this.state.fields.CARD_TYPE_ID === 2){
      if(!fields["ADDR_OTHER"]){
        formIsValid = false;
        errors["ADDR_OTHER"] = messages[this.state.language].formprofile2.ALERT_ADDR_OTHER
        errorsFocus["ALERT_ADDR_OTHER"] = 'errorFocus'
      }
    }

    this.setState({
      errors: errors,
      errorsFocus: errorsFocus
    });
    return formIsValid;
  }

  handleChange(e) {
    let { errors, errorsFocus, fields } = this.state;
    if(e.target.name == 'ADDR_NO'){
      var chkAddr = this.checkAddress(e)
      var chkChar = this.checkCharacter(e)
      if(chkAddr === true) {
        fields[e.target.name] = e.target.value;
        errors[e.target.name] = null;
        errorsFocus[e.target.name] = ''
        var firstChar = e.target.value.charAt(0)
        var lastChar = e.target.value[e.target.value.length - 1]
        if(firstChar == '-'){
          errors[e.target.name] = <FormattedMessage id="ADDR_NO_PREVENT2" />;
          errorsFocus[e.target.name] = 'errorFocus'
        }else if(lastChar == '-'){
          errors[e.target.name] = <FormattedMessage id="ADDR_NO_PREVENT1" />;
          errorsFocus[e.target.name] = 'errorFocus'
        }else       
          if(chkChar == true){
            errors[e.target.name] = <FormattedMessage id="ALERT_CHARACTER_MORE" />;
            errorsFocus[e.target.name] = 'errorFocus'
            e.preventDefault();
          }else if(chkChar == false){
            fields[e.target.name] = e.target.value;
            errors[e.target.name] = null;
            errorsFocus[e.target.name] = ''
          }
      } else {
        errors[e.target.name] = <FormattedMessage id="ADDR_NO_PREVENT" />;
        errorsFocus[e.target.name] = 'errorFocus'
      }
    }
    else if(e.target.name === 'ADDR_MOO') {
        const re = /^[0-9\b]+$/;
        if (re.test(e.target.value) !== false) {
          fields[e.target.name] = e.target.value;
          errors[e.target.name] = null;
          errorsFocus[e.target.name] = ''
        } else {
          if(e.target.value === '') { fields[e.target.name] = ''; }
            errors[e.target.name] = <FormattedMessage id="PREVENT_MOO_NO" />;
            errorsFocus[e.target.name] = ''
            errorsFocus["ADDR_MOO"] = 'errorFocus'
            e.preventDefault();
        }
      } 
      else {
        fields[e.target.name] = e.target.value;
        errors[e.target.name] = null;
        errorsFocus[e.target.name] = ''
      } 
    this.setState({ errors, fields, errorsFocus, [e.target.id]: e.target.value });
  }

  checkAddress(e){
    if((e.target.value.match(/-/g) || []).length > 1){
      return false
    }else{
      return true
    }
  }

  checkCharacter(e){
    var check = /^\d*[a-zA-Zก-ฮ][a-zA-Zก-ฮ\d\s]{1,10}$/;
    if(check.test(e.target.value) != false){
      return true
    }else{
      return false
    }
  }

  renderContent(type) {
    const { province } = this.state;
    var option = []
    var name_title;
    if (type === 'type_province') {
      name_title = province
    }

    for (var i in name_title) {
      option.push(<option key={i} value={name_title[i].codE_GUID}>{name_title[i].values}</option>)
    }

    return <select
      className={`form-control input_form ${this.state.errorsFocus['ADDR_ORG_PROVINCE_ID']}`}
      id='exampleFormControlSelect1'
      name="ADDR_ORG_PROVINCE_ID"
      value={this.state.fields['ADDR_ORG_PROVINCE_ID']}
      onChange={(e) => this.getAmphure(e.target.value, 'onchange')}
    >
      <option name="ADDR_ORG_PROVINCE_ID">{messages[this.state.language].formprofile2.title_province}</option>
      {option}
    </select>
  }

  render() {
    var { fields, amphure, tumbon, postcode } = this.state;
    amphureListData = [];
    for (var i in amphure) {
      amphureListData.push(<option name="ADDR_ORG_AMPHURE_ID" key={i} value={amphure[i].codE_GUID}>{amphure[i].values}</option>)
    }

    tumbonListData = [];
    for (var i in tumbon) {
      tumbonListData.push(<option name="ADDR_ORG_SUB_DISTRICT_ID" key={i} value={tumbon[i].codE_GUID}>{tumbon[i].values}</option>)
    }

    postcodeListData = [];
    for (var i in postcode) {
        postcodeListData.push(<option name="ADDR_ORG_POSTALCODE_ID" key={i} value={postcode[i].codE_GUID}>{postcode[i].values}</option>)
    }
    var formThaiPerson = <form className="form-horizontal" onSubmit={(e) => this.handleSubmit(e)}>
      
    <div className="bg_full">
      <div className="container-fluid">
        <div className="content_page">
          <div className="head_title_agree"><FormattedMessage id="header_address" /></div>
          <div className="form-group">
            <span className="required">* </span><span className="label1"><FormattedMessage id="title_numaddress" /></span>
            <input 
              type="text" 
              className={`form-control input_form ${this.state.errorsFocus['ADDR_NO']}`} 
              id="exampleFormControlInput1" 
              maxLength="10" 
              name="ADDR_NO" 
              onChange={(e) => this.handleChange(e)} 
              value={this.state.fields['ADDR_NO']}
              placeholder={messages[this.state.language].formprofile2.house_input} />
            <div className="errorMsg">{this.state.errors['ADDR_NO']}</div>
          </div>
          <div className="form-group">
            <label><FormattedMessage id="title_moo" /></label>
            <input
              type="text"
              maxLength="3"
              className={`form-control input_form ${this.state.errorsFocus['ADDR_MOO']}`}
              id="exampleFormControlInput1"
              name="ADDR_MOO"
              onChange={(e) => this.handleChange(e)}
              value={this.state.fields['ADDR_MOO']}
              placeholder={messages[this.state.language].formprofile2.moo_input}
            />
            <div className="errorMsg">{this.state.errors['ADDR_MOO']}</div>
          </div>
          <div className="form-group">
            <label><FormattedMessage id="title_addressbldg" /></label>
            <input type="text" className="form-control input_form" id="exampleFormControlInput1" name="ADDR_VILLAGE" onChange={(e) => this.handleChange(e)} value={this.state.fields.ADDR_VILLAGE} placeholder={messages[this.state.language].formprofile2.building_input} />
          </div>
          <div className="form-group">
            <label><FormattedMessage id="title_lane" /></label>
            <input type="text" className="form-control input_form" name="ADDR_SOI" onChange={(e) => this.handleChange(e)} value={this.state.fields.ADDR_SOI} id="exampleFormControlInput1" placeholder={messages[this.state.language].formprofile2.lane_input} />
          </div>
          <div className="form-group">
            <label><FormattedMessage id="title_road" /></label>
            <input type="text" className="form-control input_form" name="ADDR_STREET" onChange={(e) => this.handleChange(e)} value={this.state.fields.ADDR_STREET} id="exampleFormControlInput1" placeholder={messages[this.state.language].formprofile2.road_input} />
          </div>
          <div className="form-group">
            <label><span className="required">* </span><FormattedMessage id="title_province" /></label>
            {this.renderContent('type_province')}
            <div className="errorMsg">{this.state.errors['ADDR_ORG_PROVINCE_ID']}</div>
          </div>
          <div className="form-group">
            <label><span className="required">* </span><FormattedMessage id="title_area" /></label>
            <select
              className='form-control'
              id='exampleFormControlSelect1'
              name="ADDR_ORG_AMPHURE_ID"
              onChange={(e) => this.getTumbon(e.target.value, 'onchange')}
              value={this.state.fields['ADDR_ORG_AMPHURE_ID']}
            >
              <option value="0">{messages[this.state.language].formprofile2.title_area}</option>
              {amphureListData}
            </select>
            <div className="errorMsg">{this.state.errors['ADDR_ORG_AMPHURE_ID']}</div>
          </div>
          <div className="form-group">
            <label><span className="required">* </span><FormattedMessage id="title_subarea" /></label>
            <select
              className='form-control'
              id='exampleFormControlSelect1'
              name="ADDR_ORG_SUB_DISTRICT_ID"
              onChange={(e) => this.getPostcode(e.target.value, 'onchange')}
              value={this.state.fields['ADDR_ORG_SUB_DISTRICT_ID']}
            >
              <option value="0">{messages[this.state.language].formprofile2.title_subarea}</option>
              {tumbonListData}
            </select>
            <div className="errorMsg">{this.state.errors['ADDR_ORG_SUB_DISTRICT_ID']}</div>
          </div>
          <div className="form-group">
            <label><span className="required">* </span><FormattedMessage id="title_postcode" /></label>
            <select 
            className='form-control' 
            id='exampleFormControlSelect1' 
            name="ADDR_ORG_POSTALCODE_ID" 
            onChange={(e) => this.handleChange(e)}
            value={this.state.fields['ADDR_ORG_POSTALCODE_ID']}
            >
              <option value="0">{messages[this.state.language].formprofile2.title_postcode}</option>
              {postcodeListData}
            </select>
            <div className="errorMsg">{this.state.errors['ADDR_ORG_POSTALCODE_ID']}</div>
          </div>
          <div className="form-group">
            <label><FormattedMessage id="title_additional" /></label>
            <textarea className="form-control input_form" id="exampleFormControlTextarea1" rows="3" name="ADDR_OTHER" value={this.state.fields.ADDR_OTHER} onChange={(e) => this.handleChange(e)} placeholder={messages[this.state.language].formprofile2.additional_input}></textarea>
          </div>
            <div className="btn_nextstep">
              <button type="submit" className="btn btn-secondary btn-block btn_agreement"><FormattedMessage id="button_nextstep" /></button>
            </div>
        </div>
      </div>
    </div>
  </form>

  var formForeigner = <form className="form-horizontal" onSubmit={(e) => this.handleSubmit(e)}><div className="bg_full">
  <div className="container-fluid"><div className="content_page"><div className="head_title_agree"><FormattedMessage id="header_address" /></div><div className="form-group"><label><span className="required">* </span><FormattedMessage id="title_additional" /></label>
    <textarea className="form-control input_form" id="exampleFormControlTextarea1" rows="3" name="ADDR_OTHER" value={this.state.fields.ADDR_OTHER} onChange={(e) => this.handleChange(e)} placeholder={messages[this.state.language].formprofile2.additional_input}></textarea>
    <div className="errorMsg">{this.state.errors['ADDR_OTHER']}</div>
  </div>
  <div className="btn_nextstep">
  <button type="submit" className="btn btn-secondary btn-block btn_agreement"><FormattedMessage id="button_nextstep" /></button>
  </div></div></div></div></form>

    return (
      <IntlProvider
        locale={this.state.language}
        messages={messages[this.state.language].formprofile2}
      >
        <div>
          {(this.state.fields.CARD_TYPE_ID === 2 ? formForeigner : formThaiPerson)}
        </div>
      </IntlProvider>
    )
  }
}
