import React from "react";
import { regisMaxCard } from "../../_actions/regiser_ptmaxcard"
import {
  IntlProvider,
  FormattedMessage,
  // FormattedHTMLMessage
} from "react-intl";
import { addLocaleData } from "react-intl";
import locale_en from "react-intl/locale-data/en";
import locale_th from "react-intl/locale-data/th";
import intlMessageEN from "./../../_translations/en.json";
import intlMessageTH from "./../../_translations/th.json";
import { API_LIST } from "../../_constants/matcher"
import SweetAlert from "react-bootstrap-sweetalert";

var alert;

let amphureListData = [];
let tumbonListData = [];
let postcodeListData = [];
let provinceListData = [];

addLocaleData([...locale_en, ...locale_th]);

var messages = {
  th: intlMessageTH,
  en: intlMessageEN
};

export default class updateAddress extends React.Component { 

  SessionData = JSON.parse(sessionStorage.getItem('inputBody'))

  constructor(props) {
    super(props);
    this.state = {
      language: "th",
      province: '',
      tumbon: '',
      amphure: '',
      amphureList: [],
      district: '',
      postcode: '',
      address_no: '',
      errorsFocus: {
        ADDR_ADDRESS: '',
        ADDR_ORG_PROVINCE_ID: '',
        ADDR_MOO: ''
      },
      errors: {},
      content_page: "none",
      content_loading: "block",
      errors: {},
      fields: {},
      show: false,
      modal: null,
    };
  }

  componentDidMount() {
    let { fields } = this.state;
    // this.updateProfileAddress()
    const params = this.props.match.params;
    var tokenData = this.props.location;
    var sstr = tokenData.search.search("&");
    var sub_memberid = tokenData.search.substr(sstr);
    var memberid = sub_memberid.substr(5);
    sessionStorage.setItem('_usid' , memberid)
    sessionStorage.setItem('_tokenid' , params.tokenid)
    sessionStorage.setItem('_customerid' , params.customerid)
    sessionStorage.setItem('_udid' , params.udid)
    sessionStorage.setItem('_deviceos' , params.deviceos)
    this.setState({ language: (params.lang ? params.lang.toString().toLowerCase() : API_LIST.defaultLang) });
    // if(this.props.location.state.data !== null) {
    //   let newObject = Object.assign(this.props.location.state.data , this.state.fields)
    //   fields = newObject
    //   this.setState({ fields })
    // }
    this.updateProfileAddress()
    this.getDropdownlist_type("PROVINCE").then(result => { this.setState({ province: result.dropdowN_INFO }) })
  }

  updateProfileAddress(){
    let { fields , getToken } = this.state;
    var token = sessionStorage.getItem('_tokenid')
    var customer = sessionStorage.getItem('_customerid')
    var userid = sessionStorage.getItem('_usid')
    var content_page = ''
    var content_loading = ''

    regisMaxCard.updateProfileList(token,customer,userid).then(item => {
      var customerData = JSON.parse(item.data.data.data)
      var customerList = customerData.CUSTOMER_PROFILE_INFO[0]
      let newObject = Object.assign(customerList , this.state.fields)
      fields = newObject

      fields['SUBSCRIBE_EMAIL'] = (JSON.parse(sessionStorage.getItem('inputBody')).SUBSCRIBE_EMAIL != null ? JSON.parse(sessionStorage.getItem('inputBody')).SUBSCRIBE_EMAIL : (fields["SUBSCRIBE_EMAIL"] === 1 ? true : false)),
      fields['SUBSCRIBE_LETTER'] = (JSON.parse(sessionStorage.getItem('inputBody')).SUBSCRIBE_LETTER != null ? JSON.parse(sessionStorage.getItem('inputBody')).SUBSCRIBE_LETTER : (fields["SUBSCRIBE_LETTER"] === 1 ? true : false)),
      fields['SUBSCRIBE_NONE'] = (JSON.parse(sessionStorage.getItem('inputBody')).SUBSCRIBE_NONE != null ? JSON.parse(sessionStorage.getItem('inputBody')).SUBSCRIBE_NONE : (fields["SUBSCRIBE_NONE"] === 1 ? true : false)),
      fields['SUBSCRIBE_SMS'] = (JSON.parse(sessionStorage.getItem('inputBody')).SUBSCRIBE_SMS != null ? JSON.parse(sessionStorage.getItem('inputBody')).SUBSCRIBE_SMS : (fields["SUBSCRIBE_SMS"] === 1 ? true : false)),
      fields['PMT_BEAUTY'] = (JSON.parse(sessionStorage.getItem('inputBody')).PMT_BEAUTY != null ? JSON.parse(sessionStorage.getItem('inputBody')).PMT_BEAUTY : (fields["PMT_BEAUTY"] === 1 ? true : false)),
      fields['PMT_FOOD'] = (JSON.parse(sessionStorage.getItem('inputBody')).PMT_FOOD != null ? JSON.parse(sessionStorage.getItem('inputBody')).PMT_FOOD : (fields["PMT_FOOD"] === 1 ? true : false)),
      fields['PMT_OTHERS'] = (JSON.parse(sessionStorage.getItem('inputBody')).PMT_OTHERS != null ? JSON.parse(sessionStorage.getItem('inputBody')).PMT_OTHERS : (fields["PMT_OTHERS"] === 1 ? true : false)),
      fields['PMT_OTHERS_REMARK'] = (JSON.parse(sessionStorage.getItem('inputBody')).PMT_OTHERS_REMARK != null ? JSON.parse(sessionStorage.getItem('inputBody')).PMT_OTHERS_REMARK : fields["PMT_OTHERS_REMARK"]),
      fields['PMT_SHOPPING'] = (JSON.parse(sessionStorage.getItem('inputBody')).PMT_SHOPPING != null ? JSON.parse(sessionStorage.getItem('inputBody')).PMT_SHOPPING : (fields["PMT_SHOPPING"] === 1 ? true : false)),
      fields['PMT_SPORT_AND_CAR'] = (JSON.parse(sessionStorage.getItem('inputBody')).PMT_SPORT_AND_CAR != null ? JSON.parse(sessionStorage.getItem('inputBody')).PMT_SPORT_AND_CAR : (fields["PMT_SPORT_AND_CAR"] === 1 ? true : false)),
      fields['PMT_TRAVEL'] = (JSON.parse(sessionStorage.getItem('inputBody')).PMT_TRAVEL != null ? JSON.parse(sessionStorage.getItem('inputBody')).PMT_TRAVEL : (fields["PMT_TRAVEL"] === 1 ? true : false)),
      fields['ACT_EN_MUSIC'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_EN_MUSIC != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_EN_MUSIC : (fields["ACT_EN_MUSIC"] === 1 ? true : false)),
      fields['ACT_EN_CONCERT'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_EN_CONCERT != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_EN_CONCERT : (fields["ACT_EN_CONCERT"] === 1 ? true : false)),
      fields['ACT_EN_COUNTRY_CONCERT'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_EN_COUNTRY_CONCERT != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_EN_COUNTRY_CONCERT : (fields["ACT_EN_COUNTRY_CONCERT"] === 1 ? true : false )),
      fields['ACT_EN_MOVIES'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_EN_MOVIES != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_EN_MOVIES : (fields["ACT_EN_MOVIES"] === 1 ? true : false)),
      fields['ACT_EN_THEATER'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_EN_THEATER != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_EN_THEATER : (fields["ACT_EN_THEATER"] === 1 ? true : false )),
      fields['ACT_EN_OTHERS'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_EN_OTHERS != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_EN_OTHERS : (fields["ACT_EN_OTHERS"] === 1 ? true : false)),
      fields['ACT_SPT_FITNESS'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_SPT_FITNESS != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_SPT_FITNESS : (fields["ACT_SPT_FITNESS"] === 1 ? true : false)),
      fields['ACT_SPT_RUN'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_SPT_RUN != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_SPT_RUN : (fields["ACT_SPT_RUN"] === 1 ? true : false )),
      fields['ACT_SPT_CYCLING'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_SPT_CYCLING != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_SPT_CYCLING : (fields["ACT_SPT_CYCLING"] === 1 ? true : false)),
      fields['ACT_SPT_FOOTBALL'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_SPT_FOOTBALL != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_SPT_FOOTBALL : (fields["ACT_SPT_FOOTBALL"] === 1 ? true : false)),
      fields['ACT_SPT_OTHERS'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_SPT_OTHERS != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_SPT_OTHERS : (fields["ACT_SPT_OTHERS"] === 1 ? true : false)),
      fields['ACT_OD_SHOPPING'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_OD_SHOPPING != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_OD_SHOPPING : (fields["ACT_OD_SHOPPING"] === 1 ? true : false )),
      fields['ACT_OD_BEAUTY'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_OD_BEAUTY != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_OD_BEAUTY : (fields["ACT_OD_BEAUTY"] === 1 ? true : false)),
      fields['ACT_OD_RESTAURANT'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_OD_RESTAURANT != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_OD_RESTAURANT : (fields["ACT_OD_RESTAURANT"] === 1 ? true : false)),
      fields['ACT_OD_CLUB'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_OD_CLUB != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_OD_CLUB : (fields["ACT_OD_CLUB"] === 1 ? true : false)),
      fields['ACT_OD_MOMCHILD'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_OD_MOMCHILD != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_OD_MOMCHILD : (fields["ACT_OD_MOMCHILD"] === 1 ? true : false)),
      fields['ACT_OD_RALLY'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_OD_RALLY != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_OD_RALLY : (fields["ACT_OD_RALLY"] === 1 ? true : false )),
      fields['ACT_OD_ADVANTURE'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_OD_ADVANTURE != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_OD_ADVANTURE : (fields["ACT_OD_ADVANTURE"] === 1 ? true : false )),
      fields['ACT_OD_PHOTO'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_OD_PHOTO != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_OD_PHOTO : (fields["ACT_OD_PHOTO"] === 1 ? true : false )),
      fields['ACT_OD_PHILANTHROPY'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_OD_PHILANTHROPY != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_OD_PHILANTHROPY : (fields["ACT_OD_PHILANTHROPY"] === 1 ? true : false )),
      fields['ACT_OD_HOROSCOPE'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_OD_HOROSCOPE != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_OD_HOROSCOPE : (fields["ACT_OD_HOROSCOPE"] === 1 ? true : false )),
      fields['ACT_OD_ABROAD'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_OD_ABROAD != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_OD_ABROAD : (fields["ACT_OD_ABROAD"] === 1 ? true : false )),
      fields['ACT_OD_UPCOUNTRY'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_OD_UPCOUNTRY != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_OD_UPCOUNTRY : (fields["ACT_OD_UPCOUNTRY"] === 1 ? true : false )),
      fields['ACT_OD_OTHERS'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_OD_OTHERS != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_OD_OTHERS : (fields["ACT_OD_OTHERS"] === 1 ? true : false )),
      fields['ACT_ID_COOKING'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_ID_COOKING != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_ID_COOKING : (fields["ACT_ID_COOKING"] === 1 ? true : false )),
      fields['ACT_ID_BAKING'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_ID_BAKING != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_ID_BAKING : (fields["ACT_ID_BAKING"] === 1 ? true : false )),
      fields['ACT_ID_DECORATE'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_ID_DECORATE != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_ID_DECORATE : (fields["ACT_ID_DECORATE"] === 1 ? true : false )),
      fields['ACT_ID_GARDENING'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_ID_GARDENING != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_ID_GARDENING : (fields["ACT_ID_GARDENING"] === 1 ? true : false )),
      fields['ACT_ID_READING'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_ID_READING != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_ID_READING : (fields["ACT_ID_READING"] === 1 ? true : false )),
      fields['ACT_ID_GAMING'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_ID_GAMING != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_ID_GAMING : (fields["ACT_ID_GAMING"] === 1 ? true : false )),
      fields['ACT_ID_INTERNET'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_ID_INTERNET != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_ID_INTERNET : (fields["ACT_ID_INTERNET"] === 1 ? true : false)),
      fields['ACT_ID_OTHERS'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_ID_OTHERS != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_ID_OTHERS : (fields["ACT_ID_OTHERS"] === 1 ? true : false)),
      fields['ACT_HNGOUT_FRIEND'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_HNGOUT_FRIEND != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_HNGOUT_FRIEND : (fields["ACT_HNGOUT_FRIEND"] === 1 ? true : false )),
      fields['ACT_HNGOUT_COUPLE'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_HNGOUT_COUPLE != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_HNGOUT_COUPLE : (fields["ACT_HNGOUT_COUPLE"] === 1 ? true : false )),
      fields['ACT_HNGOUT_FAMILY'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_HNGOUT_FAMILY != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_HNGOUT_FAMILY : (fields["ACT_HNGOUT_FAMILY"] === 1 ? true : false )),
      fields['ACT_HNGOUT_NO'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_HNGOUT_NO != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_HNGOUT_NO : (fields["ACT_HNGOUT_NO"] === 1 ? true : false )),
      fields['ACT_EN_OTHERS_REMARK'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_EN_OTHERS_REMARK != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_EN_OTHERS_REMARK : fields["ACT_EN_OTHERS_REMARK"] ),
      fields['ACT_SPT_OTHERS_REMARK'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_SPT_OTHERS_REMARK != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_SPT_OTHERS_REMARK : fields["ACT_SPT_OTHERS_REMARK"] ),
      fields['ACT_OD_OTHERS_REMARK'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_OD_OTHERS_REMARK != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_OD_OTHERS_REMARK : fields["ACT_OD_OTHERS_REMARK"] ),
      fields['ACT_ID_OTHERS_REMARK'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_ID_OTHERS_REMARK != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_ID_OTHERS_REMARK : fields["ACT_ID_OTHERS_REMARK"])
      
      fields['ADDR_ORG_AMPHURE_ID'] = customerList.ADDR_ORG_AMPHURE_ID.toUpperCase()
      fields['ADDR_ORG_PROVINCE_ID'] = customerList.ADDR_ORG_PROVINCE_ID.toUpperCase()
      fields['ADDR_ORG_POSTALCODE_ID'] = customerList.ADDR_ORG_POSTALCODE_ID.toUpperCase()
      fields['ADDR_ORG_SUB_DISTRICT_ID'] = customerList.ADDR_ORG_SUB_DISTRICT_ID.toUpperCase()
      
      this.setState({ fields , getToken : item.data.data.tokenId })
      this.getAmphure(fields['ADDR_ORG_PROVINCE_ID'])


     if (item.data.errMsg === "Success" ) {
        content_loading = "none";
        content_page = "block";
     } else {
        content_loading = "block";
        content_page = "none";
     }
     
     var th = this
     setTimeout(function(){
      th.setState({content_loading: content_loading})
      th.setState({content_page: content_page})
     }, 1000);
    })
  }

  modalCheckSubmit(res,img){
    alert = (
      <SweetAlert
        custom
        confirmBtnBsStyle="success"
        closeOnClickOutside={true}
        cancelBtnBsStyle="default"
        focusConfirmBtn={false}
        title=""
        customIcon={img}
        showConfirm={false}
        showCancelButton
        onCancel={() => this.handleChoice(false)}
        onConfirm={() => this.handleChoice(true)}
        onOutsideClick={() => {
          this.wasOutsideClick = true;
          this.setState({ showConfirm: false })
        }}
      >
        {res}
      </SweetAlert>
    );
    this.setState({ show: true , modal: alert })
  }

  handleChoice(choice) {
    if (choice === false) {
      this.setState({ show: false , modal: null })
    }
  }

  submitFinalForm(){
    // event.preventDefault()
    sessionStorage.setItem('inputBody', JSON.stringify(this.state.fields))
    if(this.validateform()){
      regisMaxCard.updateProfile(this.state.fields,this.state.getToken).then(e => {
        if(e.data.isSuccess === false) {
          this.setState({ disableAgreement: false })
            this.setState({ resultApi: e.data.errMsg })
            this.setState({ disabledBtn: '' })
            var imgPopup = `${process.env.PUBLIC_URL}/images/cancel.png`;
            this.modalCheckSubmit(this.state.resultApi,imgPopup)
        } else {
          window.location.href=`PTG:||updateprofile||success||phonenumber=${this.state.fields.PHONE_NO}`
        }
      })
    }else{
      console.log(false)
    }
  }

  getDropdownlist_type(title) {
    return regisMaxCard.getDropdownlist(title)
  }


  getAmphure(val, type) {
    let { fields } = this.state;
    if(type) {
      fields['ADDR_ORG_AMPHURE_ID'] = ''
      fields['ADDR_ORG_SUB_DISTRICT_ID'] = ''
      fields['ADDR_ORG_POSTALCODE_ID'] = ''
    }
    this.setState({ amphure: '' })
    var value = val;
    fields['ADDR_ORG_PROVINCE_ID'] = value
    this.setState({ fields })
    regisMaxCard.getAmphure(value).then(e => {
      this.setState({
        amphure: e.data.data
      })
      this.getTumbon(this.state.fields['ADDR_ORG_AMPHURE_ID'])
    })
  }

  getTumbon(val, type) {
    let { fields } = this.state;
    if(type) {
      fields['ADDR_ORG_SUB_DISTRICT_ID'] = ''
      fields['ADDR_ORG_POSTALCODE_ID'] = ''
    }
    fields['ADDR_ORG_AMPHURE_ID'] = val
    this.setState({ fields })
    var tumbon = val
    regisMaxCard.getTumbon(tumbon).then(e => {
      this.setState({
        tumbon: e.data.data
      })
      this.getPostcode(this.state.fields['ADDR_ORG_SUB_DISTRICT_ID'])
    })
  }

  getPostcode(val, type) {
    let { fields } = this.state;
    if(type) {
      fields['ADDR_ORG_POSTALCODE_ID'] = ''
    }
    fields['ADDR_ORG_SUB_DISTRICT_ID'] = val
    this.setState({ fields })
    var postcode = val
    regisMaxCard.getPostcode(postcode).then(e => {
      if(type === 'onchange'){
        if(e.data.data.length === 1){
          fields['ADDR_ORG_POSTALCODE_ID'] = e.data.data[0].codE_GUID
          this.setState({ fields , postcode : e.data.data })
        }else{
          this.setState({
            postcode: e.data.data
          })
        }
      }else{
        this.setState({
          postcode: e.data.data
        })
      }
    })
  }

  async handleSubmit(event) {
    var { fields } = this.state;
    event.preventDefault();
    if (await this.validateform()) {
      sessionStorage.setItem('inputBody', JSON.stringify(this.state.fields))
      this.submitFinalForm();
    } else {
      console.log('formsubmit ' + false);
    }
  }

  async validateform() {
    let fields = this.state.fields;
    let errors = {};
    let errorsFocus = {};
    let formIsValid = true;
    var firstChar = fields.ADDR_ADDRESS.charAt(0)
    var check = /^[a-zA-Zก-ฮ\b]+$/;
    var getTrue = false
    var getSymbol = false
    var getReplaceChar = false
    var getReplace = /^[^0][0-9]{1,3}(\/[^0][0-9]{1,3})(\-[^0][0-9]{1,3})?$/;
    var getcheckreplace = /^[0-9]{1,5}(\-[0-9]{1,5})?$/
    var getChecklastChar = /^[0-9]{1,4}(\/[0-9]{1,4})([a-zA-Zก-ฮ]{1,})?$/
    var getcheck = false

    if (!fields["ADDR_ADDRESS"]) {
      formIsValid = false;
      errors["ADDR_ADDRESS"] = <FormattedMessage id="ALERT_ADDRESS_NO" />
      errorsFocus["ADDR_ADDRESS"] = 'errorFocus'
    }else{
      for(var i in fields.ADDR_ADDRESS){
        if(check.test(fields.ADDR_ADDRESS[i]) == true){
          getTrue = true
          if(getChecklastChar.test(fields.ADDR_ADDRESS) == false){
            getcheck = true
          }
        }else{
          getTrue = false
        }
        if(fields.ADDR_ADDRESS[i] == '-'){
          getSymbol = true
          if(getReplace.test(fields.ADDR_ADDRESS) == false ){
            getReplaceChar = true
          }
          if(getcheckreplace.test(fields.ADDR_ADDRESS) == true){
            getReplaceChar = false
          }
        }
      }
      if(getcheck == true){
        formIsValid = false;
        errors["ADDR_ADDRESS"] = <FormattedMessage id="ALERT_SHOW_NOT_NO"/>
        errorsFocus["ADDR_ADDRESS"] = 'errorFocus'
      } 
      if(getReplaceChar == true){
        formIsValid = false;
        errors["ADDR_ADDRESS"] = <FormattedMessage id="ALERT_SHOW_NOT_NO"/>
        errorsFocus["ADDR_ADDRESS"] = 'errorFocus'
      }
      if(getSymbol == true && getTrue == true){
        formIsValid = false;
        errors["ADDR_ADDRESS"] = <FormattedMessage id="ALERT_SYMBOL_CHAR"/>
        errorsFocus["ADDR_ADDRESS"] = 'errorFocus'
      }
      if(firstChar == 0){
        formIsValid = false;
        errors["ADDR_ADDRESS"] = <FormattedMessage id="ADDR_ADD_ZERO"/>
        errorsFocus["ADDR_ADDRESS"] = 'errorFocus'
      }
      var count = (fields.ADDR_ADDRESS.match(/[a-zA-Zก-ฮ]/g) || []).length;
      if(count > 1){
        formIsValid = false;
        errors["ADDR_ADDRESS"] = <FormattedMessage id="ALERT_CHARACTER_MORE"/>
        errorsFocus["ADDR_ADDRESS"] = 'errorFocus'
      }
    }

    if (!fields["ADDR_ORG_PROVINCE_ID"]) {
      formIsValid = false;
      errors["ADDR_ORG_PROVINCE_ID"] = <FormattedMessage id="ALERT_ADDRESS_PROVINCE" />
      errorsFocus["ADDR_ORG_PROVINCE_ID"] = 'errorFocus'
    }

    if (!fields["ADDR_ORG_AMPHURE_ID"]) {
      formIsValid = false;
      errors["ADDR_ORG_AMPHURE_ID"] = <FormattedMessage id="ALERT_ADDRESS_AMPHURE" />
      errorsFocus["ADDR_ORG_AMPHURE_ID"] = 'errorFocus'
    }

    if (!fields["ADDR_ORG_SUB_DISTRICT_ID"]) {
      formIsValid = false;
      errors["ADDR_ORG_SUB_DISTRICT_ID"] = <FormattedMessage id="ALERT_ADDRESS_DISTRICT" />
      errorsFocus["ADDR_ORG_SUB_DISTRICT_ID"] = 'errorFocus'
    }

    if (!fields["ADDR_ORG_POSTALCODE_ID"] || fields["ADDR_ORG_POSTALCODE_ID"] == '') {
      formIsValid = false;
      errors["ADDR_ORG_POSTALCODE_ID"] = <FormattedMessage id="ALERT_ADDRESS_POSTALCODE" />
      errorsFocus["ADDR_ORG_POSTALCODE_ID"] = 'errorFocus'
    }

    if(this.state.fields.CARD_TYPE_ID === 2){
      if(!fields["ADDR_OTHER"]){
        formIsValid = false;
        errors["ADDR_OTHER"] = <FormattedMessage id="ALERT_ADDR_OTHER" />
        errorsFocus["ALERT_ADDR_OTHER"] = 'errorFocus'
      }
    }

    this.setState({
      errors: errors,
      errorsFocus: errorsFocus
    });
    return formIsValid;
  }


  handleChange(e) {
    let { errors, errorsFocus, fields } = this.state;
    if(e.target.name == 'ADDR_ADDRESS'){
      var chkAddr = this.checkAddress(e)
      var chkChar = this.checkCharacter(e)
      if(chkAddr === true) {
        fields[e.target.name] = e.target.value;
        errors[e.target.name] = null;
        errorsFocus[e.target.name] = ''
        var firstChar = e.target.value.charAt(0)
        var lastChar = e.target.value[e.target.value.length - 1]
        if(firstChar == '-'){
          errors[e.target.name] = messages[this.state.language].formprofile2.ADDR_NO_PREVENT2
          errorsFocus[e.target.name] = 'errorFocus'
        }else if(lastChar == '-'){
          errors[e.target.name] = messages[this.state.language].formprofile2.ADDR_NO_PREVENT1
          errorsFocus[e.target.name] = 'errorFocus'
        }else       
          if(chkChar == true){
            errors[e.target.name] = messages[this.state.language].formprofile.ALERT_CHARACTER_MORE
            errorsFocus[e.target.name] = 'errorFocus'
            e.preventDefault();
          }else if(chkChar == false){
            fields[e.target.name] = e.target.value;
            errors[e.target.name] = null;
            errorsFocus[e.target.name] = ''
          }
      } else {
        errors[e.target.name] = messages[this.state.language].formprofile2.ADDR_NO_PREVENT
        errorsFocus[e.target.name] = 'errorFocus'
      }
    }
    else if(e.target.name === 'ADDR_MOO') {
        const re = /^[0-9\b]+$/;
        if (re.test(e.target.value) !== false) {
          fields[e.target.name] = e.target.value;
          errors[e.target.name] = null;
          errorsFocus[e.target.name] = ''
        } else {
          if(e.target.value === '') { fields[e.target.name] = ''; }
            errors[e.target.name] = <FormattedMessage id="PREVENT_MOO_NO" />;
            errorsFocus[e.target.name] = ''
            errorsFocus["ADDR_MOO"] = 'errorFocus'
            e.preventDefault();
        }
      } 
      else {
        fields[e.target.name] = e.target.value;
        errors[e.target.name] = null;
        errorsFocus[e.target.name] = ''
      } 
    this.setState({ errors, fields, errorsFocus, [e.target.id]: e.target.value });
  }

  checkAddress(e){
    if((e.target.value.match(/-/g) || []).length > 1){
      return false
    }else{
      return true
    }
  }

  checkCharacter(e){
    var check = /^\d*[a-zA-Zก-ฮ][a-zA-Zก-ฮ\d\s]{1,10}$/;
    if(check.test(e.target.value) != false){
      return true
    }else{
      return false
    }
  }

  renderContent(type) {
    const { province } = this.state;
    var option = []
    var name_title;
    if (type === 'type_province') {
      name_title = province
    }

    for (var i in name_title) {
      option.push(<option key={i} value={name_title[i].codE_GUID}>{name_title[i].values}</option>)
    }

    return <select
      className={`form-control input_form ${this.state.errorsFocus['ADDR_ORG_PROVINCE_ID']}`}
      id='exampleFormControlSelect1'
      name="ADDR_ORG_PROVINCE_ID"
      value={this.state.fields['ADDR_ORG_PROVINCE_ID']}
      onChange={(e) => this.getAmphure(e.target.value, 'onchange')}
    >
      <option name="ADDR_ORG_PROVINCE_ID">{messages[this.state.language].formprofile2.title_province}</option>
      {option}
    </select>
  }

  render() {
    var { province, amphure, tumbon, postcode } = this.state;
    provinceListData = [];
    for (var i in province) {
      provinceListData.push(<option name="ADDR_ORG_AMPHURE_ID" key={i} value={province[i].codE_GUID} >{province[i].values}</option>)
    }

    amphureListData = [];
    for (var i in amphure) {
      amphureListData.push(<option name="ADDR_ORG_AMPHURE_ID" key={i} value={amphure[i].codE_GUID}>{amphure[i].values}</option>)
    }

    tumbonListData = [];
    for (var i in tumbon) {
      tumbonListData.push(<option name="ADDR_ORG_SUB_DISTRICT_ID" key={i} value={tumbon[i].codE_GUID}>{tumbon[i].values}</option>)
    }

    postcodeListData = [];
    for (var i in postcode) {
      postcodeListData.push(<option name="ADDR_ORG_POSTALCODE_ID" key={i} value={postcode[i].codE_GUID}>{postcode[i].values}</option>)
    }

    var formThaiPerson = <form className="form-horizontal" onSubmit={(e) => this.handleSubmit(e)}>
          <div className="bg_full">
          <SweetAlert
            show={this.state.show}
            title=""
            text={this.state.resultApi}
            onOutsideClick={() => this.setState({ show: false })}
          />
           <div id="loading" style={{ display: this.state.content_loading}}>
                  <div className="content_class">
                      <img src={`${process.env.PUBLIC_URL}/images/Loading.gif`} />
                  </div>
            </div>
            <div className="container-fluid">
              <div className="content_page" style={{ display: this.state.content_page}}>
                <div className="head_title_agree"><FormattedMessage id="header_address" /></div>
                <div className="form-group">
                  <span className="required">* </span><span className="label1"><FormattedMessage id="title_numaddress" /></span>
                  <input 
                    type="text" 
                    className={`form-control input_form ${this.state.errorsFocus['ADDR_ADDRESS']}`} 
                    id="exampleFormControlInput1" 
                    maxLength="10" 
                    name="ADDR_ADDRESS" 
                    onChange={(e) => this.handleChange(e)} 
                    value={this.state.fields['ADDR_ADDRESS']}
                    placeholder={messages[this.state.language].formprofile2.house_input} 
                  />
                  <div className="errorMsg">{this.state.errors['ADDR_ADDRESS']}</div>
                </div>
                <div className="form-group">
                  <label><FormattedMessage id="title_moo" /></label>
                  <input
                    type="text"
                    className={`form-control input_form ${this.state.errorsFocus['ADDR_MOO']}`}
                    maxLength="3"
                    id="exampleFormControlInput1"
                    name="ADDR_MOO"
                    onChange={(e) => this.handleChange(e)}
                    value={this.state.fields['ADDR_MOO']}
                    placeholder={messages[this.state.language].formprofile2.moo_input}
                  />
                  <div className="errorMsg">{this.state.errors['ADDR_MOO']}</div>
                </div>
                <div className="form-group">
                  <label><FormattedMessage id="title_addressbldg" /></label>
                  <input type="text" className="form-control input_form" id="exampleFormControlInput1" name="ADDR_VILLEGE" onChange={(e) => this.handleChange(e)} value={this.state.fields['ADDR_VILLEGE']} placeholder={messages[this.state.language].formprofile2.building_input} />
                </div>
                <div className="form-group">
                  <label><FormattedMessage id="title_lane" /></label>
                  <input type="text" className="form-control input_form" name="ADDR_SOI" onChange={(e) => this.handleChange(e)} value={this.state.fields.ADDR_SOI} id="exampleFormControlInput1" placeholder={messages[this.state.language].formprofile2.lane_input} />
                </div>
                <div className="form-group">
                  <label><FormattedMessage id="title_road" /></label>
                  <input type="text" className="form-control input_form" name="ADDR_STREET" onChange={(e) => this.handleChange(e)} value={this.state.fields.ADDR_STREET} id="exampleFormControlInput1" placeholder={messages[this.state.language].formprofile2.road_input} />
                </div>
                <div className="form-group">
                  <label><span className="required">* </span><FormattedMessage id="title_province" /></label>
                  <select
                    className='form-control'
                    id='exampleFormControlSelect1'
                    name="ADDR_ORG_PROVINCE_ID"
                    onChange={(e) => this.getAmphure(e.target.value, 'onchange')}
                    value={this.state.fields['ADDR_ORG_PROVINCE_ID'] == 0 ? this.state.fields['ADDR_ORG_PROVINCE_ID'] = '' : this.state.fields['ADDR_ORG_PROVINCE_ID']}
                  >
                    <option value="0">{messages[this.state.language].formprofile2.title_province}</option>
                  {provinceListData}
                  </select>
                  {/* {this.renderContent('type_province')} */}
                  <div className="errorMsg">{this.state.errors['ADDR_ORG_PROVINCE_ID']}</div>
                </div>
                <div className="form-group">
                  <label><span className="required">* </span><FormattedMessage id="title_area" /></label>
                  <select
                    className='form-control'
                    id='exampleFormControlSelect1'
                    name="ADDR_ORG_AMPHURE_ID"
                    onChange={(e) => this.getTumbon(e.target.value, 'onchange')}
                    value={this.state.fields['ADDR_ORG_AMPHURE_ID'] == 0 ? this.state.fields['ADDR_ORG_AMPHURE_ID'] = '' : this.state.fields['ADDR_ORG_AMPHURE_ID']}
                  >
                    <option value="0">{messages[this.state.language].formprofile2.title_area}</option>
                    {amphureListData}
                  </select>
                  <div className="errorMsg">{this.state.errors['ADDR_ORG_AMPHURE_ID']}</div>
                </div>
                <div className="form-group">
                  <label><span className="required">* </span><FormattedMessage id="title_subarea" /></label>
                  <select
                    className='form-control'
                    id='exampleFormControlSelect1'
                    name="ADDR_ORG_SUB_DISTRICT_ID"
                    onChange={(e) => this.getPostcode(e.target.value, 'onchange')}
                    value={this.state.fields['ADDR_ORG_SUB_DISTRICT_ID'] == 0 ? this.state.fields['ADDR_ORG_SUB_DISTRICT_ID'] = '' : this.state.fields['ADDR_ORG_SUB_DISTRICT_ID']}
                  >
                    <option value="0">{messages[this.state.language].formprofile2.title_subarea}</option>
                    {tumbonListData}
                  </select>
                  <div className="errorMsg">{this.state.errors['ADDR_ORG_SUB_DISTRICT_ID']}</div>
                </div>
                <div className="form-group">
                  <label><span className="required">* </span><FormattedMessage id="title_postcode" /></label>
                  <select 
                    className='form-control' 
                    id='exampleFormControlSelect1' 
                    name="ADDR_ORG_POSTALCODE_ID" 
                    onChange={(e) => this.handleChange(e)}
                    value={this.state.fields['ADDR_ORG_POSTALCODE_ID'] == 0 ? this.state.fields['ADDR_ORG_POSTALCODE_ID'] = '' : (this.state.fields['ADDR_ORG_POSTALCODE_ID'] !== '' ? this.state.fields['ADDR_ORG_POSTALCODE_ID'] : '')}
                  >=
                    <option value="0">{messages[this.state.language].formprofile2.title_postcode}</option>
                    {postcodeListData}
                  </select>
                  <div className="errorMsg">{this.state.errors['ADDR_ORG_POSTALCODE_ID']}</div>
                </div>
                <div className="form-group">
                  <label><FormattedMessage id="title_additional" /></label>
                  <textarea className="form-control input_form" id="exampleFormControlTextarea1" rows="3" name="ADDR_OTHER" value={this.state.fields.ADDR_OTHER} onChange={(e) => this.handleChange(e)} placeholder={messages[this.state.language].formprofile2.additional_input}></textarea>
                </div>
                  <div className="btn_nextstep">
                    <button 
                        type="submit" 
                        className="btn btn-secondary btn-block btn_agreement" 
                        // onClick={() => this.submitFinalForm()}
                    >
                        <FormattedMessage id="submitfinalform" />
                    </button>
                  </div>
                  {this.state.modal}
              </div>
            </div>
          </div>
        </form>

        var formForeigner = <form className="form-horizontal" onSubmit={(e) => this.handleSubmit(e)}><div className="bg_full">
        <div className="container-fluid"><div className="content_page"><div className="head_title_agree"><FormattedMessage id="header_address" /></div><div className="form-group"><label><FormattedMessage id="title_additional" /></label>
        <textarea className="form-control input_form" id="exampleFormControlTextarea1" rows="3" name="ADDR_OTHER" value={this.state.fields.ADDR_OTHER} onChange={(e) => this.handleChange(e)} placeholder={messages[this.state.language].formprofile2.additional_input}></textarea>
        </div>
        <div className="btn_nextstep">
        <button 
          type="submit" 
          className="btn btn-secondary btn-block btn_agreement"
          // onClick={() => this.submitFinalForm()}
        >
          <FormattedMessage id="button_nextstep" /></button>
        </div></div></div></div></form>

    return (
      <IntlProvider
        locale={this.state.language}
        messages={messages[this.state.language].formprofile2}
      >
        <div>
          {(this.state.fields.CARD_TYPE_ID === 2 ? formForeigner : formThaiPerson)}
        </div>
      </IntlProvider>
    )
  }
}
