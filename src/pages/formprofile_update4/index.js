import React from "react";
import { regisMaxCard } from "../../_actions/regiser_ptmaxcard"
import {
  IntlProvider,
  FormattedMessage,
  // FormattedHTMLMessage
} from "react-intl";
import { addLocaleData } from "react-intl";
import locale_en from "react-intl/locale-data/en";
import locale_th from "react-intl/locale-data/th";
import intlMessageEN from "./../../_translations/en.json";
import intlMessageTH from "./../../_translations/th.json";
import { API_LIST } from "../../_constants/matcher"

var disabled_block = 'form-control input_form disabled'
let SessionData = JSON.parse(sessionStorage.getItem('inputBody'))

addLocaleData([...locale_en, ...locale_th]);

  var messages = {
    th: intlMessageTH,
    en: intlMessageEN
  };

  sessionStorage.setItem('inputBody','{}')


class updateType1 extends React.Component {
 
  constructor(props) {
    super(props);
    this.state = {
      language: "th",
      disabled: true,
      fields: { 
        isChangeCartype: false,
      },
      errors: {},
      errorsFocus: { 
        PMT_OTHERS_REMARK: "",
        CAR_BRAND: "",
        OCCUPATION_ID: ""
      },
      content_page: "none",
      content_loading: "block",
      CAR_BRAND: 1, 
      CAR_BRAND_REMARK: 'none',
      OCCAPATION_REMARK: 'none',
      occupation: '',
      education: '',
      income: '',
      cartype: '',
      car_brand:'',
      carType: []
    };
  }

  componentDidMount() {
    let { fields} = this.state;
    // if(this.props.location.state.data) {
    //   let newObject = Object.assign(this.props.location.state.data , this.state.fields)
    //   fields = newObject
    //   this.setState({ fields })
    // }
    const params = this.props.match.params;
    var tokenData = this.props.location;
    var sstr = tokenData.search.search("&");
    var sub_memberid = tokenData.search.substr(sstr);
    var memberid = sub_memberid.substr(5);
    this.setState({ language: (params.lang ? params.lang.toString().toLowerCase() : API_LIST.defaultLang) });
    this.getDropdownlist_type("OCCUPATION").then(result => {this.setState({ occupation: result.dropdowN_INFO })})
    this.getDropdownlist_type("EDUCATION").then(result => {this.setState({ education: result.dropdowN_INFO })})
    this.getDropdownlist_type("INCOME").then(result => {this.setState({ income: result.dropdowN_INFO })})
    this.getDropdownlist_type("CAR_TYPE").then(result => {this.setState({ cartype: result.dropdowN_INFO })})
    this.getDropdownlist_type("CAR_BRAND").then(result => {this.setState({ car_brand: result.dropdowN_INFO })})

    // fields['OCCUPATION_ID'] = (JSON.parse(sessionStorage.getItem('inputBody')).OCCUPATION_ID != null ? JSON.parse(sessionStorage.getItem('inputBody')).OCCUPATION_ID : 0)
    // fields['CAR_TYPE'] = (JSON.parse(sessionStorage.getItem('inputBody')).CAR_TYPE != null ? JSON.parse(sessionStorage.getItem('inputBody')).CAR_TYPE : 0)
    // fields['CAR_BRAND'] = (JSON.parse(sessionStorage.getItem('inputBody')).CAR_BRAND != null ? JSON.parse(sessionStorage.getItem('inputBody')).CAR_BRAND : 0)
    // fields['EDUCATION_ID'] = (JSON.parse(sessionStorage.getItem('inputBody')).EDUCATION_ID != null ? JSON.parse(sessionStorage.getItem('inputBody')).EDUCATION_ID : 0)
    // fields['INCOME_ID'] = (JSON.parse(sessionStorage.getItem('inputBody')).INCOME_ID != null ? JSON.parse(sessionStorage.getItem('inputBody')).INCOME_ID : 0)

    // this.setState({ fields })

    sessionStorage.setItem('_usid' , memberid)
    sessionStorage.setItem('_tokenid' , params.tokenid)
    sessionStorage.setItem('_customerid' , params.customerid)
    sessionStorage.setItem('_cardmemberid' , params.cardmemberid)
    sessionStorage.setItem('_udid' , params.udid)
    sessionStorage.setItem('_deviceos' , params.deviceos)
    this.getCartype()
  }

  getCartype(){
    var userid = sessionStorage.getItem('_usid')
    var cardmemberid = sessionStorage.getItem('_cardmemberid')
    regisMaxCard.getCarcard(userid).then(e => {
      if(e.data.isSuccess === true){
        var dataCard = e.data.data
        for(var i in dataCard){
          if(cardmemberid === dataCard[i].carD_MEMBER_ID){
            var car_type = dataCard[i].carD_CAR_TYPE_ID
            var car_brand = dataCard[i].caR_BRAND
            var car_brand_remark = dataCard[i].caR_BRAND_REMARK
          }else{
            console.log(false)
          }
        } 
      }else{
        console.log(e)
      }
      this.setState ({ CAR_TYPE : car_type , CAR_BRAND : car_brand , CAR_BRAND_REMARK : car_brand_remark })
      this.updatetype1()
    })
  }

  updatetype1(){
    let { fields, CAR_BRAND_REMARK, OCCAPATION_REMARK , getToken } = this.state;
    var token = sessionStorage.getItem('_tokenid')
    var customer = sessionStorage.getItem('_customerid')
    var userid = sessionStorage.getItem('_usid')
    var content_page = ''
    var content_loading = ''

    regisMaxCard.updateProfileList(token,customer,userid).then(e => {
      var customerData = JSON.parse(e.data.data.data)
      var customerList = customerData.CUSTOMER_PROFILE_INFO[0]
      let newObject = Object.assign(customerList , this.state.fields)
      fields = newObject

      if(fields['CAR_BRAND'] == 9 || this.state.CAR_BRAND == 9){
        CAR_BRAND_REMARK = "block";
      }else{
        CAR_BRAND_REMARK = "none";
      }

      fields['CAR_TYPE'] = (JSON.parse(sessionStorage.getItem('inputBody')).CAR_TYPE != null ? JSON.parse(sessionStorage.getItem('inputBody')).CAR_TYPE : (this.state.CAR_TYPE != null ? this.state.CAR_TYPE  : 0)),
      fields['CAR_BRAND'] = (JSON.parse(sessionStorage.getItem('inputBody')).CAR_BRAND != null ? JSON.parse(sessionStorage.getItem('inputBody')).CAR_BRAND : (this.state.CAR_BRAND != null ? this.state.CAR_BRAND : 0)),
      fields['CAR_BRAND_REMARK'] = (JSON.parse(sessionStorage.getItem('inputBody')).CAR_BRAND_REMARK != null ? JSON.parse(sessionStorage.getItem('inputBody')).CAR_BRAND_REMARK : this.state.CAR_BRAND_REMARK),
      fields['OCCUPATION_ID'] = (JSON.parse(sessionStorage.getItem('inputBody')).OCCUPATION_ID != null ? JSON.parse(sessionStorage.getItem('inputBody')).OCCUPATION_ID : (fields["OCCUPATION_ID"] != null ? fields["OCCUPATION_ID"] : 0)),
      fields['OCCAPATION_REMARK'] = (JSON.parse(sessionStorage.getItem('inputBody')).OCCAPATION_REMARK != null ? JSON.parse(sessionStorage.getItem('inputBody')).OCCAPATION_REMARK : fields["OCCAPATION_REMARK"]),
      fields['EDUCATION_ID'] = (JSON.parse(sessionStorage.getItem('inputBody')).EDUCATION_ID != null ? JSON.parse(sessionStorage.getItem('inputBody')).EDUCATION_ID : (fields["EDUCATION_ID"]!= null ? fields["EDUCATION_ID"] : 0)),
      fields['INCOME_ID'] = (JSON.parse(sessionStorage.getItem('inputBody')).INCOME_ID != null ? JSON.parse(sessionStorage.getItem('inputBody')).INCOME_ID : (fields["INCOME_ID"] != null ? fields["INCOME_ID"] : 0)),
      fields['SUBSCRIBE_EMAIL'] = (JSON.parse(sessionStorage.getItem('inputBody')).SUBSCRIBE_EMAIL != null ? JSON.parse(sessionStorage.getItem('inputBody')).SUBSCRIBE_EMAIL : (fields["SUBSCRIBE_EMAIL"] === 1 ? true : false)),
      fields['SUBSCRIBE_LETTER'] = (JSON.parse(sessionStorage.getItem('inputBody')).SUBSCRIBE_LETTER != null ? JSON.parse(sessionStorage.getItem('inputBody')).SUBSCRIBE_LETTER : (fields["SUBSCRIBE_LETTER"] === 1 ? true : false)),
      fields['SUBSCRIBE_NONE'] = (JSON.parse(sessionStorage.getItem('inputBody')).SUBSCRIBE_NONE != null ? JSON.parse(sessionStorage.getItem('inputBody')).SUBSCRIBE_NONE : (fields["SUBSCRIBE_NONE"] === 1 ? true : false)),
      fields['SUBSCRIBE_SMS'] = (JSON.parse(sessionStorage.getItem('inputBody')).SUBSCRIBE_SMS != null ? JSON.parse(sessionStorage.getItem('inputBody')).SUBSCRIBE_SMS : (fields["SUBSCRIBE_SMS"] === 1 ? true : false)),
      fields['PMT_BEAUTY'] = (JSON.parse(sessionStorage.getItem('inputBody')).PMT_BEAUTY != null ? JSON.parse(sessionStorage.getItem('inputBody')).PMT_BEAUTY : (fields["PMT_BEAUTY"] === 1 ? true : false)),
      fields['PMT_FOOD'] = (JSON.parse(sessionStorage.getItem('inputBody')).PMT_FOOD != null ? JSON.parse(sessionStorage.getItem('inputBody')).PMT_FOOD : (fields["PMT_FOOD"] === 1 ? true : false)),
      fields['PMT_OTHERS'] = (JSON.parse(sessionStorage.getItem('inputBody')).PMT_OTHERS != null ? JSON.parse(sessionStorage.getItem('inputBody')).PMT_OTHERS : (fields["PMT_OTHERS"] === 1 ? true : false)),
      fields['PMT_OTHERS_REMARK'] = (JSON.parse(sessionStorage.getItem('inputBody')).PMT_OTHERS_REMARK != null ? JSON.parse(sessionStorage.getItem('inputBody')).PMT_OTHERS_REMARK : fields["PMT_OTHERS_REMARK"]),
      fields['PMT_SHOPPING'] = (JSON.parse(sessionStorage.getItem('inputBody')).PMT_SHOPPING != null ? JSON.parse(sessionStorage.getItem('inputBody')).PMT_SHOPPING : (fields["PMT_SHOPPING"] === 1 ? true : false)),
      fields['PMT_SPORT_AND_CAR'] = (JSON.parse(sessionStorage.getItem('inputBody')).PMT_SPORT_AND_CAR != null ? JSON.parse(sessionStorage.getItem('inputBody')).PMT_SPORT_AND_CAR : (fields["PMT_SPORT_AND_CAR"] === 1 ? true : false)),
      fields['PMT_TRAVEL'] = (JSON.parse(sessionStorage.getItem('inputBody')).PMT_TRAVEL != null ? JSON.parse(sessionStorage.getItem('inputBody')).PMT_TRAVEL : (fields["PMT_TRAVEL"] === 1 ? true : false)),
      fields['ACT_EN_MUSIC'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_EN_MUSIC != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_EN_MUSIC : (fields["ACT_EN_MUSIC"] === 1 ? true : false)),
      fields['ACT_EN_CONCERT'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_EN_CONCERT != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_EN_CONCERT : (fields["ACT_EN_CONCERT"] === 1 ? true : false)),
      fields['ACT_EN_COUNTRY_CONCERT'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_EN_COUNTRY_CONCERT != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_EN_COUNTRY_CONCERT : (fields["ACT_EN_COUNTRY_CONCERT"] === 1 ? true : false )),
      fields['ACT_EN_MOVIES'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_EN_MOVIES != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_EN_MOVIES : (fields["ACT_EN_MOVIES"] === 1 ? true : false)),
      fields['ACT_EN_THEATER'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_EN_THEATER != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_EN_THEATER : (fields["ACT_EN_THEATER"] === 1 ? true : false )),
      fields['ACT_EN_OTHERS'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_EN_OTHERS != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_EN_OTHERS : (fields["ACT_EN_OTHERS"] === 1 ? true : false)),
      fields['ACT_SPT_FITNESS'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_SPT_FITNESS != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_SPT_FITNESS : (fields["ACT_SPT_FITNESS"] === 1 ? true : false)),
      fields['ACT_SPT_RUN'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_SPT_RUN != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_SPT_RUN : (fields["ACT_SPT_RUN"] === 1 ? true : false )),
      fields['ACT_SPT_CYCLING'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_SPT_CYCLING != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_SPT_CYCLING : (fields["ACT_SPT_CYCLING"] === 1 ? true : false)),
      fields['ACT_SPT_FOOTBALL'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_SPT_FOOTBALL != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_SPT_FOOTBALL : (fields["ACT_SPT_FOOTBALL"] === 1 ? true : false)),
      fields['ACT_SPT_OTHERS'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_SPT_OTHERS != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_SPT_OTHERS : (fields["ACT_SPT_OTHERS"] === 1 ? true : false)),
      fields['ACT_OD_SHOPPING'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_OD_SHOPPING != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_OD_SHOPPING : (fields["ACT_OD_SHOPPING"] === 1 ? true : false )),
      fields['ACT_OD_BEAUTY'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_OD_BEAUTY != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_OD_BEAUTY : (fields["ACT_OD_BEAUTY"] === 1 ? true : false)),
      fields['ACT_OD_RESTAURANT'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_OD_RESTAURANT != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_OD_RESTAURANT : (fields["ACT_OD_RESTAURANT"] === 1 ? true : false)),
      fields['ACT_OD_CLUB'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_OD_CLUB != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_OD_CLUB : (fields["ACT_OD_CLUB"] === 1 ? true : false)),
      fields['ACT_OD_MOMCHILD'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_OD_MOMCHILD != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_OD_MOMCHILD : (fields["ACT_OD_MOMCHILD"] === 1 ? true : false)),
      fields['ACT_OD_RALLY'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_OD_RALLY != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_OD_RALLY : (fields["ACT_OD_RALLY"] === 1 ? true : false )),
      fields['ACT_OD_ADVANTURE'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_OD_ADVANTURE != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_OD_ADVANTURE : (fields["ACT_OD_ADVANTURE"] === 1 ? true : false )),
      fields['ACT_OD_PHOTO'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_OD_PHOTO != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_OD_PHOTO : (fields["ACT_OD_PHOTO"] === 1 ? true : false )),
      fields['ACT_OD_PHILANTHROPY'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_OD_PHILANTHROPY != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_OD_PHILANTHROPY : (fields["ACT_OD_PHILANTHROPY"] === 1 ? true : false )),
      fields['ACT_OD_HOROSCOPE'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_OD_HOROSCOPE != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_OD_HOROSCOPE : (fields["ACT_OD_HOROSCOPE"] === 1 ? true : false )),
      fields['ACT_OD_ABROAD'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_OD_ABROAD != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_OD_ABROAD : (fields["ACT_OD_ABROAD"] === 1 ? true : false )),
      fields['ACT_OD_UPCOUNTRY'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_OD_UPCOUNTRY != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_OD_UPCOUNTRY : (fields["ACT_OD_UPCOUNTRY"] === 1 ? true : false )),
      fields['ACT_OD_OTHERS'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_OD_OTHERS != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_OD_OTHERS : (fields["ACT_OD_OTHERS"] === 1 ? true : false )),
      fields['ACT_ID_COOKING'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_ID_COOKING != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_ID_COOKING : (fields["ACT_ID_COOKING"] === 1 ? true : false )),
      fields['ACT_ID_BAKING'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_ID_BAKING != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_ID_BAKING : (fields["ACT_ID_BAKING"] === 1 ? true : false )),
      fields['ACT_ID_DECORATE'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_ID_DECORATE != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_ID_DECORATE : (fields["ACT_ID_DECORATE"] === 1 ? true : false )),
      fields['ACT_ID_GARDENING'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_ID_GARDENING != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_ID_GARDENING : (fields["ACT_ID_GARDENING"] === 1 ? true : false )),
      fields['ACT_ID_READING'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_ID_READING != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_ID_READING : (fields["ACT_ID_READING"] === 1 ? true : false )),
      fields['ACT_ID_GAMING'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_ID_GAMING != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_ID_GAMING : (fields["ACT_ID_GAMING"] === 1 ? true : false )),
      fields['ACT_ID_INTERNET'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_ID_INTERNET != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_ID_INTERNET : (fields["ACT_ID_INTERNET"] === 1 ? true : false)),
      fields['ACT_ID_OTHERS'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_ID_OTHERS != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_ID_OTHERS : (fields["ACT_ID_OTHERS"] === 1 ? true : false)),
      fields['ACT_HNGOUT_FRIEND'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_HNGOUT_FRIEND != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_HNGOUT_FRIEND : (fields["ACT_HNGOUT_FRIEND"] === 1 ? true : false )),
      fields['ACT_HNGOUT_COUPLE'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_HNGOUT_COUPLE != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_HNGOUT_COUPLE : (fields["ACT_HNGOUT_COUPLE"] === 1 ? true : false )),
      fields['ACT_HNGOUT_FAMILY'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_HNGOUT_FAMILY != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_HNGOUT_FAMILY : (fields["ACT_HNGOUT_FAMILY"] === 1 ? true : false )),
      fields['ACT_HNGOUT_NO'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_HNGOUT_NO != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_HNGOUT_NO : (fields["ACT_HNGOUT_NO"] === 1 ? true : false )),
      fields['ACT_EN_OTHERS_REMARK'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_EN_OTHERS_REMARK != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_EN_OTHERS_REMARK : fields["ACT_EN_OTHERS_REMARK"] ),
      fields['ACT_SPT_OTHERS_REMARK'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_SPT_OTHERS_REMARK != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_SPT_OTHERS_REMARK : fields["ACT_SPT_OTHERS_REMARK"] ),
      fields['ACT_OD_OTHERS_REMARK'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_OD_OTHERS_REMARK != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_OD_OTHERS_REMARK : fields["ACT_OD_OTHERS_REMARK"] ),
      fields['ACT_ID_OTHERS_REMARK'] = (JSON.parse(sessionStorage.getItem('inputBody')).ACT_ID_OTHERS_REMARK != null ? JSON.parse(sessionStorage.getItem('inputBody')).ACT_ID_OTHERS_REMARK : fields["ACT_ID_OTHERS_REMARK"])

      if (fields.CAR_BRAND === 9) {
        CAR_BRAND_REMARK = "block";
      }
      if (fields.OCCUPATION_ID === 5) {
        OCCAPATION_REMARK = "block";
      }
      
      this.setState({ fields, CAR_BRAND_REMARK, OCCAPATION_REMARK , getToken : e.data.data.tokenId })

     if (e.data.errMsg === "Success" ) {
        content_loading = "none";
        content_page = "block";
     } else {
        content_loading = "block";
        content_page = "none";
     }
     
     var th = this
     setTimeout(function(){
      th.setState({content_loading: content_loading})
      th.setState({content_page: content_page})
     }, 1000);

    })
  }

  handleSubmit(event) {
    event.preventDefault();
    sessionStorage.setItem('inputBody', JSON.stringify(this.state.fields))
    if (this.validateForm()) {
      this.props.history.push({
        pathname: `${process.env.PUBLIC_URL}/update_formtype2/${this.state.language}/${sessionStorage.getItem('_tokenid')}/${sessionStorage.getItem('_customerid')}/${sessionStorage.getItem('_cardmemberid')}`,
        state: {
          userid:sessionStorage.getItem('_usid'),
          data: this.state.fields,
          changeCartype: this.state.fields.isChangeCartype,
          tokenGet : this.state.getToken
        }
      });
    } else {
      console.log('formsubmit ' + false);
    }
  }

  validateForm() {

    let fields = this.state.fields;
    let errors = {};
    let errorsFocus = {};
    let formIsValid = true;

    // if (!fields["CAR_TYPE"]) {
    //   formIsValid = false;
    //   errorsFocus["CAR_TYPE"] = 'errorFocus'
    //   errors["CAR_TYPE"] = <FormattedMessage id="SELECT_TYPECAR" />
    // }
    // if (!fields["CAR_BRAND"]) {
    //   formIsValid = false;
    //   errorsFocus["CAR_BRAND"] = 'errorFocus'
    //   errors["CAR_BRAND"] = <FormattedMessage id="SELECT_TYPEBRAND" />
    // }
    // if(fields["CAR_BRAND"] === 9) {
    //   if (!fields["CAR_BRAND_REMARK"]) {
    //     formIsValid = false;
    //     errorsFocus["CAR_BRAND_REMARK"] = 'errorFocus'
    //     errors["CAR_BRAND_REMARK"] = (
    //       <FormattedMessage id="TITLE_REMARK_CAR" />
    //     );
    //   }
    // }
    // if(fields["OCCUPATION_ID"] === 5) {
    //   if (!fields["OCCAPATION_REMARK"]) {
    //     formIsValid = false;
    //     errorsFocus["OCCAPATION_REMARK"] = 'errorFocus'
    //     errors["OCCAPATION_REMARK"] = (
    //       <FormattedMessage id="TITLE_REMARK_CAREER" />
    //     );
    //   }
    // }
    // if (!fields["OCCUPATION_ID"]) {
    //   formIsValid = false;
    //   errorsFocus["OCCUPATION_ID"] = 'errorFocus'
    //   errors["OCCUPATION_ID"] = <FormattedMessage id="SELECT_TYPECAREER" />
    // }
    // if (!fields["EDUCATION_ID"]) {
    //   formIsValid = false;
    //   errorsFocus["EDUCATION_ID"] = 'errorFocus'
    //   errors["EDUCATION_ID"] = <FormattedMessage id="SELECT_TYPEEDUCATION" />
    // }
    // if (!fields["INCOME_ID"]) {
    //   formIsValid = false;
    //   errorsFocus["INCOME_ID"] = 'errorFocus'
    //   errors["INCOME_ID"] = <FormattedMessage id="SELECT_TYPEINCOME" />
    // }
   
     this.setState({
      errors: errors,
      errorsFocus: errorsFocus
    });
    return formIsValid;
  }

  handleGameClik(ev) {
    let { fields } = this.state;
    if(ev.target.name === "promotion") {
      if (ev.target.id === "PMT_OTHERS") {
        if(ev.target.checked === true) {
          disabled_block = 'form-control input_form'
          fields[ev.target.id] = ev.target.checked
        } else {
          disabled_block = 'form-control input_form disabled'
          fields['PMT_OTHERS_REMARK'] = ''
          fields[ev.target.id] = ev.target.checked
        }
      } else {
        if(fields['PMT_OTHERS'] === true) {
          disabled_block = 'form-control input_form'
        } else {
          disabled_block = 'form-control input_form disabled'
          fields['PMT_OTHERS_REMARK'] = ''
        }
        fields[ev.target.id] = ev.target.checked
      }
      this.setState( {disabled: (ev.target.checked === "" ? false : true)} )
    } else {
      fields[ev.target.id] = ev.target.value
    }
      this.setState({ fields })
  } 
  
  dropdownlistCheck(e) {
    let { errors, errorsFocus ,fields, CAR_BRAND_REMARK, OCCAPATION_REMARK } = this.state;
    if(e.target.name === "CAR_TYPE"){
      if(e.target.value != 0){
        this.state.fields.isChangeCartype = true
      }else{
        this.state.fields.isChangeCartype = false
      }
    }
    if(e.target.name === "CAR_BRAND") {
      if(e.target.value != 0){
        this.state.fields.isChangeCartype = true
      }else{
        this.state.fields.isChangeCartype = false
      }
      if(e.target.value === '9'){
        CAR_BRAND_REMARK = 'block'
      } else {
        CAR_BRAND_REMARK = 'none'
        fields["CAR_BRAND_REMARK"] = '';
        errors["CAR_BRAND_REMARK"]  = null;
        errorsFocus["CAR_BRAND_REMARK"]  = ''
      }
      errors[e.target.name] = null;
      fields[e.target.name] = parseInt(e.target.value)
      errorsFocus[e.target.name] = ''
    }else if(e.target.name === "OCCUPATION_ID") {
      if(e.target.value === '5'){
        OCCAPATION_REMARK = 'block'
      } else {
        OCCAPATION_REMARK = 'none'
        fields["OCCAPATION_REMARK"] = '';
        errors["OCCAPATION_REMARK"]  = null;
        errorsFocus["OCCAPATION_REMARK"]  = ''
      }
      errors[e.target.name] = null;
      fields[e.target.name] = parseInt(e.target.value)
      errorsFocus[e.target.name] = ''
    } else {
        fields[e.target.name] = e.target.value;
        errors[e.target.name] = null;
        errorsFocus[e.target.name] = ''
      }
      this.setState({ errors, errorsFocus, fields, CAR_BRAND_REMARK, OCCAPATION_REMARK})
  }

  checboxClick(ev) {
    let { fields } = this.state;
      if(ev.target.name === "news") {
        fields[ev.target.id] = ev.target.checked
        if (ev.target.id === "SUBSCRIBE_NONE") {
          if(ev.target.checked === true) {
            fields['SUBSCRIBE_EMAIL'] = false
            fields['SUBSCRIBE_LETTER'] = false
            fields['SUBSCRIBE_SMS'] = false
          }
        } else {
          fields['SUBSCRIBE_NONE'] = false
        }
      } else {
        fields[ev.target.id] = ev.target.value
      }

    this.setState({ fields })
}


  getDropdownlist_type(title) {
    return regisMaxCard.getDropdownlist(title)
  }

  renderContent(type) {
    const { occupation, education, income, cartype, car_brand} = this.state;
    var option = []
    var name_title;
    if (type === 'type_cartype') {
      name_title = cartype 
    } else if (type === 'type_brand') {
        name_title = car_brand 
    } else if (type === 'type_occupation') {
          name_title = occupation 
    } else if (type === 'type_education'){
      name_title = education 
    } else {
      name_title = income 
    }
    for(var i in name_title) {
      option.push(<option key={i} value={name_title[i].code}>{name_title[i].values}</option>)
    }
    return option
    }

  render() {
    return (
      <IntlProvider
      locale={this.state.language}
      messages={messages[this.state.language].formprofile4}
      >

    <form className="form-horizontal formprofile_4" onSubmit={e => this.handleSubmit(e)}>
      <div className="bg_full">
       <div id="loading" style={{ display: this.state.content_loading}}>
            <div className="content_class">
                <img src={`${process.env.PUBLIC_URL}/images/Loading.gif`} />
            </div>
        </div>
        <div className="container-fluid">
        <div className="content_page" style={{ display: this.state.content_page}}>
            <div className="form-group">
              <label>
                <FormattedMessage id="title_typecars" />
              </label>
              <select className={`form-control ${this.state.errorsFocus['CAR_TYPE']}`}
                    name="CAR_TYPE" 
                    id='exampleFormControlSelect1'
                    value={this.state.fields['CAR_TYPE']}
                    onChange={(e) => this.dropdownlistCheck(e)}>
                    <option value="0">
                    {messages[this.state.language].formprofile4.SELECT_TYPECAR}
                    </option>
                        {this.renderContent('type_cartype')}
              </select>
              <div className="errorMsg CAR_TYPE">
                    {this.state.errors["CAR_TYPE"]}
              </div>
              {/* <FormattedHTMLMessage id="title_typecarslist" /> */}
            </div>
             <div className="form-group" isChangeCartype={true}>
             <label>
               <FormattedMessage id="title_brand" />
            </label>
              <select className={`form-control ${this.state.errorsFocus['CAR_BRAND']}`}
                      name="CAR_BRAND" 
                      value={this.state.fields['CAR_BRAND']}
                      id='exampleFormControlSelect1' 
                      onChange={(e) => this.dropdownlistCheck(e)}>
                      <option value="0">
                      {messages[this.state.language].formprofile4.SELECT_TYPEBRAND}
                      </option>
                          {this.renderContent('type_brand')}
              </select>
                <div className="errorMsg CAR_BRAND">
                    {this.state.errors['CAR_BRAND']}
                </div>
              <div className="form-group" style={{ display: this.state.CAR_BRAND_REMARK}}>
              <input
                      type="text"
                      className={`form-control ${this.state.errorsFocus['CAR_BRAND_REMARK']} input_form`}
                      name="CAR_BRAND_REMARK"
                      value={this.state.fields['CAR_BRAND_REMARK']}
                      onChange={e => this.dropdownlistCheck(e)}
                      id="exampleFormControlInput1"
                      placeholder={ messages[this.state.language].formprofile4.TITLE_REMARK_CAR}
                    />
                    <div className="errorMsg CAR_BRAND_REMARK">
                    {this.state.errors['CAR_BRAND_REMARK']}
                    </div>
              </div>
              {/* <label><FormattedMessage id="title_brand" /></label>
              {this.renderContent('type_brand')} */}
              {/* <FormattedHTMLMessage id="title_brandlist" /> */}
            </div>
            <div className="form-group">
              <label><FormattedMessage id="title_career" /></label>
              <select className={`form-control ${this.state.errorsFocus['OCCUPATION_ID']}`}
                      name="OCCUPATION_ID" 
                      value={this.state.fields['OCCUPATION_ID']}
                      id='exampleFormControlSelect1' 
                      onChange={(e) => this.dropdownlistCheck(e)}>
                      <option value="0" >
                      {messages[this.state.language].formprofile4.SELECT_TYPECAREER}
                      </option>
                          {this.renderContent('type_occupation')}
              </select>
              <div className="errorMsg OCCUPATION_ID">
                    {this.state.errors['OCCUPATION_ID']}
                </div>
              <div className="form-group" style={{ display: this.state.OCCAPATION_REMARK}}>
              <input
                      type="text"
                      className={`form-control ${this.state.errorsFocus['OCCAPATION_REMARK']} input_form`}
                      name="OCCAPATION_REMARK"
                      value={this.state.fields['OCCAPATION_REMARK']}
                      onChange={e => this.dropdownlistCheck(e)}
                      id="exampleFormControlInput1"
                      placeholder={ messages[this.state.language].formprofile4 .TITLE_REMARK_CAREER}
                    />
                     <div className="errorMsg OCCAPATION_REMARK">
                    {this.state.errors['OCCAPATION_REMARK']}
                    </div> 
              </div>
              {/* <FormattedHTMLMessage id="title_careerlist" /> */}
            </div>
            <div className="form-group">
              <label><FormattedMessage id="title_education" /></label>
              <select className={`form-control ${this.state.errorsFocus['EDUCATION_ID']}`}
                        name="EDUCATION_ID" 
                        value={this.state.fields['EDUCATION_ID']}
                        id='exampleFormControlSelect1' 
                        onChange={(e) => this.dropdownlistCheck(e)}>
              <option value="0">{messages[this.state.language].formprofile4.SELECT_TYPEEDUCATION}</option>
              {this.renderContent('type_education')}
              </select>
              <div className="errorMsg EDUCATION_ID">
                    {this.state.errors["EDUCATION_ID"]}
              </div>
              {/* <FormattedHTMLMessage id="title_educationlist" /> */}
            </div>
            <div className="form-group">
              <label><FormattedMessage id="title_income" /></label>
              <select  className={`form-control ${this.state.errorsFocus['INCOME_ID']}`}
                        name="INCOME_ID" 
                        value={this.state.fields['INCOME_ID']}
                        id='exampleFormControlSelect1' 
                        onChange={(e) => this.dropdownlistCheck(e)}>
              <option value="0">{messages[this.state.language].formprofile4.SELECT_TYPEINCOME}</option>
              {this.renderContent('type_income')}
              </select>
              <div className="errorMsg INCOME_ID">
                    {this.state.errors["INCOME_ID"]}
              </div>
              {/* <FormattedHTMLMessage id="title_incomelist" /> */}
            </div>
            {/* <label><FormattedMessage id="title_remindnews" /></label>
            <div className="row">
                <div className="col">
                <label className="check_conditions_select"><FormattedMessage id="title_listnews" />
                  <input type="checkbox" id="SUBSCRIBE_LETTER" name="news"  onChange = {(e) => this.checboxClick(e)} checked={(this.state.fields['SUBSCRIBE_LETTER']) ? (this.state.fields['SUBSCRIBE_LETTER'] === true || this.state.fields['SUBSCRIBE_NONE'] != true ? true : false) : false} />
                  <span className="checkmark_condition_select"></span>
                </label>
                </div>
                <div className="col">
                <label className="check_conditions_select"><FormattedMessage id="title_listemail" />
                  <input type="checkbox" id="SUBSCRIBE_EMAIL" name="news" onChange = {(e) => this.checboxClick(e)} checked={(this.state.fields['SUBSCRIBE_EMAIL']) ? (this.state.fields['SUBSCRIBE_EMAIL'] === true || this.state.fields['SUBSCRIBE_NONE'] != true ? true : false) : false} />
                  <span className="checkmark_condition_select"></span>
                </label>
                </div>
                <div className="w-100"></div>
                <div className="col">
                <label className="check_conditions_select"><FormattedMessage id="title_listsms" />
                  <input type="checkbox" id="SUBSCRIBE_SMS" name="news" onChange = {(e) => this.checboxClick(e)} checked={(this.state.fields['SUBSCRIBE_SMS']) ? (this.state.fields['SUBSCRIBE_SMS'] === true || this.state.fields['SUBSCRIBE_NONE'] != true ? true : false) : false} />
                  <span className="checkmark_condition_select"></span>
                </label>
                </div>
                <div className="col">
                <label className="check_conditions_select"><FormattedMessage id="title_listnonesms" />
                  <input type="checkbox" id="SUBSCRIBE_NONE" name="news" onChange = {(e) => this.checboxClick(e)} checked={(this.state.fields['SUBSCRIBE_NONE'] === true ? true : false)} />
                  <span className="checkmark_condition_select"></span>
                </label>
                </div>
            </div> */}
            <label><FormattedMessage id="title_remindpromotion" /></label>
            <div className="row">
                <div className="col">
                <label className="selectoneonly"><FormattedMessage id="title_listfood" />
                  <input type="checkbox" id="PMT_FOOD" checked={this.state.fields['PMT_FOOD']} name="promotion" onClick = {(e) => this.handleGameClik(e)} />
                  <span className="checkmark_selectoneonly"></span>
                </label>
                </div>
                <div className="col">
                <label className="selectoneonly"><FormattedMessage id="title_listravel" />
                  <input type="checkbox" id="PMT_TRAVEL" checked={this.state.fields['PMT_TRAVEL']} name="promotion" onClick = {(e) => this.handleGameClik(e)} />
                  <span className="checkmark_selectoneonly"></span>
                </label>
                </div>
                <div className="w-100"></div>
                <div className="col">
                <label className="selectoneonly"><FormattedMessage id="title_listshopping" />
                  <input type="checkbox" id="PMT_SHOPPING" checked={this.state.fields['PMT_SHOPPING']} name="promotion" onClick = {(e) => this.handleGameClik(e)} />
                  <span className="checkmark_selectoneonly"></span>
                </label>
                </div>
                <div className="col">
                <label className="selectoneonly"><FormattedMessage id="title_listbeauty" />
                  <input type="checkbox" id="PMT_BEAUTY" checked={this.state.fields['PMT_BEAUTY']} name="promotion" onClick = {(e) => this.handleGameClik(e)} />
                  <span className="checkmark_selectoneonly"></span>
                </label>
                </div>
                <div className="w-100"></div>
                <div className="col">
                <label className="selectoneonly"><FormattedMessage id="title_listcars" />
                  <input type="checkbox" id="PMT_SPORT_AND_CAR" checked={this.state.fields['PMT_SPORT_AND_CAR']} name="promotion" onClick = {(e) => this.handleGameClik(e)} />
                  <span className="checkmark_selectoneonly"></span>
                </label>
                </div>
                <div className="w-100"></div>
                <div className="col">
                <label className="selectoneonly"><FormattedMessage id="title_listother" />
                  <input type="checkbox" id="PMT_OTHERS" checked={this.state.fields['PMT_OTHERS']} name="promotion" onClick = {(e) => this.handleGameClik(e)} />
                  <span className="checkmark_selectoneonly"></span>
                </label>
                <div className="form-group">
                  <input type="text" id="PMT_OTHERS_REMARK" value={this.state.fields['PMT_OTHERS_REMARK']} className={disabled_block} 
                  placeholder={messages[this.state.language].formprofile4.etc_input} value={this.state.fields['PMT_OTHERS_REMARK']}  onChange={(e) => this.handleGameClik(e)} />
                </div>
                </div>
            </div>
              <div className="">
                  <button type="submit" className="btn btn-secondary btn-block btn_agreement"><FormattedMessage id="button_nextstep" /></button>
              </div>
          </div>
        </div>
      </div>
      </form>
      </IntlProvider>
    )
  }
}

export default updateType1;
