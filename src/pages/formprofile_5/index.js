import React from "react";
import "./formprofile_5.css";
import {
  IntlProvider,
  FormattedMessage,
  // FormattedHTMLMessage
} from "react-intl";
import { addLocaleData } from "react-intl";
import locale_en from "react-intl/locale-data/en";
import locale_th from "react-intl/locale-data/th";
import intlMessageEN from "./../../_translations/en.json";
import intlMessageTH from "./../../_translations/th.json";
import { API_LIST } from "../../_constants/matcher"
import { regisMaxCard } from "../../_actions/regiser_ptmaxcard";
import SweetAlert from "react-bootstrap-sweetalert";
var crypto = require('crypto');

var disabled_block = 'form-control input_form disabled'
var sport_remark_display = 'form-control input_form disabled'
var outside_remark_display = 'form-control input_form disabled'
var inside_remark_display = 'form-control input_form disabled'

addLocaleData([...locale_en, ...locale_th]);

  var messages = {
    th: intlMessageTH,
    en: intlMessageEN
  };


class Formprofile_5 extends React.Component {
  
  constructor(props) {
    super(props);
    this.state = {
      language: "th",
      disabled: true,
      disabled_sport: true,
      disabled_outside: true,
      disabled_inside: true,
      disabledBtn: '',
      resultApi: '',
      modalIsOpen: false,
      show: false,
      modal: null,
      fields: { 
        ACT_EN_MUSIC: (JSON.parse(localStorage.getItem('inputBody')).ACT_EN_MUSIC != null ? JSON.parse(localStorage.getItem('inputBody')).ACT_EN_MUSIC : false),
        ACT_EN_CONCERT: (JSON.parse(localStorage.getItem('inputBody')).ACT_EN_CONCERT != null ? JSON.parse(localStorage.getItem('inputBody')).ACT_EN_CONCERT : false),
        ACT_EN_COUNTRY_CONCERT: (JSON.parse(localStorage.getItem('inputBody')).ACT_EN_COUNTRY_CONCERT != null ? JSON.parse(localStorage.getItem('inputBody')).ACT_EN_COUNTRY_CONCERT : false),
        ACT_EN_MOVIES: (JSON.parse(localStorage.getItem('inputBody')).ACT_EN_MOVIES != null ? JSON.parse(localStorage.getItem('inputBody')).ACT_EN_MOVIES : false),
        ACT_EN_THEATER: (JSON.parse(localStorage.getItem('inputBody')).ACT_EN_THEATER != null ? JSON.parse(localStorage.getItem('inputBody')).ACT_EN_THEATER : false),
        ACT_EN_OTHERS: (JSON.parse(localStorage.getItem('inputBody')).ACT_EN_OTHERS != null ? JSON.parse(localStorage.getItem('inputBody')).ACT_EN_OTHERS : false),
        ACT_SPT_FITNESS: (JSON.parse(localStorage.getItem('inputBody')).ACT_SPT_FITNESS != null ? JSON.parse(localStorage.getItem('inputBody')).ACT_SPT_FITNESS : false),
        ACT_SPT_RUN: (JSON.parse(localStorage.getItem('inputBody')).ACT_SPT_RUN != null ? JSON.parse(localStorage.getItem('inputBody')).ACT_SPT_RUN : false),
        ACT_SPT_CYCLING: (JSON.parse(localStorage.getItem('inputBody')).ACT_SPT_CYCLING != null ? JSON.parse(localStorage.getItem('inputBody')).ACT_SPT_CYCLING : false),
        ACT_SPT_FOOTBALL: (JSON.parse(localStorage.getItem('inputBody')).ACT_SPT_FOOTBALL != null ? JSON.parse(localStorage.getItem('inputBody')).ACT_SPT_FOOTBALL : false),
        ACT_SPT_OTHERS: (JSON.parse(localStorage.getItem('inputBody')).ACT_SPT_OTHERS != null ? JSON.parse(localStorage.getItem('inputBody')).ACT_SPT_OTHERS : false),
        ACT_OD_SHOPPING: (JSON.parse(localStorage.getItem('inputBody')).ACT_OD_SHOPPING != null ? JSON.parse(localStorage.getItem('inputBody')).ACT_OD_SHOPPING : false),
        ACT_OD_BEAUTY: (JSON.parse(localStorage.getItem('inputBody')).ACT_OD_BEAUTY != null ? JSON.parse(localStorage.getItem('inputBody')).ACT_OD_BEAUTY : false),
        ACT_OD_RESTAURANT: (JSON.parse(localStorage.getItem('inputBody')).ACT_OD_RESTAURANT != null ? JSON.parse(localStorage.getItem('inputBody')).ACT_OD_RESTAURANT : false),
        ACT_OD_CLUB: (JSON.parse(localStorage.getItem('inputBody')).ACT_OD_CLUB != null ? JSON.parse(localStorage.getItem('inputBody')).ACT_OD_CLUB : false),
        ACT_OD_MOMCHILD: (JSON.parse(localStorage.getItem('inputBody')).ACT_OD_MOMCHILD != null ? JSON.parse(localStorage.getItem('inputBody')).ACT_OD_MOMCHILD : false),
        ACT_OD_RALLY: (JSON.parse(localStorage.getItem('inputBody')).ACT_OD_RALLY != null ? JSON.parse(localStorage.getItem('inputBody')).ACT_OD_RALLY : false),
        ACT_OD_ADVANTURE: (JSON.parse(localStorage.getItem('inputBody')).ACT_OD_ADVANTURE != null ? JSON.parse(localStorage.getItem('inputBody')).ACT_OD_ADVANTURE : false),
        ACT_OD_PHOTO: (JSON.parse(localStorage.getItem('inputBody')).ACT_OD_PHOTO != null ? JSON.parse(localStorage.getItem('inputBody')).ACT_OD_PHOTO : false),
        ACT_OD_PHILANTHROPY: (JSON.parse(localStorage.getItem('inputBody')).ACT_OD_PHILANTHROPY != null ? JSON.parse(localStorage.getItem('inputBody')).ACT_OD_PHILANTHROPY : false),
        ACT_OD_HOROSCOPE: (JSON.parse(localStorage.getItem('inputBody')).ACT_OD_HOROSCOPE != null ? JSON.parse(localStorage.getItem('inputBody')).ACT_OD_HOROSCOPE : false),
        ACT_OD_ABROAD: (JSON.parse(localStorage.getItem('inputBody')).ACT_OD_ABROAD != null ? JSON.parse(localStorage.getItem('inputBody')).ACT_OD_ABROAD : false),
        ACT_OD_UPCOUNTRY: (JSON.parse(localStorage.getItem('inputBody')).ACT_OD_UPCOUNTRY != null ? JSON.parse(localStorage.getItem('inputBody')).ACT_OD_UPCOUNTRY : false),
        ACT_OD_OTHERS: (JSON.parse(localStorage.getItem('inputBody')).ACT_OD_OTHERS != null ? JSON.parse(localStorage.getItem('inputBody')).ACT_OD_OTHERS : false),
        ACT_ID_COOKING: (JSON.parse(localStorage.getItem('inputBody')).ACT_ID_COOKING != null ? JSON.parse(localStorage.getItem('inputBody')).ACT_ID_COOKING : false),
        ACT_ID_BAKING: (JSON.parse(localStorage.getItem('inputBody')).ACT_ID_BAKING != null ? JSON.parse(localStorage.getItem('inputBody')).ACT_ID_BAKING : false),
        ACT_ID_DECORATE: (JSON.parse(localStorage.getItem('inputBody')).ACT_ID_DECORATE != null ? JSON.parse(localStorage.getItem('inputBody')).ACT_ID_DECORATE : false),
        ACT_ID_GARDENING: (JSON.parse(localStorage.getItem('inputBody')).ACT_ID_GARDENING != null ? JSON.parse(localStorage.getItem('inputBody')).ACT_ID_GARDENING : false),
        ACT_ID_READING: (JSON.parse(localStorage.getItem('inputBody')).ACT_ID_READING != null ? JSON.parse(localStorage.getItem('inputBody')).ACT_ID_READING : false),
        ACT_ID_GAMING: (JSON.parse(localStorage.getItem('inputBody')).ACT_ID_GAMING != null ? JSON.parse(localStorage.getItem('inputBody')).ACT_ID_GAMING : false),
        ACT_ID_INTERNET: (JSON.parse(localStorage.getItem('inputBody')).ACT_ID_INTERNET != null ? JSON.parse(localStorage.getItem('inputBody')).ACT_ID_INTERNET : false),
        ACT_ID_OTHERS: (JSON.parse(localStorage.getItem('inputBody')).ACT_ID_OTHERS != null ? JSON.parse(localStorage.getItem('inputBody')).ACT_ID_OTHERS : false),
        ACT_HNGOUT_FRIEND: (JSON.parse(localStorage.getItem('inputBody')).ACT_HNGOUT_FRIEND != null ? JSON.parse(localStorage.getItem('inputBody')).ACT_HNGOUT_FRIEND : false),
        ACT_HNGOUT_COUPLE: (JSON.parse(localStorage.getItem('inputBody')).ACT_HNGOUT_COUPLE != null ? JSON.parse(localStorage.getItem('inputBody')).ACT_HNGOUT_COUPLE : false),
        ACT_HNGOUT_FAMILY: (JSON.parse(localStorage.getItem('inputBody')).ACT_HNGOUT_FAMILY != null ? JSON.parse(localStorage.getItem('inputBody')).ACT_HNGOUT_FAMILY : false),
        ACT_HNGOUT_NO: (JSON.parse(localStorage.getItem('inputBody')).ACT_HNGOUT_NO != null ? JSON.parse(localStorage.getItem('inputBody')).ACT_HNGOUT_NO : false),
        ACT_EN_OTHERS_REMARK: (JSON.parse(localStorage.getItem('inputBody')).ACT_EN_OTHERS_REMARK != null ? JSON.parse(localStorage.getItem('inputBody')).ACT_EN_OTHERS_REMARK : ''),
        ACT_SPT_OTHERS_REMARK: (JSON.parse(localStorage.getItem('inputBody')).ACT_SPT_OTHERS_REMARK != null ? JSON.parse(localStorage.getItem('inputBody')).ACT_SPT_OTHERS_REMARK : ''),
        ACT_OD_OTHERS_REMARK: (JSON.parse(localStorage.getItem('inputBody')).ACT_OD_OTHERS_REMARK != null ? JSON.parse(localStorage.getItem('inputBody')).ACT_OD_OTHERS_REMARK : ''),
        ACT_ID_OTHERS_REMARK: (JSON.parse(localStorage.getItem('inputBody')).ACT_ID_OTHERS_REMARK != null ? JSON.parse(localStorage.getItem('inputBody')).ACT_ID_OTHERS_REMARK : '')
      },  
    };
    this.openModal = this.openModal.bind(this);
    this.afterOpenModal = this.afterOpenModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
  }
  

  openModal() {
    this.setState({modalIsOpen: true});
  }

  afterOpenModal() {
    // references are now sync'd and can be accessed.
    this.subtitle.style.color = '#f00';
  }

  closeModal() {
    this.setState({modalIsOpen: false});
  }

  componentDidMount() {
    sessionStorage.setItem('test' , true);
    if(this.props.location.state.data) {
      let newObject = Object.assign(this.props.location.state.data , this.state.fields)
      this.setState({ fields: newObject })
    }
    const params = this.props.match.params;
    this.setState({ language: (params.lang ? params.lang.toString().toLowerCase() : API_LIST.defaultLang) });
  }

  handleGameClik(ev) {
    let { fields } = this.state;
    if(ev.target.name === "entertain") {
      if (ev.target.id === "ACT_EN_OTHERS") {
        if(ev.target.checked === true) {
          disabled_block = 'form-control input_form'
          fields[ev.target.id] = ev.target.checked
        } else {
          disabled_block = 'form-control input_form disabled'
          fields['ACT_EN_OTHERS_REMARK'] = ''
          fields[ev.target.id] = ev.target.checked
        }
        // disabled_block = 'form-control input_form'
      } else {
        if(fields['ACT_EN_OTHERS'] === true) {
          disabled_block = 'form-control input_form'
        } else {
          disabled_block = 'form-control input_form disabled'
          fields['ACT_EN_OTHERS_REMARK'] = ''
        }
        fields[ev.target.id] = ev.target.checked
      }
      this.setState( {disabled: (ev.target.checked === "" ? false : true)} )
    } 
      else if(ev.target.name === "sport") {
        if (ev.target.id === "ACT_SPT_OTHERS") {
          if(ev.target.checked === true) {
            sport_remark_display = 'form-control input_form'
            fields[ev.target.id] = ev.target.checked
          } else {
            sport_remark_display = 'form-control input_form disabled'
            fields['ACT_SPT_OTHERS_REMARK'] = ''
            fields[ev.target.id] = ev.target.checked
          }
        } else {
          if (fields['ACT_SPT_OTHERS'] === true) {
            sport_remark_display = 'form-control input_form'
          } else {
            sport_remark_display = 'form-control input_form disabled'
            fields['ACT_SPT_OTHERS_REMARK'] = ''
          }
          fields[ev.target.id] = ev.target.checked
        }
      this.setState( {disabled_sport: (ev.target.checked === "" ? false : true)} )
    } else if(ev.target.name === "outside") {
        if (ev.target.id === "ACT_OD_OTHERS") {
          if(ev.target.checked === true) {
            outside_remark_display = 'form-control input_form'
            fields[ev.target.id] = ev.target.checked
          } else {
            outside_remark_display = 'form-control input_form disabled'
            fields['ACT_OD_OTHERS_REMARK'] = ''
            fields[ev.target.id] = ev.target.checked
          }
        } else {
          if (fields['ACT_OD_OTHERS'] === true){
            outside_remark_display = 'form-control input_form'
          } else {
            outside_remark_display = 'form-control input_form disabled'
            fields['ACT_OD_OTHERS_REMARK'] = ''
          }
          fields[ev.target.id] = ev.target.checked
        }  
      this.setState( {disabled_outside: (ev.target.checked === "" ? false : true)} )
    } else if(ev.target.name === "inside") {
      if (ev.target.id === "ACT_ID_OTHERS") {
        if(ev.target.checked === true) {
          inside_remark_display = 'form-control input_form'
          fields[ev.target.id] = ev.target.checked
        } else {
          inside_remark_display = 'form-control input_form disabled'
          fields['ACT_ID_OTHERS_REMARK'] = ''
          fields[ev.target.id] = ev.target.checked
        }
      } else {
        if ( fields['ACT_ID_OTHERS'] === true) {
          inside_remark_display = 'form-control input_form'
        } else {
          inside_remark_display = 'form-control input_form disabled'
          fields['ACT_ID_OTHERS_REMARK'] = ''
        }
       fields[ev.target.id] = ev.target.checked
      }  
      this.setState( {disabled_inside: (ev.target.checked === "" ? false : true)} )
    } else if(ev.target.name === "inside") { 
      fields['ACT_HNGOUT_FRIEND'] = false
      fields['ACT_HNGOUT_COUPLE'] = false
      fields['ACT_HNGOUT_FAMILY'] = false
      fields['ACT_HNGOUT_NO'] = false
      fields[ev.target.id] = ev.target.checked
    } else {
      fields[ev.target.id] = ev.target.value
    }
  
      this.setState({ fields })
  } 

  modalCheckSubmit(res,img){
    alert = (
      <SweetAlert
        custom
        confirmBtnBsStyle="success"
        closeOnClickOutside={true}
        cancelBtnBsStyle="default"
        focusConfirmBtn={false}
        title=""
        customIcon={img}
        showConfirm={false}
        showCancelButton
        onCancel={() => this.handleChoice(false)}
        onConfirm={() => this.handleChoice(true)}
        onOutsideClick={() => {
          this.wasOutsideClick = true;
          this.setState({ showConfirm: false })
        }}
      >
        {res}
      </SweetAlert>
    );

    this.setState({ show: true , modal: alert })
  }

  handleChoice(choice) {
    if (choice === false) {
      this.setState({ show: false , modal: null })
    }
  }

  submitForm() {
    this.setState({ disabledBtn: 'disabled' })
    localStorage.setItem('inputBody', JSON.stringify(this.state.fields))
    this.submitFinalForm()
  }

  submitFinalForm() {
    var pdpaJson = 
      {
        "CUSTOMER_ID": "",
        "UPDATE_BY": "API",
        "CONSENT_INFO": [
          {
            "CONSENT_MASTER_ID": 16,
            "CONSENT_NAME": "ส่งข้อมูลข่าวสาร การโฆษณาที่เกี่ยวกับสินค้า หรือบริการ",
            "CONSENT_VERSION": 4,
            "IS_CHECK": this.state.fields.pdpacon1
          },
          {
            "CONSENT_MASTER_ID": 17,
            "CONSENT_NAME": "เสนอโปรโมชั่นสินค้าและบริการ โดยตรงถึงสมาชิก",
            "CONSENT_VERSION": 4,
            "IS_CHECK": this.state.fields.pdpacon2
          },
          {
            "CONSENT_MASTER_ID": 18,
            "CONSENT_NAME": "เชิญชวนเข้าร่วมกิจกรรมต่างๆ ที่สมาชิกได้ระบุความสนใจหรือความชื่นชอบในขณะสมัครสมาชิก",
            "CONSENT_VERSION": 4,
            "IS_CHECK": this.state.fields.pdpacon3
          },
              {
            "CONSENT_MASTER_ID": 19,
            "CONSENT_NAME": "วิเคราะห์การใช้สินค้าหรือบริการของสมาชิก เพื่อจัดทำโปรโมชั่น หรือส่วนลดในสินค้าหรือบริการ",
            "CONSENT_VERSION": 4,
            "IS_CHECK": this.state.fields.pdpacon4
          }
        ]
      }
      var resString = JSON.stringify(pdpaJson);
      const ENC_KEY = "kHyT9QwZX5nkPCx5Gs1NNiliCon@2019"; // set random encryption key
      const IV = "xRtyUw5Pt+58ZxqA"; // set random initialisation vector
      let cipher = crypto.createCipheriv('aes-256-cbc', Buffer.from(ENC_KEY), IV);
      let encrypted = cipher.update(resString);
      encrypted = Buffer.concat([encrypted, cipher.final()]);
      var en = encrypted.toString('base64');
      console.log()
    regisMaxCard.activateAndRegister(this.state.fields).then(e => {
      console.log(this.state.fields)
      if(e.data.isSuccess === false) {
        this.setState({ disableAgreement: false })
        this.setState({ resultApi: e.data.errMsg })
        this.setState({ disabledBtn: '' })
        var imgPopup = `${process.env.PUBLIC_URL}/images/cancel.png`;
        this.modalCheckSubmit(this.state.resultApi,imgPopup)
      } else {
        this.setState({ resultApi: e.data.errMsg });
        window.location.href=`PTG:||Register||sucess||phonenumber=${this.props.location.state.data.PHONE_NO}?data=${en}`;
      }
    })
  }


  checboxClick(ev) {
    let { fields } = this.state;
      if(ev.target.name === "activities") {
        fields[ev.target.id] = ev.target.checked
        if (ev.target.id === "ACT_HNGOUT_NO") {
          if(ev.target.checked === true) {
            fields['ACT_HNGOUT_FRIEND'] = false
            fields['ACT_HNGOUT_COUPLE'] = false
            fields['ACT_HNGOUT_FAMILY'] = false
          }
        } else {
          fields['ACT_HNGOUT_NO'] = false
        }
      } else {
        fields[ev.target.id] = ev.target.value
      }
    this.setState({ fields })
}

  render() {
    return (
      <IntlProvider
      locale={this.state.language}
      defaultLocale={this.state.language}
      messages={messages[this.state.language].formprofile5}
      >

     
      <div className="bg_full">
      <SweetAlert
          show={this.state.show}
          title=""
          text={this.state.resultApi}
          onOutsideClick={() => this.setState({ show: false })}
        />
        <div className="container-fluid">
        <div className="content_page">
            <label ><FormattedMessage id="title_typecars" /></label>
            <div className="label1">{messages[this.state.language].formprofile5.title_entertainment}</div>
            <div className="row">
                <div className="col">
                <label className="selectoneonly"><FormattedMessage id="title_listmusic" />
                  <input type="checkbox" id="ACT_EN_MUSIC" checked={this.state.fields['ACT_EN_MUSIC']} name="entertain" onClick = {(e) => this.handleGameClik(e)} />
                  <span className="checkmark_selectoneonly"></span>
                </label>
                </div>
                <div className="col">
                <label className="selectoneonly"><FormattedMessage id="title_listconcert" />
                  <input type="checkbox" id="ACT_EN_CONCERT" checked={this.state.fields['ACT_EN_CONCERT']} name="entertain" onClick = {(e) => this.handleGameClik(e)} />
                  <span className="checkmark_selectoneonly"></span>
                </label>
                </div>
                <div className="w-100"></div>
                <div className="col">
                <label className="selectoneonly"><FormattedMessage id="title_listcontry_concert" />
                  <input type="checkbox" id="ACT_EN_COUNTRY_CONCERT" checked={this.state.fields['ACT_EN_COUNTRY_CONCERT']} name="entertain" onClick = {(e) => this.handleGameClik(e)} />
                  <span className="checkmark_selectoneonly"></span>
                </label>
                </div>
                <div className="col">
                <label className="selectoneonly"><FormattedMessage id="title_listmovie" />
                  <input type="checkbox" id="ACT_EN_MOVIES" checked={this.state.fields['ACT_EN_MOVIES']} name="entertain" onClick = {(e) => this.handleGameClik(e)} />
                  <span className="checkmark_selectoneonly"></span>
                </label>
                </div>
                <div className="w-100"></div>
                <div className="col">
                <label className="selectoneonly"><FormattedMessage id="title_liststage_play" />
                  <input type="checkbox" id="ACT_EN_THEATER" checked={this.state.fields['ACT_EN_THEATER']} name="entertain" onClick = {(e) => this.handleGameClik(e)} />
                  <span className="checkmark_selectoneonly"></span>
                </label>
                </div>
                <div className="w-100"></div>
                <div className="col">
                <label className="selectoneonly"><FormattedMessage id="title_listother" />
                  <input type="checkbox" id="ACT_EN_OTHERS" checked={this.state.fields['ACT_EN_OTHERS']} name="entertain" onClick = {(e) => this.handleGameClik(e)} />
                  <span className="checkmark_selectoneonly"></span>
                </label>
                <div className="form-group">
                <input type="text" id="ACT_EN_OTHERS_REMARK" className={disabled_block} 
                  placeholder={messages[this.state.language].formprofile5.etc_input} value={this.state.fields['ACT_EN_OTHERS_REMARK']}  onChange={(e) => this.handleGameClik(e)} />
                </div>
                </div>
            </div>

            <label ><FormattedMessage id="title_typesport" /></label>
            <div className="row">
                <div className="col">
                <label className="selectoneonly"><FormattedMessage id="title_listfitness" />
                  <input type="checkbox" id="ACT_SPT_FITNESS" checked={this.state.fields['ACT_SPT_FITNESS']} name="sport" onClick = {(e) => this.handleGameClik(e)} />
                  <span className="checkmark_selectoneonly"></span>
                </label>
                </div>
                <div className="col">
                <label className="selectoneonly"><FormattedMessage id="title_listrun" />
                  <input type="checkbox" id="ACT_SPT_RUN" checked={this.state.fields['ACT_SPT_RUN']} name="sport" onClick = {(e) => this.handleGameClik(e)} />
                  <span className="checkmark_selectoneonly"></span>
                </label>
                </div>
                <div className="w-100"></div>
                <div className="col">
                <label className="selectoneonly"><FormattedMessage id="title_bicycle" />
                  <input type="checkbox" id="ACT_SPT_CYCLING" checked={this.state.fields['ACT_SPT_CYCLING']} name="sport" onClick = {(e) => this.handleGameClik(e)} />
                  <span className="checkmark_selectoneonly"></span>
                </label>
                </div>
                <div className="col">
                <label className="selectoneonly"><FormattedMessage id="title_football" />
                  <input type="checkbox" id="ACT_SPT_FOOTBALL" checked={this.state.fields['ACT_SPT_FOOTBALL']} name="sport" onClick = {(e) => this.handleGameClik(e)} />
                  <span className="checkmark_selectoneonly"></span>
                </label>
                </div>
                <div className="w-100"></div>
                <div className="col">
                <label className="selectoneonly"><FormattedMessage id="title_listother" />
                  <input type="checkbox" id="ACT_SPT_OTHERS" checked={this.state.fields['ACT_SPT_OTHERS']} name="sport"  onClick = {(e) => this.handleGameClik(e)} />
                  <span className="checkmark_selectoneonly"></span>
                </label>
                <div className="form-group">
                <input type="text" id="ACT_SPT_OTHERS_REMARK" className={sport_remark_display} 
                  placeholder={messages[this.state.language].formprofile5.etc_input} value={this.state.fields['ACT_SPT_OTHERS_REMARK']}  onChange={(e) => this.handleGameClik(e)} />
                </div>
                </div>
  
            </div>


            <label ><FormattedMessage id="title_listoutside"/></label>
            <div className="row">
                <div className="col">
                <label className="selectoneonly"><FormattedMessage id="title_listshopping" />
                  <input type="checkbox" id="ACT_OD_SHOPPING" checked={this.state.fields['ACT_OD_SHOPPING']} name="outside" onClick = {(e) => this.handleGameClik(e)} />
                  <span className="checkmark_selectoneonly"></span>
                </label>
                </div>
                <div className="col">
                <label className="selectoneonly"><FormattedMessage id="title_listbeauty" />
                  <input type="checkbox" id="ACT_OD_BEAUTY" checked={this.state.fields['ACT_OD_BEAUTY']} name="outside" onClick = {(e) => this.handleGameClik(e)} />
                  <span className="checkmark_selectoneonly"></span>
                </label>
                </div>
                <div className="w-100"></div>
                <div className="col">
                <label className="selectoneonly"><FormattedMessage id="title_listrestaurant" />
                  <input type="checkbox" id="ACT_OD_RESTAURANT" checked={this.state.fields['ACT_OD_RESTAURANT']} name="outside" onClick = {(e) => this.handleGameClik(e)} />
                  <span className="checkmark_selectoneonly"></span>
                </label>
                </div>
                <div className="col">
                <label className="selectoneonly"><FormattedMessage id="title_listpub" />
                  <input type="checkbox" id="ACT_OD_CLUB" checked={this.state.fields['ACT_OD_CLUB']} name="outside" onClick = {(e) => this.handleGameClik(e)} />
                  <span className="checkmark_selectoneonly"></span>
                </label>
                </div>
                <div className="w-100"></div>
                <div className="col">
                <label className="selectoneonly"><FormattedMessage id="title_listmom" />
                  <input type="checkbox" id="ACT_OD_MOMCHILD" checked={this.state.fields['ACT_OD_MOMCHILD']} name="outside" onClick = {(e) => this.handleGameClik(e)} />
                  <span className="checkmark_selectoneonly"></span>
                </label>
                </div>
                <div className="col">
                <label className="selectoneonly"><FormattedMessage id="title_listrally" />
                  <input type="checkbox" id="ACT_OD_RALLY" checked={this.state.fields['ACT_OD_RALLY']} name="outside" onClick = {(e) => this.handleGameClik(e)} />
                  <span className="checkmark_selectoneonly"></span>
                </label>
                </div>
                <div className="w-100"></div>
                <div className="col">
                <label className="selectoneonly"><FormattedMessage id="title_listadventure" />
                  <input type="checkbox" id="ACT_OD_ADVANTURE" checked={this.state.fields['ACT_OD_ADVANTURE']} name="outside" onClick = {(e) => this.handleGameClik(e)} />
                  <span className="checkmark_selectoneonly"></span>
                </label>
                </div>
                <div className="col">
                <label className="selectoneonly"><FormattedMessage id="title_listtakephoto" />
                  <input type="checkbox" id="ACT_OD_PHOTO" checked={this.state.fields['ACT_OD_PHOTO']} name="outside" onClick = {(e) => this.handleGameClik(e)} />
                  <span className="checkmark_selectoneonly"></span>
                </label>
                </div>
                <div className="w-100"></div>
                <div className="col">
                <label className="selectoneonly"><FormattedMessage id="title_listmakemerit" />
                  <input type="checkbox" id="ACT_OD_PHILANTHROPY" checked={this.state.fields['ACT_OD_PHILANTHROPY']} name="outside" onClick = {(e) => this.handleGameClik(e)} />
                  <span className="checkmark_selectoneonly"></span>
                </label>
                </div>
                <div className="col">
                <label className="selectoneonly"><FormattedMessage id="title_listhoroscope" />
                  <input type="checkbox" id="ACT_OD_HOROSCOPE" checked={this.state.fields['ACT_OD_HOROSCOPE']} name="outside" onClick = {(e) => this.handleGameClik(e)} />
                  <span className="checkmark_selectoneonly"></span>
                </label>
                </div>
                <div className="w-100"></div>
                <div className="col">
                <label className="selectoneonly"><FormattedMessage id="title_listtravelout" />
                  <input type="checkbox" id="ACT_OD_ABROAD" checked={this.state.fields['ACT_OD_ABROAD']} name="outside" onClick = {(e) => this.handleGameClik(e)} />
                  <span className="checkmark_selectoneonly"></span>
                </label>
                </div>
                <div className="col">
                <label className="selectoneonly"><FormattedMessage id="title_listtravelin" />
                  <input type="checkbox" id="ACT_OD_UPCOUNTRY" checked={this.state.fields['ACT_OD_UPCOUNTRY']} name="outside" onClick = {(e) => this.handleGameClik(e)} />
                  <span className="checkmark_selectoneonly"></span>
                </label>
                </div>
                <div className="w-100"></div>
                <div className="col">
                <label className="selectoneonly"><FormattedMessage id="title_listother" />
                  <input type="checkbox" id="ACT_OD_OTHERS" checked={this.state.fields['ACT_OD_OTHERS']} name="outside" onClick = {(e) => this.handleGameClik(e)} />
                  <span className="checkmark_selectoneonly"></span>
                </label>
                <div className="form-group">
                  <input type="text" id="ACT_OD_OTHERS_REMARK" className={outside_remark_display} 
                    placeholder={messages[this.state.language].formprofile5.etc_input} value={this.state.fields['ACT_OD_OTHERS_REMARK']}  onChange={(e) => this.handleGameClik(e)} />
                </div>
                </div>
            </div>
            <label><FormattedMessage id="title_listin_house" /></label>
            <div className="row">
                <div className="col">
                <label className="selectoneonly"><FormattedMessage id="title_listcook" />
                  <input type="checkbox"  id="ACT_ID_COOKING" checked={this.state.fields['ACT_ID_COOKING']} name="inside" onClick = {(e) => this.handleGameClik(e)} />
                  <span className="checkmark_selectoneonly"></span>
                </label>
                </div>
                <div className="col">
                <label className="selectoneonly"><FormattedMessage id="title_listpastries" />
                  <input type="checkbox"  id="ACT_ID_BAKING" checked={this.state.fields['ACT_ID_BAKING']} name="inside" onClick = {(e) => this.handleGameClik(e)} />
                  <span className="checkmark_selectoneonly"></span>
                </label>
                </div>
                <div className="w-100"></div>
                <div className="col">
                <label className="selectoneonly"><FormattedMessage id="title_listdecorate" />
                  <input type="checkbox"  id="ACT_ID_DECORATE" checked={this.state.fields['ACT_ID_DECORATE']} name="inside" onClick = {(e) => this.handleGameClik(e)} />
                  <span className="checkmark_selectoneonly"></span>
                </label>
                </div>
                <div className="col">
                <label className="selectoneonly"><FormattedMessage id="title_listgardening" />
                  <input type="checkbox"  id="ACT_ID_GARDENING" checked={this.state.fields['ACT_ID_GARDENING']} name="inside" onClick = {(e) => this.handleGameClik(e)} />
                  <span className="checkmark_selectoneonly"></span>
                </label>
                </div>
                <div className="w-100"></div>
                <div className="col">
                <label className="selectoneonly"><FormattedMessage id="title_listread" />
                  <input type="checkbox"  id="ACT_ID_READING" checked={this.state.fields['ACT_ID_READING']} name="inside" onClick = {(e) => this.handleGameClik(e)} />
                  <span className="checkmark_selectoneonly"></span>
                </label>
                </div>
                <div className="col">
                <label className="selectoneonly"><FormattedMessage id="title_listplaygame" />
                  <input type="checkbox"  id="ACT_ID_GAMING" checked={this.state.fields['ACT_ID_GAMING']} name="inside" onClick = {(e) => this.handleGameClik(e)} />
                  <span className="checkmark_selectoneonly"></span>
                </label>
                </div>
                <div className="w-100"></div>
                <div className="col">
                <label className="selectoneonly"><FormattedMessage id="title_listinternet" />
                  <input type="checkbox"  id="ACT_ID_INTERNET" checked={this.state.fields['ACT_ID_INTERNET']} name="inside" onClick = {(e) => this.handleGameClik(e)} />
                  <span className="checkmark_selectoneonly"></span>
                </label>
                </div>
                <div className="w-100"></div>
                <div className="col">
                <label className="selectoneonly"><FormattedMessage id="title_listother" />
                  <input type="checkbox"  id="ACT_ID_OTHERS" checked={this.state.fields['ACT_ID_OTHERS']} name="inside" value="inside_other" onClick = {(e) => this.handleGameClik(e)} />
                  <span className="checkmark_selectoneonly"></span>
                </label>
                <div className="form-group">
                <input type="text" id="ACT_ID_OTHERS_REMARK" className={inside_remark_display} 
                    placeholder={messages[this.state.language].formprofile5.etc_input} value={this.state.fields['ACT_ID_OTHERS_REMARK']}  onChange={(e) => this.handleGameClik(e)} />
                </div>
                </div>
            </div>
            <label ><FormattedMessage id="title_listyourinterested" /></label>
            <div className="row">
                <div className="col">
                <label className="check_conditions_select"><FormattedMessage id="title_listactivitieswithfriend" />
                  <input type="checkbox"  id="ACT_HNGOUT_FRIEND" name="activities"  onChange = {(e) => this.checboxClick(e)} checked={(this.state.fields['ACT_HNGOUT_FRIEND']) ? (this.state.fields['ACT_HNGOUT_FRIEND'] === true || this.state.fields['ACT_HNGOUT_NO'] != true ? true : false) : false}  />
                  <span className="checkmark_condition_select"></span>
                </label>
                </div>
                <div className="col">
                <label className="check_conditions_select"><FormattedMessage id="title_listactivitieswithcouple" />
                  <input type="checkbox"  id="ACT_HNGOUT_COUPLE" name="activities"  onChange = {(e) => this.checboxClick(e)} checked={(this.state.fields['ACT_HNGOUT_COUPLE']) ? (this.state.fields['ACT_HNGOUT_COUPLE'] === true || this.state.fields['ACT_HNGOUT_NO'] != true ? true : false) : false} />
                  <span className="checkmark_condition_select"></span>
                </label>
                </div>
                <div className="w-100"></div>
                <div className="col">
                <label className="check_conditions_select"><FormattedMessage id="title_listactivitieswithfamily" />
                  <input type="checkbox"  id="ACT_HNGOUT_FAMILY" name="activities"  onChange = {(e) => this.checboxClick(e)} checked={(this.state.fields['ACT_HNGOUT_FAMILY']) ? (this.state.fields['ACT_HNGOUT_FAMILY'] === true || this.state.fields['ACT_HNGOUT_NO'] != true ? true : false) : false} />
                  <span className="checkmark_condition_select"></span>
                </label>
                </div>
                <div className="col">
                <label className="check_conditions_select"><FormattedMessage id="title_listactivitiesnone" />
                  <input type="checkbox"  id="ACT_HNGOUT_NO" name="activities"   onChange = {(e) => this.checboxClick(e)} checked={(this.state.fields['ACT_HNGOUT_NO'] === true ? true : false)} />
                  <span className="checkmark_condition_select"></span>
                </label>
                </div>
            </div>
            
             {/* <a href={link}> */}
             {/* <h4>{this.state.resultApi}</h4> */}
              <div className="">
              <button type="button" className={`btn btn-secondary btn-block btn_agreement ${this.state.disabledBtn}`} onClick={() => this.submitForm()}>{(this.state.disabledBtn === 'disabled' ? <i className="fa fa-circle-o-notch fa-spin" style={{ marginRight: "10px",marginTop: "5px" }}/> : '')}<FormattedMessage id="button_nextstep" /></button>
              </div>
             {/* </a> */}
            {this.state.modal}
          </div>
        </div>
      </div>
      </IntlProvider>
    )
  }
}

export default Formprofile_5;
