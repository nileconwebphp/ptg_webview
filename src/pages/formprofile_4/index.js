import React from "react";
import { regisMaxCard } from "../../_actions/regiser_ptmaxcard"
// import { NavLink } from "react-router-dom";
import "./formprofile_4.css";
import {
  IntlProvider,
  FormattedMessage,
  // FormattedHTMLMessage
} from "react-intl";
import { addLocaleData } from "react-intl";
import locale_en from "react-intl/locale-data/en";
import locale_th from "react-intl/locale-data/th";
import intlMessageEN from "./../../_translations/en.json";
import intlMessageTH from "./../../_translations/th.json";
import { API_LIST } from "../../_constants/matcher"

var disabled_block = 'form-control input_form disabled'
let SessionData = JSON.parse(localStorage.getItem('inputBody'))

addLocaleData([...locale_en, ...locale_th]);

  var messages = {
    th: intlMessageTH,
    en: intlMessageEN
  };


class Formprofile_4 extends React.Component {
 
  constructor(props) {
    super(props);
    this.state = {
      language: "th",
      disabled: true,
      fields: { 
        OCCUPATION_ID: (JSON.parse(localStorage.getItem('inputBody')).OCCUPATION_ID != null ? JSON.parse(localStorage.getItem('inputBody')).OCCUPATION_ID : 0),
        CAR_TYPE: (JSON.parse(localStorage.getItem('inputBody')).CAR_TYPE != null ? JSON.parse(localStorage.getItem('inputBody')).CAR_TYPE : 0),
        CAR_BRAND: (JSON.parse(localStorage.getItem('inputBody')).CAR_BRAND != null ? JSON.parse(localStorage.getItem('inputBody')).CAR_BRAND : 0),
        CAR_BRAND_REMARK: (JSON.parse(localStorage.getItem('inputBody')).CAR_BRAND_REMARK != null ? JSON.parse(localStorage.getItem('inputBody')).CAR_BRAND_REMARK : ""),
        OCCUPATION_ID: (JSON.parse(localStorage.getItem('inputBody')).OCCUPATION_ID != null ? JSON.parse(localStorage.getItem('inputBody')).OCCUPATION_ID : 0),
        OCCAPATION_REMARK: (JSON.parse(localStorage.getItem('inputBody')).OCCAPATION_REMARK != null ? JSON.parse(localStorage.getItem('inputBody')).OCCAPATION_REMARK : ""),
        EDUCATION_ID: (JSON.parse(localStorage.getItem('inputBody')).EDUCATION_ID != null ? JSON.parse(localStorage.getItem('inputBody')).EDUCATION_ID : 0),
        INCOME_ID: (JSON.parse(localStorage.getItem('inputBody')).INCOME_ID != null ? JSON.parse(localStorage.getItem('inputBody')).INCOME_ID : 0),
        SUBSCRIBE_EMAIL: (JSON.parse(localStorage.getItem('inputBody')).SUBSCRIBE_EMAIL != null ? JSON.parse(localStorage.getItem('inputBody')).SUBSCRIBE_EMAIL : false),
        SUBSCRIBE_LETTER: (JSON.parse(localStorage.getItem('inputBody')).SUBSCRIBE_LETTER != null ? JSON.parse(localStorage.getItem('inputBody')).SUBSCRIBE_LETTER : false),
        SUBSCRIBE_SMS: (JSON.parse(localStorage.getItem('inputBody')).SUBSCRIBE_SMS != null ? JSON.parse(localStorage.getItem('inputBody')).SUBSCRIBE_SMS : true),
        SUBSCRIBE_NONE: (JSON.parse(localStorage.getItem('inputBody')).SUBSCRIBE_NONE != null ? JSON.parse(localStorage.getItem('inputBody')).SUBSCRIBE_NONE : false),
        PMT_FOOD: (JSON.parse(localStorage.getItem('inputBody')).PMT_FOOD != null ? JSON.parse(localStorage.getItem('inputBody')).PMT_FOOD : false),
        PMT_TRAVEL: (JSON.parse(localStorage.getItem('inputBody')).PMT_TRAVEL != null ? JSON.parse(localStorage.getItem('inputBody')).PMT_TRAVEL : false),
        PMT_SHOPPING: (JSON.parse(localStorage.getItem('inputBody')).PMT_SHOPPING != null ? JSON.parse(localStorage.getItem('inputBody')).PMT_SHOPPING : false),
        PMT_BEAUTY: (JSON.parse(localStorage.getItem('inputBody')).PMT_BEAUTY != null ? JSON.parse(localStorage.getItem('inputBody')).PMT_BEAUTY : false),
        PMT_SPORT_AND_CAR: (JSON.parse(localStorage.getItem('inputBody')).PMT_SPORT_AND_CAR != null ? JSON.parse(localStorage.getItem('inputBody')).PMT_SPORT_AND_CAR : false),
        PMT_OTHERS: (JSON.parse(localStorage.getItem('inputBody')).PMT_OTHERS != null ? JSON.parse(localStorage.getItem('inputBody')).PMT_OTHERS : false),
        PMT_OTHERS_REMARK: (JSON.parse(localStorage.getItem('inputBody')).PMT_OTHERS_REMARK != null ? JSON.parse(localStorage.getItem('inputBody')).PMT_OTHERS_REMARK : ''),
      },
      errors: {},
      errorsFocus: { 
        PMT_OTHERS_REMARK: "",
        CAR_BRAND: "",
        OCCUPATION_ID: ""
      },
      CAR_BRAND: 1, 
      CAR_BRAND_REMARK: 'none',
      OCCAPATION_REMARK: 'none',
      occupation: '',
      education: '',
      income: '',
      cartype: '',
      car_brand:''
    };
  }

  componentDidMount() {
    sessionStorage.setItem('test' , true);
    let { fields, CAR_BRAND_REMARK, OCCAPATION_REMARK } = this.state;
    if(this.props.location.state.data) {
      let newObject = Object.assign(this.props.location.state.data , this.state.fields)
      fields = newObject
      if (fields.CAR_BRAND === 9) {
        CAR_BRAND_REMARK = "block";
      }
      if (fields.OCCUPATION_ID === 5) {
        OCCAPATION_REMARK = "block";
      }
      this.setState({ fields })
    }
    const params = this.props.match.params;
    this.setState({ language: (params.lang ? params.lang.toString().toLowerCase() : API_LIST.defaultLang) });
    this.getDropdownlist_type("OCCUPATION").then(result => {this.setState({ occupation: result.dropdowN_INFO })})
    this.getDropdownlist_type("EDUCATION").then(result => {this.setState({ education: result.dropdowN_INFO })})
    this.getDropdownlist_type("INCOME").then(result => {this.setState({ income: result.dropdowN_INFO })})
    this.getDropdownlist_type("CAR_TYPE").then(result => {this.setState({ cartype: result.dropdowN_INFO })})
    this.getDropdownlist_type("CAR_BRAND").then(result => {this.setState({ car_brand: result.dropdowN_INFO })})

    fields['OCCUPATION_ID'] = (JSON.parse(localStorage.getItem('inputBody')).OCCUPATION_ID != null ? JSON.parse(localStorage.getItem('inputBody')).OCCUPATION_ID : 0)
    fields['CAR_TYPE'] = (JSON.parse(localStorage.getItem('inputBody')).CAR_TYPE != null ? JSON.parse(localStorage.getItem('inputBody')).CAR_TYPE : 0)
    fields['CAR_BRAND'] = (JSON.parse(localStorage.getItem('inputBody')).CAR_BRAND != null ? JSON.parse(localStorage.getItem('inputBody')).CAR_BRAND : 0)
    fields['EDUCATION_ID'] = (JSON.parse(localStorage.getItem('inputBody')).EDUCATION_ID != null ? JSON.parse(localStorage.getItem('inputBody')).EDUCATION_ID : 0)
    fields['INCOME_ID'] = (JSON.parse(localStorage.getItem('inputBody')).INCOME_ID != null ? JSON.parse(localStorage.getItem('inputBody')).INCOME_ID : 0)

    this.setState({ fields, CAR_BRAND_REMARK, OCCAPATION_REMARK })
  }

  handleSubmit(event) {
    event.preventDefault();
    localStorage.setItem('inputBody', JSON.stringify(this.state.fields))
    if (this.validateForm()) {
      this.props.history.push({
        pathname: `${process.env.PUBLIC_URL}/formprofile_5/${this.state.language}`,
        state: {
          data: this.state.fields
        }
      });
    } else {
      console.log('formsubmit ' + false);
    }
  }

  validateForm() {

    let fields = this.state.fields;
    let errors = {};
    let errorsFocus = {};
    let formIsValid = true;

    // if (!fields["CAR_TYPE"]) {
    //   formIsValid = false;
    //   errorsFocus["CAR_TYPE"] = 'errorFocus'
    //   errors["CAR_TYPE"] = <FormattedMessage id="SELECT_TYPECAR" />
    // }
    // if (!fields["CAR_BRAND"]) {
    //   formIsValid = false;
    //   errorsFocus["CAR_BRAND"] = 'errorFocus'
    //   errors["CAR_BRAND"] = <FormattedMessage id="SELECT_TYPEBRAND" />
    // }
    if(fields["CAR_BRAND"] === 9) {
      if (!fields["CAR_BRAND_REMARK"]) {
        formIsValid = false;
        errorsFocus["CAR_BRAND_REMARK"] = 'errorFocus'
        errors["CAR_BRAND_REMARK"] = (
          <FormattedMessage id="TITLE_REMARK_CAR" />
        );
      }
    }
    if(fields["OCCUPATION_ID"] === 5) {
      if (!fields["OCCAPATION_REMARK"]) {
        formIsValid = false;
        errorsFocus["OCCAPATION_REMARK"] = 'errorFocus'
        errors["OCCAPATION_REMARK"] = (
          <FormattedMessage id="TITLE_REMARK_CAREER" />
        );
      }
    }
    // if (!fields["OCCUPATION_ID"]) {
    //   formIsValid = false;
    //   errorsFocus["OCCUPATION_ID"] = 'errorFocus'
    //   errors["OCCUPATION_ID"] = <FormattedMessage id="SELECT_TYPECAREER" />
    // }
    // if (!fields["EDUCATION_ID"]) {
    //   formIsValid = false;
    //   errorsFocus["EDUCATION_ID"] = 'errorFocus'
    //   errors["EDUCATION_ID"] = <FormattedMessage id="SELECT_TYPEEDUCATION" />
    // }
    // if (!fields["INCOME_ID"]) {
    //   formIsValid = false;
    //   errorsFocus["INCOME_ID"] = 'errorFocus'
    //   errors["INCOME_ID"] = <FormattedMessage id="SELECT_TYPEINCOME" />
    // }
   
     this.setState({
      errors: errors,
      errorsFocus: errorsFocus
    });
    return formIsValid;
  }

  handleGameClik(ev) {
    let { fields } = this.state;
    if(ev.target.name === "promotion") {
      if (ev.target.id === "PMT_OTHERS") {
        if(ev.target.checked === true) {
          disabled_block = 'form-control input_form'
          fields[ev.target.id] = ev.target.checked
        } else {
          disabled_block = 'form-control input_form disabled'
          fields['PMT_OTHERS_REMARK'] = ''
          fields[ev.target.id] = ev.target.checked
        }
      } else {
        if(fields['PMT_OTHERS'] === true) {
          disabled_block = 'form-control input_form'
        } else {
          disabled_block = 'form-control input_form disabled'
          fields['PMT_OTHERS_REMARK'] = ''
        }
        fields[ev.target.id] = ev.target.checked
      }
      this.setState( {disabled: (ev.target.checked === "" ? false : true)} )
    } else {
      fields[ev.target.id] = ev.target.value
    }
      this.setState({ fields })
  } 
  
  dropdownlistCheck(e) {
    let { errors, errorsFocus ,fields, CAR_BRAND_REMARK, OCCAPATION_REMARK } = this.state;
    if(e.target.name === "CAR_BRAND") {
      if(e.target.value === '9'){
        CAR_BRAND_REMARK = 'block'
      } else {
        CAR_BRAND_REMARK = 'none'
        fields["CAR_BRAND_REMARK"] = '';
        errors["CAR_BRAND_REMARK"]  = null;
        errorsFocus["CAR_BRAND_REMARK"]  = ''
      }
      errors[e.target.name] = null;
      fields[e.target.name] = parseInt(e.target.value)
      errorsFocus[e.target.name] = ''
    }else if(e.target.name === "OCCUPATION_ID") {
      if(e.target.value === '5'){
        OCCAPATION_REMARK = 'block'
      } else {
        OCCAPATION_REMARK = 'none'
        fields["OCCAPATION_REMARK"] = '';
        errors["OCCAPATION_REMARK"]  = null;
        errorsFocus["OCCAPATION_REMARK"]  = ''
      }
      errors[e.target.name] = null;
      fields[e.target.name] = parseInt(e.target.value)
      errorsFocus[e.target.name] = ''
    } else {
        fields[e.target.name] = e.target.value;
        errors[e.target.name] = null;
        errorsFocus[e.target.name] = ''
      }
      this.setState({ errors, errorsFocus, fields, CAR_BRAND_REMARK, OCCAPATION_REMARK})
  }

  checboxClick(ev) {
    let { fields } = this.state;
      if(ev.target.name === "news") {
        fields[ev.target.id] = ev.target.checked
        if (ev.target.id === "SUBSCRIBE_NONE") {
          if(ev.target.checked === true) {
            fields['SUBSCRIBE_EMAIL'] = false
            fields['SUBSCRIBE_LETTER'] = false
            fields['SUBSCRIBE_SMS'] = false
          }
        } else {
          fields['SUBSCRIBE_NONE'] = false
        }
      } else {
        fields[ev.target.id] = ev.target.value
      }

    this.setState({ fields })
}


  getDropdownlist_type(title) {
    return regisMaxCard.getDropdownlist(title)
  }

  renderContent(type) {
    const { occupation, education, income, cartype, car_brand} = this.state;
    var option = []
    var name_title;
    if (type === 'type_cartype') {
      name_title = cartype 
    } else if (type === 'type_brand') {
        name_title = car_brand 
    } else if (type === 'type_occupation') {
          name_title = occupation 
    } else if (type === 'type_education'){
      name_title = education 
    } else {
      name_title = income 
    }
    for(var i in name_title) {
      option.push(<option key={i} value={name_title[i].code}>{name_title[i].values}</option>)
    }
    return option
    }

  render() {
    // var link = `/formprofile_5/${this.state.language}`
    return (

      <IntlProvider
      locale={this.state.language}
      messages={messages[this.state.language].formprofile4}
      >

    <form className="form-horizontal formprofile_4" onSubmit={e => this.handleSubmit(e)}>
      <div className="bg_full">
        <div className="container-fluid">
        <div className="content_page">
            <div className="form-group">
              <label>
                <FormattedMessage id="title_typecars" />
              </label>
              <select className={`form-control ${this.state.errorsFocus['CAR_TYPE']}`}
                    name="CAR_TYPE" 
                    id='exampleFormControlSelect1'
                    value={this.state.fields['CAR_TYPE']}
                    onChange={(e) => this.dropdownlistCheck(e)}>
                    <option value="0">
                    {messages[this.state.language].formprofile4.SELECT_TYPECAR}
                    </option>
                        {this.renderContent('type_cartype')}
              </select>
              <div className="errorMsg CAR_TYPE">
                    {this.state.errors["CAR_TYPE"]}
              </div>
              {/* <FormattedHTMLMessage id="title_typecarslist" /> */}
            </div>
             <div className="form-group">
             <label>
               <FormattedMessage id="title_brand" />
            </label>
              <select className={`form-control ${this.state.errorsFocus['CAR_BRAND']}`}
                      name="CAR_BRAND" 
                      value={this.state.fields['CAR_BRAND']}
                      id='exampleFormControlSelect1' 
                      onChange={(e) => this.dropdownlistCheck(e)}>
                      <option value="0">
                      {messages[this.state.language].formprofile4.SELECT_TYPEBRAND}
                      </option>
                          {this.renderContent('type_brand')}
              </select>
                <div className="errorMsg CAR_BRAND">
                    {this.state.errors['CAR_BRAND']}
                </div>
              <div className="form-group" style={{ display: this.state.CAR_BRAND_REMARK}}>
              <input
                      type="text"
                      className={`form-control ${this.state.errorsFocus['CAR_BRAND_REMARK']} input_form`}
                      name="CAR_BRAND_REMARK"
                      value={this.state.fields['CAR_BRAND_REMARK']}
                      onChange={e => this.dropdownlistCheck(e)}
                      id="exampleFormControlInput1"
                      placeholder={ messages[this.state.language].formprofile4.TITLE_REMARK_CAR}
                    />
                    <div className="errorMsg CAR_BRAND_REMARK">
                    {this.state.errors['CAR_BRAND_REMARK']}
                    </div>
              </div>
              
              {/* <label><FormattedMessage id="title_brand" /></label>
              {this.renderContent('type_brand')} */}
              {/* <FormattedHTMLMessage id="title_brandlist" /> */}
            </div>
            <div className="form-group">
              <label><FormattedMessage id="title_career" /></label>
              <select className={`form-control ${this.state.errorsFocus['OCCUPATION_ID']}`}
                      name="OCCUPATION_ID" 
                      value={this.state.fields['OCCUPATION_ID']}
                      id='exampleFormControlSelect1' 
                      onChange={(e) => this.dropdownlistCheck(e)}>
                      <option value="0" >
                      {messages[this.state.language].formprofile4.SELECT_TYPECAREER}
                      </option>
                          {this.renderContent('type_occupation')}
              </select>
              <div className="errorMsg OCCUPATION_ID">
                    {this.state.errors['OCCUPATION_ID']}
                </div>
              <div className="form-group" style={{ display: this.state.OCCAPATION_REMARK}}>
              <input
                      type="text"
                      className={`form-control ${this.state.errorsFocus['OCCAPATION_REMARK']} input_form`}
                      name="OCCAPATION_REMARK"
                      value={this.state.fields['OCCAPATION_REMARK']}
                      onChange={e => this.dropdownlistCheck(e)}
                      id="exampleFormControlInput1"
                      placeholder={ messages[this.state.language].formprofile4 .TITLE_REMARK_CAREER}
                    />
                    <div className="errorMsg OCCAPATION_REMARK">
                    {this.state.errors['OCCAPATION_REMARK']}
                    </div> 
              </div>
              {/* <FormattedHTMLMessage id="title_careerlist" /> */}
            </div>
            <div className="form-group">
              <label><FormattedMessage id="title_education" /></label>
              <select className={`form-control ${this.state.errorsFocus['EDUCATION_ID']}`}
                        name="EDUCATION_ID" 
                        value={this.state.fields['EDUCATION_ID']}
                        id='exampleFormControlSelect1' 
                        onChange={(e) => this.dropdownlistCheck(e)}>
              <option value="0">{messages[this.state.language].formprofile4.SELECT_TYPEEDUCATION}</option>
              {this.renderContent('type_education')}
              </select>
              <div className="errorMsg EDUCATION_ID">
                    {this.state.errors["EDUCATION_ID"]}
              </div>
              {/* <FormattedHTMLMessage id="title_educationlist" /> */}
            </div>
            <div className="form-group">
              <label><FormattedMessage id="title_income" /></label>
              <select  className={`form-control ${this.state.errorsFocus['INCOME_ID']}`}
                        name="INCOME_ID" 
                        value={this.state.fields['INCOME_ID']}
                        id='exampleFormControlSelect1' 
                        onChange={(e) => this.dropdownlistCheck(e)}>
              <option value="0">{messages[this.state.language].formprofile4.SELECT_TYPEINCOME}</option>
              {this.renderContent('type_income')}
              </select>
              <div className="errorMsg INCOME_ID">
                    {this.state.errors["INCOME_ID"]}
              </div>
              {/* <FormattedHTMLMessage id="title_incomelist" /> */}
            </div>
            {/* <label><FormattedMessage id="title_remindnews" /></label>
            <div className="row">
                <div className="col">
                <label className="check_conditions_select"><FormattedMessage id="title_listnews" />
                  <input type="checkbox" id="SUBSCRIBE_LETTER" name="news"  onChange = {(e) => this.checboxClick(e)} checked={(this.state.fields['SUBSCRIBE_LETTER']) ? (this.state.fields['SUBSCRIBE_LETTER'] === true || this.state.fields['SUBSCRIBE_NONE'] != true ? true : false) : false} />
                  <span className="checkmark_condition_select"></span>
                </label>
                </div>
                <div className="col">
                <label className="check_conditions_select"><FormattedMessage id="title_listemail" />
                  <input type="checkbox" id="SUBSCRIBE_EMAIL" name="news" onChange = {(e) => this.checboxClick(e)} checked={(this.state.fields['SUBSCRIBE_EMAIL']) ? (this.state.fields['SUBSCRIBE_EMAIL'] === true || this.state.fields['SUBSCRIBE_NONE'] != true ? true : false) : false} />
                  <span className="checkmark_condition_select"></span>
                </label>
                </div>
                <div className="w-100"></div>
                <div className="col">
                <label className="check_conditions_select"><FormattedMessage id="title_listsms" />
                  <input type="checkbox" id="SUBSCRIBE_SMS" name="news" onChange = {(e) => this.checboxClick(e)} checked={(this.state.fields['SUBSCRIBE_SMS'] === true ? true : false )} />
                  <span className="checkmark_condition_select"></span>
                </label>
                </div>
                <div className="col">
                <label className="check_conditions_select"><FormattedMessage id="title_listnonesms" />
                  <input type="checkbox" id="SUBSCRIBE_NONE" name="news" onChange = {(e) => this.checboxClick(e)} checked={(this.state.fields['SUBSCRIBE_NONE']) ? (this.state.fields['SUBSCRIBE_NONE'] === true || this.state.fields['SUBSCRIBE_NONE'] != true ? true : false) : false } />
                  <span className="checkmark_condition_select"></span>
                </label>
                </div>
            </div> */}
            <label><FormattedMessage id="title_remindpromotion" /></label>
            <div className="row">
                <div className="col">
                <label className="selectoneonly"><FormattedMessage id="title_listfood" />
                  <input type="checkbox" id="PMT_FOOD" checked={this.state.fields['PMT_FOOD']} name="promotion" onClick = {(e) => this.handleGameClik(e)} />
                  <span className="checkmark_selectoneonly"></span>
                </label>
                </div>
                <div className="col">
                <label className="selectoneonly"><FormattedMessage id="title_listravel" />
                  <input type="checkbox" id="PMT_TRAVEL" checked={this.state.fields['PMT_TRAVEL']} name="promotion" onClick = {(e) => this.handleGameClik(e)} />
                  <span className="checkmark_selectoneonly"></span>
                </label>
                </div>
                <div className="w-100"></div>
                <div className="col">
                <label className="selectoneonly"><FormattedMessage id="title_listshopping" />
                  <input type="checkbox" id="PMT_SHOPPING" checked={this.state.fields['PMT_SHOPPING']} name="promotion" onClick = {(e) => this.handleGameClik(e)} />
                  <span className="checkmark_selectoneonly"></span>
                </label>
                </div>
                <div className="col">
                <label className="selectoneonly"><FormattedMessage id="title_listbeauty" />
                  <input type="checkbox" id="PMT_BEAUTY" checked={this.state.fields['PMT_BEAUTY']} name="promotion" onClick = {(e) => this.handleGameClik(e)} />
                  <span className="checkmark_selectoneonly"></span>
                </label>
                </div>
                <div className="w-100"></div>
                <div className="col">
                <label className="selectoneonly"><FormattedMessage id="title_listcars" />
                  <input type="checkbox" id="PMT_SPORT_AND_CAR" checked={this.state.fields['PMT_SPORT_AND_CAR']} name="promotion" onClick = {(e) => this.handleGameClik(e)} />
                  <span className="checkmark_selectoneonly"></span>
                </label>
                </div>
                <div className="w-100"></div>
                <div className="col">
                <label className="selectoneonly"><FormattedMessage id="title_listother" />
                  <input type="checkbox" id="PMT_OTHERS" checked={this.state.fields['PMT_OTHERS']} name="promotion" onClick = {(e) => this.handleGameClik(e)} />
                  <span className="checkmark_selectoneonly"></span>
                </label>
                <div className="form-group">
                  <input type="text" id="PMT_OTHERS_REMARK" value={this.state.fields['PMT_OTHERS_REMARK']} className={disabled_block} 
                  placeholder={messages[this.state.language].formprofile4.etc_input} value={this.state.fields['PMT_OTHERS_REMARK']}  onChange={(e) => this.handleGameClik(e)} />
                </div>
                </div>
            </div>
              <div className="">
                  <button type="submit" className="btn btn-secondary btn-block btn_agreement"><FormattedMessage id="button_nextstep" /></button>
              </div>
          </div>
        </div>
      </div>
      </form>
      </IntlProvider>
    )
  }
}

export default Formprofile_4;
