import React from "react";
import "./history_totalpoint.css";
import 'moment/locale/th';
import { getTransection } from "../../_actions/getTransection";
import {
  IntlProvider,
  FormattedMessage,
} from "react-intl";
import { addLocaleData } from "react-intl";
import locale_en from "react-intl/locale-data/en";
import locale_th from "react-intl/locale-data/th";
import intlMessageEN from "./../../_translations/en.json";
import intlMessageTH from "./../../_translations/th.json";
import moment from 'moment-timezone';

addLocaleData([...locale_en, ...locale_th]);

var m = moment().startOf('month').format('MM');
var y = moment().startOf('year').format('YYYY');

  var messages = {
    th: intlMessageTH,
    en: intlMessageEN
  };


class History_totalpoint extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      startDate: new Date(),
      language: "th"
    };
  }

  componentDidMount() {
    const params = this.props.match.params;
    var tokenData = this.props.location;
    var sstr = tokenData.search.search("&");
    var sub_memberid = tokenData.search.substr(sstr);
    var memberid = sub_memberid.substr(5);
    this.setState({ language: params.lang });
    sessionStorage.setItem('userId', memberid)
    sessionStorage.setItem('TOKEN_ID', params.tokenid)
    sessionStorage.setItem('CUSTOMER_ID', params.customerid)
    sessionStorage.setItem('CARD_MEMBER_ID', params.cardmemberid)
    sessionStorage.setItem('_udid' , params.udid)
    sessionStorage.setItem('_deviceos' , params.deviceos)
    this.getHistoryTotalpoint(m,y);
  }

  getHistoryTotalpoint(m,y){
    var startDate = moment([y, m - 1]).format('YYYY-MM-DD');
    var endDate = moment().format('YYYY-MM-DD')
    var user = sessionStorage.getItem('userId')
    var token = sessionStorage.getItem('TOKEN_ID')
    var customer = sessionStorage.getItem('CUSTOMER_ID')
    var cardmember = sessionStorage.getItem('CARD_MEMBER_ID')
    var type = "E"
    getTransection.getBurnHistoryList(startDate,endDate,user,token,customer,cardmember,type).then(e => {
      if(e.data.isSuccess === false){
        this.setState({ notfound : messages[this.state.language].history.datanull})
      }else{
        if(e.data.data !== "" ){
          var earn_info = JSON.parse(e.data.data)
          this.setState({ data : earn_info.TRANS_EXPIRE})
          var resp = e.data;
          if (resp.errCode === 111 ) {
            this.setState({ notfound : messages[this.state.language].history.datanull})
          } else {
            if (earn_info.TRANS_EXPIRE === null ) {
            this.setState({ notfound : messages[this.state.language].history.datanull})
            } else {
              this.setState({ notfound : ""})
            }
          }
        }else{
          this.setState({ notfound: messages[this.state.language].history.datanull })
        }
      }
    })
  }
 
  render() {
    var totalpoint = this.state.data
    var totalPointList = [];
    var transDate = ''
    for ( var i in totalpoint){
      if(this.state.language === 'th'){
        transDate = moment(totalpoint[i].TRANS_DATE).add(543,'y').locale('th').format('LLL')
      }else{
        transDate = moment(totalpoint[i].TRANS_DATE).locale('en').format('LLL')
      }
      totalPointList.push(
        <div key={i} className="row list_date">
          <div className="col col-12 sub_date_history_point">{transDate}</div>
          <div className="col col-8 select_date text-left" >
            <div className="title_list_history"><FormattedMessage id="pointexpried" /></div>
            <div className="sub_title_list_history_point"><FormattedMessage id="estamp" /></div>
          </div>
        <div className="col col-4 select_date text-right">
          <div className="title_list_history">{totalpoint[i].TOTAL_POINT}</div>
          <div className="title_list_history">{totalpoint[i].ESTAMP_POINT}</div>
        </div>
      </div>
      )
    }
    return (

      <IntlProvider
        locale={this.state.language}
        messages={messages[this.state.language].history}
      >
      <div className="bg_full">
        <div className="container-fluid">
        <div className="content_page">
            {totalPointList}
            <div className="data_null">{this.state.notfound}</div> 
          </div >
        </div>
      </div>
    </IntlProvider>
    )
  }
}

export default History_totalpoint;
