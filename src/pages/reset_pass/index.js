import React from "react";
// import { NavLink } from "react-router-dom";

// import DatePicker from "react-datepicker";
import "./reset_pass.css";
import {
  IntlProvider,
  FormattedMessage,
} from "react-intl";
import { addLocaleData } from "react-intl";
import locale_en from "react-intl/locale-data/en";
import locale_th from "react-intl/locale-data/th";
import intlMessageEN from "./../../_translations/en.json";
import intlMessageTH from "./../../_translations/th.json";

addLocaleData([...locale_en, ...locale_th]);

  var messages = {
    th: intlMessageTH,
    en: intlMessageEN
  };


class Reset_pass extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      startDate: new Date(),
      language: "th"
      
    };
    this.handleChange = this.handleChange.bind(this);
  }
 
  handleChange(date) {
    this.setState({
      startDate: date
    });
  }

  componentDidMount() {
    const params = this.props.match.params;
    this.setState({ language: params.lang });
  }
 

  render() {

    var link = `/reset_pass/${this.state.language}`


    return (

      <IntlProvider
        locale={this.state.language}
        messages={messages[this.state.language].reset_pass}
      >
      <div className="bg_full">
        <div className="container-fluid">
        <div className="content_page">
             <div className="form-group">
              <input type="text" className="form-control input_form" id="exampleFormControlInput1" placeholder={messages[this.state.language].reset_pass.resetold_input} />
            </div>
            <div className="form-group">
              <input type="text" className="form-control input_form" id="exampleFormControlInput1" placeholder={messages[this.state.language].reset_pass.resetnew_input}/>
            </div>
            <div className="form-group">
              <input type="text" className="form-control input_form" id="exampleFormControlInput1" placeholder={messages[this.state.language].reset_pass.resetfirm_input} />
            </div>
            <a href={link}>
              <div className="btn_nextstep">
                  <button type="button" className="btn btn-secondary btn-block btn_agreement"><FormattedMessage id="button_change" /></button>
              </div>
             </a>
          </div>
        </div>
      </div>
      </IntlProvider>
    )
  }
}

export default Reset_pass;
