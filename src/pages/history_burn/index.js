import React from "react";
import moment from 'moment-timezone';
import 'moment/locale/th';
import { getTransection } from "../../_actions/getTransection";
import "./history_burn.css";
import {
  IntlProvider,
  FormattedMessage,
  FormattedHTMLMessage,
} from "react-intl";
import { addLocaleData } from "react-intl";
import locale_en from "react-intl/locale-data/en";
import locale_th from "react-intl/locale-data/th";
import intlMessageEN from "./../../_translations/en.json";
import intlMessageTH from "./../../_translations/th.json";
import SweetAlert from "react-bootstrap-sweetalert";

let alert;
var burnData = []

addLocaleData([...locale_en, ...locale_th]);

  var messages = {
    th: intlMessageTH,
    en: intlMessageEN
  };

  var m = moment().startOf('month').format('MM');
  var y = moment().startOf('year').format('YYYY');

  var checkLength = 0

class History_burn extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      startDate: new Date(),
      language: "th",
      fields: {
        SELECT_YEAR: "",
        SELECT_MONTH: ""
      },
      MONTH : "",
      YEAR : "",
      show: false,
      modal: null,
      textalert : '',
      setMonth: '',
      loading: false,
      data: ""
    };
    this.handleChange = this.handleChange.bind(this);
  }

  fetchMoreData = () => {
    // a fake async api call like which sends
    // 20 more records in 1.5 secs
    setTimeout(() => {
      this.setState({
        items: this.state.items.concat(Array.from({ length: 20 }))
      });
    }, 1500);
  };

  componentDidMount() {
    const params = this.props.match.params;
    var tokenData = this.props.location;
    var sstr = tokenData.search.search("&");
    var sub_memberid = tokenData.search.substr(sstr);
    var memberid = sub_memberid.substr(5);
    this.setState({ language: params.lang });
    sessionStorage.setItem('userId', memberid)
    sessionStorage.setItem('TOKEN_ID', params.tokenid)
    sessionStorage.setItem('CUSTOMER_ID', params.customerid)
    sessionStorage.setItem('CARD_MEMBER_ID', params.cardmemberid)
    sessionStorage.setItem('_udid' , params.udid)
    sessionStorage.setItem('_deviceos' , params.deviceos)

    this.checkMonth()
    this.getBurnHistory(m,y);
  }

  handleChange(e){
    this.setState({ MONTH : e.target.value }) 
  }

  changeHandle(event){
    this.setState({ YEAR : event.target.value })
  }

  modalCheckSubmit(res,img){
    alert = (
      <SweetAlert
        custom
        confirmBtnBsStyle="success"
        closeOnClickOutside={true}
        cancelBtnBsStyle="default"
        focusConfirmBtn={false}
        title=""
        customIcon={img}
        showConfirm={false}
        showCancelButton
        onCancel={() => this.handleChoice(false)}
        onConfirm={() => this.handleChoice(true)}
        onOutsideClick={() => {
          this.wasOutsideClick = true;
          this.setState({ showConfirm: false })
        }}
      >
        {res}
      </SweetAlert>
    );
    this.setState({ show: true , modal: alert })
  }

  handleChoice(choice) {
    if (choice === false) {
      this.setState({ show: false , modal: null })
    }
  }

  checkMonth(){
    if(m == "01"){
      this.setState ({ setMonth : messages[this.state.language].history.jan })
    }else if (m == "02"){
      this.setState ({ setMonth : messages[this.state.language].history.feb })
    }else if(m == "03"){
      this.setState ({ setMonth : messages[this.state.language].history.march })
    }else if(m == "04"){
      this.setState ({ setMonth : messages[this.state.language].history.april })
    }else if(m == "05"){
      this.setState ({ setMonth : messages[this.state.language].history.may })
    }else if(m == "06"){
      this.setState ({ setMonth : messages[this.state.language].history.june })
    }else if(m == "07"){
      this.setState ({ setMonth : messages[this.state.language].history.july })
    }else if(m == "08"){
      this.setState ({ setMonth : messages[this.state.language].history.august })
    }else if(m == "09"){
      this.setState ({ setMonth : messages[this.state.language].history.sep })
    }else if(m == "10"){
      this.setState ({ setMonth : messages[this.state.language].history.oct })
    }else if(m == "11"){
      this.setState ({ setMonth : messages[this.state.language].history.november })
    }else{
      this.setState ({ setMonth : messages[this.state.language].history.december })
    }
  }
  

  getBurnHistory(m,y){
    var startDate = moment([y, m - 1]).format('YYYY-MM-DD');
    var endDate = moment(startDate).endOf('month').format('YYYY-MM-DD');
    var user = sessionStorage.getItem('userId')
    var token = sessionStorage.getItem('TOKEN_ID')
    var customer = sessionStorage.getItem('CUSTOMER_ID')
    var cardmember = sessionStorage.getItem('CARD_MEMBER_ID')
    var type = "R"
    getTransection.getBurnHistoryList(startDate,endDate,user,token,customer,cardmember,type).then(e => {
      console.log("getBurnHistoryList", e)
      if(e.data.iSSuccess === false){
        this.setState({ notfound : messages[this.state.language].history.datanull})     
      }else{
        if(e.data.data !== ""){
          var earn_info = JSON.parse(e.data.data)
          console.log("earn_info", earn_info)
          this.setState({ data : earn_info.TRANS_REDEEM })
          var resp = e.data;
          if (resp.errCode == 111) {
            this.setState({ notfound : messages[this.state.language].history.datanull})
          } else {
            this.setState({ notfound : ""})
          }
        }else{
          this.setState ({ data : '' })
          this.setState({ notfound : messages[this.state.language].history.datanull})
        }
      }
    })
}

groupNormalPoint(data){
  let point = 0
  if(data.length > 0){
    for(let i in data){
      point += data[i].REWARD_POINT
    }
  }
  return point
}

groupOtherPoint(data){
  var result = [];

  data.reduce(function(res, value) {
    if (!res[value.POINT_TYPE_NAME]) {
      res[value.POINT_TYPE_NAME] = { POINT_TYPE_NAME: value.POINT_TYPE_NAME, REWARD_POINT: 0 };
      result.push(res[value.POINT_TYPE_NAME])
    }
    res[value.POINT_TYPE_NAME].REWARD_POINT += value.REWARD_POINT;
    return res
    
  }, {});
  return result
}

  render() {
    if(this.state.MONTH == '' && this.state.YEAR == ''){
      this.state.MONTH = m
      this.state.YEAR = y
    }
    var yearnow = new Date().getFullYear()
    var start = yearnow - parseInt(100);
    var getYear = [];
    for(start ; yearnow >= start ; yearnow--){
      getYear.push(<option key={yearnow} name="SELECT_YEAR" selected={(y == yearnow ? true : false )}>{yearnow}</option>)
    }

    burnData = this.state.data
    var allProducts = ""
    if(burnData){
      allProducts = burnData.map((item) => (
        <div className="row list_date">
        <div className="col col-8 select_date text-left" >
          <div className="title_list_history">{item.TRANS_PRODUCT} ({item.SHOP_NAME})</div>
          <div className="sub_date_history">{(this.state.language === 'th' ? moment(item.TRANS_DATE).add(543,'y').locale('th').format('LLL') : moment(item.TRANS_DATE).locale('en').format('LLL'))}</div>
        </div>
        <div className="col col-4 select_date text-right">
          <div className="point_history_deduct">- {this.groupNormalPoint((item.REDEEM_POINT_DETAIL).filter(e => e.POINT_TYPE_NAME == "Normal Point" || e.POINT_TYPE_NAME == "Special Point"))}</div>
            
          {
            (this.groupOtherPoint((item.REDEEM_POINT_DETAIL).filter(e => e.POINT_TYPE_NAME != "Normal Point" && e.POINT_TYPE_NAME != "Special Point")).map((point) => {
              return (
                <div className="sub_date_history text-red"> {point.POINT_TYPE_NAME + " : "} - {point.REWARD_POINT} </div>
              )
            }))
          }
          {/* {item.REDEEM_POINT_DETAIL.map((point) => {
              return (
                ( 
                  point.POINT_TYPE_NAME != "Normal Point" && point.POINT_TYPE_NAME != "Special Point"
                  ? <div className="sub_date_history text-red">{point.POINT_TYPE_NAME + " : "} - {point.REWARD_POINT} </div>
                  : "")
              );
            })} */}
        </div> 
        </div>
      ))
    }
    //   for ( var i in burnData){
    //     checkLength = burnData.length
    //     if(this.state.language === 'th'){
    //       var transDate = moment(burnData[i].TRANS_DATE).add(543,'y').locale('th').format('LLL')
    //     }else{
    //       var transDate = moment(burnData[i].TRANS_DATE).locale('en').format('LLL')
    //     }

    //   for ( var j in burnData[i].REDEEM_POINT_DETAIL) {
    //     var REDEEM_POINT_DETAIL = burnData[i].REDEEM_POINT_DETAIL
    //     burnDataList.push
    //     (<div key={j} className="row list_date">
    //       <div className="col col-8 select_date text-left" >
    //         <div className="title_list_history">{burnData[i].TRANS_PRODUCT} ({burnData[i].SHOP_NAME})</div>
    //         <div className="sub_date_history">{transDate}</div>
    //       </div>
    //       <div className="col col-4 select_date text-right">
    //         <div className="point_history_deduct">- {REDEEM_POINT_DETAIL[j].REWARD_POINT}</div>
    //       </div> 
    //     </div>)
    //   }

    // }
    return (
      <IntlProvider
        locale={this.state.language}
        messages={messages[this.state.language].history}
      >
      <div className="bg_full">
        <div className="container-fluid">
        <div className="content_page">
            <div className="title_pagemanagepoint"><FormattedMessage id="date_title" /></div>
            <div className="row select_date_row">
                <div className="col select_date">
                  <div className="form-group">
                    <select name="SELECT_MONTH" className="form-control" id="exampleFormControlSelect1" onChange={e => this.handleChange(e)}>
                      <option value="01" selected={(m == '01' ? true : false)}>{messages[this.state.language].history.jan}</option>
                      <option value="02" selected={(m == '02' ? true : false)}>{messages[this.state.language].history.feb}</option>
                      <option value="03" selected={(m == '03' ? true : false)}>{messages[this.state.language].history.march}</option>
                      <option value="04" selected={(m == '04' ? true : false)}>{messages[this.state.language].history.april}</option>
                      <option value="05" selected={(m == '05' ? true : false)}>{messages[this.state.language].history.may}</option>
                      <option value="06" selected={(m == '06' ? true : false)}>{messages[this.state.language].history.june}</option>
                      <option value="07" selected={(m == '07' ? true : false)}>{messages[this.state.language].history.july}</option>
                      <option value="08" selected={(m == '08' ? true : false)}>{messages[this.state.language].history.august}</option>
                      <option value="09" selected={(m == '09' ? true : false)}>{messages[this.state.language].history.sep}</option>
                      <option value="10" selected={(m == '10' ? true : false)}>{messages[this.state.language].history.oct}</option>
                      <option value="11" selected={(m == '11' ? true : false)}>{messages[this.state.language].history.november}</option>
                      <option value="12" selected={(m == '12' ? true : false)}>{messages[this.state.language].history.december}</option>
                    </select>
                  </div>
                </div>
                <div className="col select_date">
                  <div className="form-group">
                    <select name="SELECT_YEAR" className="form-control" id="exampleFormControlSelect1" onChange={event => this.changeHandle(event)}>
                      {getYear}
                    </select>
                  </div>
                </div>
            </div>
            <div className="row">
              <div className="col select_date "></div>
              <div className="col select_date">
                  <div className="">
                      <button 
                        type="button" 
                        className="btn btn-secondary btn-block btn_search"
                        onClick={() => this.getBurnHistory(this.state.MONTH,this.state.YEAR)}
                      >
                        <FormattedMessage id="button_search" />
                      </button>
                  </div>
              </div>
              <div className="col select_date"></div>
            </div>
                {allProducts}
              <div className="data_null">{this.state.notfound}</div> 
              {this.state.modal}
          </div >
        </div>
      </div>
      </IntlProvider>

    )
  }
}

export default History_burn;
