import React, {Component} from "react";
import Routing from "./routes";

class App extends Component {

  render() {
    return (
      <div className="my-app">
        <Routing />
      </div>
    );
  }
}

export default App;